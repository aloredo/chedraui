package netasystems.net.pantallaschedraui.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import netasystems.net.pantallaschedraui.R;

public class MenuConfirmacion extends AppCompatActivity {

    Button cancelar, jerarquíaArticulo, confirmacion, articulo, nivel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_confirmacion);

        cancelar = (Button)findViewById(R.id.btnCancelar);
        jerarquíaArticulo = (Button)findViewById(R.id.btnJerarquiaArticulo);
        confirmacion = (Button)findViewById(R.id.btnConfirmacion);
        articulo = (Button)findViewById(R.id.btnArticulo);
        nivel = (Button)findViewById(R.id.btnNivel);

        nivel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuConfirmacion.this,NivelMueble.class);
                startActivity(intent);
            }
        });
        articulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuConfirmacion.this,NivelMueble.class);
                startActivity(intent);
            }
        });

        confirmacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuConfirmacion.this, ClaseCenso.class);
                startActivity(intent);
            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuConfirmacion.this, MenúPrincipalActivity.class);
                startActivity(intent);
            }
        });
    }
}
