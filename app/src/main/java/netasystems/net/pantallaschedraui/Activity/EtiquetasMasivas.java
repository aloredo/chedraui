package netasystems.net.pantallaschedraui.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import netasystems.net.pantallaschedraui.R;

public class EtiquetasMasivas extends AppCompatActivity {

    Button aceptar, cancelar, etiquetas;
    EditText numEtiquetas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etiquetas_masivas);

        aceptar = (Button)findViewById(R.id.btnSiguiente);
        cancelar = (Button)findViewById(R.id.btnCancelar);
        etiquetas = (Button)findViewById(R.id.btnEtiqueta);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EtiquetasMasivas.this, MenúPrincipalActivity.class);
                startActivity(intent);
            }
        });

        etiquetas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                class BackgroundTask extends AsyncTask<String, Void, Void> {

                    Socket s;
                    PrintWriter writer;

                    @Override
                    protected Void doInBackground(String... voids) {
                        try {
                            String message = voids[0];
                            s = new Socket("192.168.100.109", 6000);
                            writer = new PrintWriter(s.getOutputStream());
                            writer.write(message);
                            writer.flush();
                            writer.close();

                        } catch (IOException e){
                            e.printStackTrace();
                        }
                        return null;
                    }
                    public void send_data(View v){

                        numEtiquetas = (EditText)findViewById(R.id.edEtiqueta);
                        String message = numEtiquetas.getText().toString();
                        BackgroundTask b1 = new BackgroundTask();
                        b1.execute(message);

                    }
                }
            }
        });
    }
}
