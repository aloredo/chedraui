package netasystems.net.pantallaschedraui.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import netasystems.net.pantallaschedraui.R;

/*
    @Author  Neta Systems José Alfredo Santiago
    Activity Escaneos
 */
public class EspecificacionActivity extends AppCompatActivity {

    /*
   Declaración de cajas de texto, botones y listview
    */
    EditText tienda;
    Button aceptar, salir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_especificacion);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Asignación de las variables a su id correspondiente
        aceptar = (Button) findViewById(R.id.btnLimpiar);
        salir = (Button) findViewById(R.id.btnSalir);
        tienda = (EditText) findViewById(R.id.txtTienda);

        //Aplicación del método onClickListener al botón aceptar
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String num = tienda.getText().toString();
                //variable que evalua si la variable num no esté vacía
                if (!num.isEmpty()){

                    //Intent que lleva al Activity MenúPrincipal
                    Intent intent = new Intent(EspecificacionActivity.this, MenúPrincipalActivity.class);
                    startActivity(intent);
                }else {
                    //Mensaje que notifica si el cuadro de texto está vacío
                    Toast.makeText(getApplicationContext(), "Debes ingresar un número de tienda", Toast.LENGTH_SHORT).show();
                }


            }
        });
        //Aplicación del método onClickListener al botón salir
        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EspecificacionActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
