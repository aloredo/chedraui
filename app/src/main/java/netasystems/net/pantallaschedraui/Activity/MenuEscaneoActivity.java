package netasystems.net.pantallaschedraui.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import netasystems.net.pantallaschedraui.R;

/*
    @Author  Neta Systems José Alfredo Santiago
    Activity Menu Escaneos
 */
public class MenuEscaneoActivity extends AppCompatActivity {

    /*
  Declaración de botones y String de mensaje
   */
    Button anaquel, bodega, vitrina, merma, volver;
    String mensaje;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_escaneo);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Asignación de las variables a su id correspondiente
        anaquel = (Button)findViewById(R.id.btnAnaquel);
        bodega = (Button)findViewById(R.id.btnBodega);
        vitrina = (Button)findViewById(R.id.btnVitrina);
        merma = (Button)findViewById(R.id.btnMerma);
        volver = (Button)findViewById(R.id.btnVolver);

        //Asignación del método onClickListener al botón Anaquel
        anaquel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuEscaneoActivity.this, MuebleActivity.class);
                startActivity(intent);
            }
        });

        //Asignación del método onClickListener al botón bodega
        bodega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuEscaneoActivity.this, MenuUsuarioActivity.class);
                startActivity(intent);
            }
        });

        //Asignación del método onClickListener al botón vitrina
        vitrina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje = "vitrina";
                Intent intent = new Intent(MenuEscaneoActivity.this, EscaneoCharolaActivity.class);
                intent.putExtra("MENSAJE", mensaje);
                startActivity(intent);
            }
        });

        //Asignación del método onClickListener al botón merma
        merma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje = "merma";
                Intent intent = new Intent(MenuEscaneoActivity.this, EscaneoCharolaActivity.class);
                intent.putExtra("MENSAJE", mensaje);
                startActivity(intent);
            }
        });

        //Asignación del método onClickListener al botón volver
        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuEscaneoActivity.this, MenúPrincipalActivity.class);
                startActivity(intent);
            }
        });

    }
}
