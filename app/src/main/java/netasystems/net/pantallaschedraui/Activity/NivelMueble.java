package netasystems.net.pantallaschedraui.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import netasystems.net.pantallaschedraui.R;

public class NivelMueble extends AppCompatActivity {

    Button salir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nivel_mueble);

        salir = (Button)findViewById(R.id.btnSalir);

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NivelMueble.this, MenuConfirmacion.class);
                startActivity(intent);
            }
        });
    }
}
