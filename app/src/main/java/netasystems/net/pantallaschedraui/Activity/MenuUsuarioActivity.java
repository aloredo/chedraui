package netasystems.net.pantallaschedraui.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import netasystems.net.pantallaschedraui.R;

public class MenuUsuarioActivity extends AppCompatActivity {
    EditText mueble, empleado, proveedor;
    CheckBox soyEmpleado;
    TextView tMueble, tEmpleado, tProveedor;
    Button aceptar, cancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_usuario);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mueble = (EditText)findViewById(R.id.edMueble);
        empleado = (EditText) findViewById(R.id.edEmpleado);
        proveedor = (EditText)findViewById(R.id.edProveedor);
        soyEmpleado = (CheckBox)findViewById(R.id.checkProveedor);
        tMueble = (TextView)findViewById(R.id.txtMueble);
        tEmpleado = (TextView)findViewById(R.id.txtEmpleado);
        tProveedor = (TextView)findViewById(R.id.txtProveedor);
        aceptar = (Button)findViewById(R.id.btnLimpiar);
        cancelar = (Button)findViewById(R.id.btnSalir);






    }

    public void soyEmpleado (View view)
    {

        if (soyEmpleado.isChecked() == false) {
            empleado.setVisibility(View.VISIBLE);
            tEmpleado.setVisibility(View.VISIBLE);
            proveedor.setVisibility(View.GONE);
            tProveedor.setVisibility(View.GONE);

        }
        else {
            proveedor.setVisibility(View.VISIBLE);
            tProveedor.setVisibility(View.VISIBLE);
            empleado.setVisibility(View.GONE);
            tEmpleado.setVisibility(View.GONE);
        }


    }

    public void aceptar (View view) {
        String a = mueble.getText().toString();
        String b = empleado.getText().toString();
        String c = proveedor.getText().toString();
        if (soyEmpleado.isChecked()== false) {
            if (!a.isEmpty() && !b.isEmpty()){
                Intent intent = new Intent(MenuUsuarioActivity.this, EscaneoActivity.class);
                startActivity(intent);
            } else
                Toast.makeText(getApplicationContext(), "Número de mueble y/o de empleado están vacíos",Toast.LENGTH_SHORT).show();
        } else {
            if (!a.isEmpty() && !c.isEmpty()){
                Intent intent = new Intent(MenuUsuarioActivity.this, EscaneoActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(),"El número de mueble y/o proveedor están vacíos", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public void cancelar (View view) {
        Intent intent = new Intent(MenuUsuarioActivity.this, MenuEscaneoActivity.class);
    }
}
