package netasystems.net.pantallaschedraui.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import netasystems.net.pantallaschedraui.R;

/*
    @Author  Neta Systems José Alfredo Santiago
    Activity Escaneos
 */

public class EscaneoActivity extends AppCompatActivity {

    /*
    Declaración de cajas de texto, botones y listview
     */
    Button aceptar, finalizar, cancelar;
    EditText codigo;
    String recibido;
    ListView lista;
    ArrayList<String> productos;
    ArrayAdapter<String> adaptador1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escaneo);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Variable que recibe el mensaje enviado del MuebleActivity
        recibido = getIntent().getStringExtra("Mueble");
        //Asignación de las variables a su id correspondiente
        cancelar = (Button) findViewById(R.id.btnCancelar);
        aceptar = (Button) findViewById(R.id.btnLimpiar);
        codigo = (EditText) findViewById(R.id.eUpc);
        lista = (ListView) findViewById(R.id.lvLista);
        finalizar = (Button)findViewById(R.id.btnFinalizar);
        //Creación de la lista productos
        productos = new ArrayList<String>();
        //llenado de lista con datos aleatorios
        productos.add("Cereal Nesquik 500 gr $65");
        productos.add("Cereal Nesquik 250 gr $35");
        productos.add("Cereal Kornflakes 400 gr $45");

        //Creación del adaptaor que se encarga de llenar la listview
        adaptador1 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, productos);
        lista.setAdapter(adaptador1);

        //Asignación del método onItemLongListener a la lista
        lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final int posicion=i;

                //Creación de un AlertDialog que toma la posición en la listview que selecciona el usuario y le muestra 4 opciones
                AlertDialog.Builder dialogo1 = new AlertDialog.Builder(EscaneoActivity.this);
                dialogo1.setTitle("Importante");
                dialogo1.setMessage("¿Elimina este producto?");
                dialogo1.setCancelable(false);
                dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    //Metodo que elimina el elemento seleccionado
                    public void onClick(DialogInterface dialogo1, int id) {
                        productos.set(posicion, codigo.getText().toString());
                        adaptador1.notifyDataSetChanged();
                        codigo.setText("");
                    }
                });
                dialogo1.setNeutralButton("Modificar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //Método que permite actualizar el elemento seleccionado
                       codigo.setText(productos.set(posicion, codigo.getText().toString()));
                       adaptador1.notifyDataSetChanged();
                       productos.set(posicion, codigo.getText().toString());
                       productos.remove(posicion);
                       adaptador1.notifyDataSetChanged();

                    }
                });
                dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    //Método que cancela la acción y cierra el AlertDialog
                    public void onClick(DialogInterface dialogo1, int id) {
                    }
                });
                dialogo1.show();

                return false;
            }
        });


        //Asignación del método OnClickListener a la variable Aceptar
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Condición if que evalúa que el cuadro de texto no esté vacío
                if (codigo.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "UPC vacío",Toast.LENGTH_SHORT).show();
                } else {
                    //Se agrega el contenido del cuadro de texto a la lista y se limpia el mismo
                    productos.add(codigo.getText().toString());
                    adaptador1.notifyDataSetChanged();
                    codigo.setText("");

                }

            }
        });

        //Asignación del método onClicListener al botón cancelar
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String mensaje;
                //Intent que envía al MuebleActivity con el mismo número de mueble ingresado y un mensaje de advertencia
                Intent intent = new Intent(EscaneoActivity.this, MuebleActivity.class);
                mensaje = "¡Si especifica un mueble diferente al: " + recibido + " perderá la información capturada!";
                intent.putExtra("Mensaje", mensaje);
                intent.putExtra("Mtienda", recibido);
                startActivity(intent);
            }
        });

        //Asignación del método onClickListener al botón finalizar
        finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
