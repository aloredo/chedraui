package netasystems.net.pantallaschedraui.Activity;
/*
    @autor Neta Systems José Alfredo Santiago

 */

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import netasystems.net.pantallaschedraui.R;

public class LoginActivity extends AppCompatActivity {


    /*
    Declaración de cajas de texto y botones
     */
    EditText Usuario, Contra;
    Button Cancelar, Aceptar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /*
        Asignación de las variables tipo Botón por Id
         */
        Cancelar = (Button) findViewById(R.id.btnCancelar);
        Aceptar = (Button) findViewById(R.id.btnSiguiente);

        /*
        Aplicación del método onClickListener al botón aceptar
         */
        Aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Asignación de las variables tipo EditText por Id
                 */
                Usuario = (EditText)findViewById(R.id.edClase);
                Contra = (EditText)findViewById(R.id.edPass);

                //Condición if que evalua que los campos de usuario y contraseña ingresados correspondan a los parametros predeterminados para ingresar
                if (Usuario.getText().toString().equals("Admin") && Contra.getText().toString().equals("123")) {
                    //Intent que envía al usuario al activity Especificación
                    Intent intent2 = new Intent(LoginActivity.this, EspecificacionActivity.class);
                    startActivity(intent2);
                } else {
                    //Mensaje emergente que notifica al usuario que su usuario y contraseña son incorrectos
                    Toast.makeText(getApplicationContext(), "Usuario y/o contraseña no válidos", Toast.LENGTH_SHORT).show();
                }


            }
        });
        //Asignación del método onClickListener al botón Cancelar
        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Método que finaliza la aplicación
                finish();
            }
        });


    }
}
