package netasystems.net.pantallaschedraui.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import netasystems.net.pantallaschedraui.R;

public class AuditoriaPrecios extends AppCompatActivity {

    EditText ean, sku;
    Button aceptar, cancelar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auditoria_precios);

        aceptar = (Button)findViewById(R.id.btnSiguiente);
        cancelar = (Button)findViewById(R.id.btnCancelar);

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Auditar();
            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AuditoriaPrecios.this, MenúPrincipalActivity.class);
                startActivity(intent);
            }
        });
    }

    public void Auditar() {
        ean = (EditText)findViewById(R.id.edEan);
        sku = (EditText) findViewById(R.id.edSku);

        if (ean.getText().toString().isEmpty() && sku.getText().toString().isEmpty()){
            Toast.makeText(getApplicationContext(),"Favor de ingresar el EAN y el Precio", Toast.LENGTH_SHORT).show();
        }else if(ean.getText().equals("") && !sku.getText().toString().isEmpty()){
            Toast.makeText(getApplicationContext(), "Favor de Ingresar el EAN", Toast.LENGTH_SHORT).show();
        }else if (!ean.getText().toString().isEmpty() && sku.getText().toString().isEmpty()){
            Toast.makeText(getApplicationContext(), "Favor de ingresar el Precio", Toast.LENGTH_SHORT).show();
        }else if(ean.getText().length() > 18){
            Toast.makeText(getApplicationContext(),"El EAN no debe ser mayor a 18 digitos", Toast.LENGTH_SHORT).show();
        }else if (sku.getText().length() > 8){
            Toast.makeText(getApplicationContext(),"El precio no puede ser mayor de 8 digitos",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(getApplicationContext(), "Sincronización Exitosa",Toast.LENGTH_SHORT).show();
        }
    }
}
