package netasystems.net.pantallaschedraui.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import netasystems.net.pantallaschedraui.R;

public class ConfirmarPrecioMueble extends AppCompatActivity {

    EditText ean, sku;
    Button siguiente, cancelar, limpiar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar_precio_mueble);

        ean = (EditText)findViewById(R.id.edEan);
        sku = (EditText)findViewById(R.id.edSku);
        limpiar = (Button)findViewById(R.id.btnLimpiar);
        siguiente = (Button)findViewById(R.id.btnSiguiente);
        cancelar = (Button)findViewById(R.id.btnCancelar);

        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ean.setText("");
                sku.setText("");
            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmarPrecioMueble.this,MenúPrincipalActivity.class);
            }
        });
    }
}
