package netasystems.net.pantallaschedraui.Activity;

/*
    Importación delas clases de la aplicación

 */

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import netasystems.net.pantallaschedraui.R;

public class MenúPrincipalActivity extends AppCompatActivity {

    TextView a, b;
    Button etiqueta, rotulo, masivas, confirmacion,escaneos,aceptar,salir;
    Spinner impresora, ip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menuprincipal);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        a = (TextView)findViewById(R.id.txtImpresora);
        b = (TextView)findViewById(R.id.txtIp);
        impresora = (Spinner)findViewById(R.id.spImpresora);
        ip = (Spinner)findViewById(R.id.spIp);
        aceptar = (Button)findViewById(R.id.btnLimpiar);
        final Button escaneos, etiqueta, masivas, confirmacion;


        escaneos = (Button)findViewById(R.id.btnEscaneos);
        etiqueta = (Button)findViewById(R.id.btnEtiqueta);
        masivas = (Button)findViewById(R.id.btnMasivas);
        confirmacion = (Button)findViewById(R.id.btnConfirmacion);



        String[] datos = new String[] {" ","ZebraQL220Plus", "HP", "Cannon",};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, datos);

        impresora.setAdapter(adapter);String[] datos2 = new String[] {" ","192.168.23.1", "192.54.34.21", "192.164.44.21",};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, datos2);
        ip.setAdapter(adapter2);
        salir = (Button)findViewById(R.id.btnSalir);

        escaneos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                masivas.setEnabled(false);
                etiqueta.setEnabled(false);
                confirmacion.setEnabled(false);
                escaneos.setEnabled(false);
                a.setVisibility(View.VISIBLE);
                b.setVisibility(View.VISIBLE);
                ip.setVisibility(View.VISIBLE);
                impresora.setVisibility(View.VISIBLE);
                aceptar.setVisibility(View.VISIBLE);
                aceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tipoImpresora = impresora.getSelectedItem().toString();
                        String ipUsada = ip.getSelectedItem().toString();
                        if (tipoImpresora == " "|| ipUsada == " "){
                            Toast toast = Toast.makeText(getApplicationContext(), "Selecciona un tipo de impresora y/o IP", Toast.LENGTH_SHORT);
                            toast.show();

                        } else {

                            Intent intent = new Intent(MenúPrincipalActivity.this, MenuEscaneoActivity.class);
                            startActivity(intent);
                        }
                    }
                });


            }
        });

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (masivas.isEnabled() && etiqueta.isEnabled() && confirmacion.isEnabled() && escaneos.isEnabled()){
                    Intent intent = new Intent(MenúPrincipalActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else {
                    masivas.setEnabled(true);
                    etiqueta.setEnabled(true);
                    confirmacion.setEnabled(true);
                    escaneos.setEnabled(true);
                    a.setVisibility(View.GONE);
                    b.setVisibility(View.GONE);
                    ip.setVisibility(View.GONE);
                    impresora.setVisibility(View.GONE);
                    aceptar.setVisibility(View.GONE);
                }

            }
        });

        confirmacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                masivas.setEnabled(false);
                etiqueta.setEnabled(false);
                confirmacion.setEnabled(false);
                escaneos.setEnabled(false);
                a.setVisibility(View.VISIBLE);
                b.setVisibility(View.VISIBLE);
                ip.setVisibility(View.VISIBLE);
                impresora.setVisibility(View.VISIBLE);
                aceptar.setVisibility(View.VISIBLE);
                aceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tipoImpresora = impresora.getSelectedItem().toString();
                        String ipUsada = ip.getSelectedItem().toString();
                        if (tipoImpresora == " "|| ipUsada == " "){
                            Toast toast = Toast.makeText(getApplicationContext(), "Selecciona un tipo de impresora y/o IP", Toast.LENGTH_SHORT);
                            toast.show();

                        } else {

                            Intent intent = new Intent(MenúPrincipalActivity.this, MenuConfirmacion.class);
                            startActivity(intent);
                        }
                    }
                });
            }
        });

        masivas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                masivas.setEnabled(false);
                etiqueta.setEnabled(false);
                confirmacion.setEnabled(false);
                escaneos.setEnabled(false);
                a.setVisibility(View.VISIBLE);
                b.setVisibility(View.VISIBLE);
                ip.setVisibility(View.VISIBLE);
                impresora.setVisibility(View.VISIBLE);
                aceptar.setVisibility(View.VISIBLE);
                aceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tipoImpresora = impresora.getSelectedItem().toString();
                        String ipUsada = ip.getSelectedItem().toString();
                        if (tipoImpresora == " "|| ipUsada == " "){
                            Toast toast = Toast.makeText(getApplicationContext(), "Selecciona un tipo de impresora y/o IP", Toast.LENGTH_SHORT);
                            toast.show();

                        } else {

                            Intent intent = new Intent(MenúPrincipalActivity.this, EtiquetasMasivas.class);
                            startActivity(intent);
                        }
                    }
                });
            }
        });

    }




}
