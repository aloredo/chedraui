package netasystems.net.pantallaschedraui.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import netasystems.net.pantallaschedraui.R;

public class ConfirmarPrecioSinCenso extends AppCompatActivity {

    Button siguiente, cancelar, limpiar;
    EditText ean, sku;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar_precio_sin_censo);

        siguiente = (Button)findViewById(R.id.btnSiguiente);
        cancelar = (Button)findViewById(R.id.btnCancelar);
        limpiar = (Button)findViewById(R.id.btnLimpiar);
        ean = (EditText)findViewById(R.id.edEan);
        sku = (EditText)findViewById(R.id.edSku);

        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ean.setText("");
                sku.setText("");
            }
        });


    }
}
