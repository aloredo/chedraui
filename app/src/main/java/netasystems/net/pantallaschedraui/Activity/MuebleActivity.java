package netasystems.net.pantallaschedraui.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import netasystems.net.pantallaschedraui.R;

public class MuebleActivity extends AppCompatActivity {

    EditText mueble;
    Button aceptar, cancelar;
    TextView alerta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mueble);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mueble = (EditText)findViewById(R.id.eMueble);
        aceptar = (Button) findViewById(R.id.btnLimpiar);
        cancelar = (Button) findViewById(R.id.btnCancelar);
        alerta = (TextView) findViewById(R.id.txtAlerta);

        String muebleRecibido, mensaje;

        muebleRecibido = getIntent().getStringExtra("Mtienda");
        mensaje = getIntent().getStringExtra("Mensaje");

        mueble.setText(muebleRecibido);
        alerta.setText(mensaje);



        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mueble.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Ingresa un número de mueble",Toast.LENGTH_SHORT).show();
                }

                    else{
                    String men = mueble.getText().toString();
                    Intent intent = new Intent(MuebleActivity.this, EscaneoActivity.class);
                    intent.putExtra("Mueble", men);
                    startActivity(intent);
                }

            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MuebleActivity.this, MenuEscaneoActivity.class);
                startActivity(intent);
            }
        });
    }
}
