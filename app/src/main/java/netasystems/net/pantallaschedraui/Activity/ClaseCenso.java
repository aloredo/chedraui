package netasystems.net.pantallaschedraui.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import netasystems.net.pantallaschedraui.R;

public class ClaseCenso extends AppCompatActivity {

    EditText clase;
    Button aceptar, cancelar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clase_censo);

        aceptar = (Button)findViewById(R.id.btnSiguiente);
        cancelar = (Button)findViewById(R.id.btnCancelar);

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarClase();
            }
        });
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClaseCenso.this, MenúPrincipalActivity.class);
                startActivity(intent);
            }
        });
    }

    public void validarClase(){
        clase = (EditText)findViewById(R.id.edClase);
        if (clase.getText().toString().isEmpty()){
            Toast.makeText(getApplicationContext(), "Favor de específicar la Clase", Toast.LENGTH_SHORT).show();
        }
    }
}
