
package netasystems.net.pantallaschedraui.Activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import netasystems.net.pantallaschedraui.DTOS.FaltanteAnaquelTemp;
import netasystems.net.pantallaschedraui.DTOS.PerfilAutenticacion;
import netasystems.net.pantallaschedraui.R;

public class MuebleFaltanteAnaquel extends AppCompatActivity {

    EditText et_Mueble;
    TextView lblResultado;
    TextView lblMensaje;

    private static Boolean resultado;
    private String mueble;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mueble_faltante_anaquel);

            et_Mueble = (EditText) findViewById(R.id.eMueble);
            lblResultado = (TextView) findViewById(R.id.lblResultado);
            lblMensaje = (TextView) findViewById(R.id.lblMensaje);

            if (resultado)
            {
                String resultado = "Información del mueble: " + PerfilAutenticacion.getMuebleFaltanteAnaquel() + " guardada correctamente!!";
                resultado = this.lblResultado.getText().toString();
                this.lblResultado.setVisibility(View.INVISIBLE);
                this.lblResultado.setTextColor(Color.BLUE);

                //this.lblResultado.Visible = true;
                //this.lblResultado.ForeColor = Color.Blue;
            }
            else
            {
                this.lblResultado.setVisibility(View.INVISIBLE);
                //this.lblResultado.Visible = false;
/*
                mueble = FaltanteAnaquelTemp.ObtenerMueble();

                if (mueble != null && mueble.Length > 0)
                {
                    this.txtMueble.Text = mueble;
                    this.lblMensaje.Text = "Si especifica un mueble diferente al: " + mueble.ToString() + " perderá la información capturada!!";
                    this.lblMensaje.Visible = true;
                    this.lblMensaje.ForeColor = Color.Red;
                }
                */
            }

    }





/*
    //#region Eventos
    private void btnAceptar_Click(object sender, EventArgs e)
    {
        this.Mueble();
    }

    private void btnCancelar_Click(object sender, EventArgs e)
    {
        Form miForma = new MenuPrincipalForm();
        miForma.Show();
        this.Close();
    }

    private void txtMueble_KeyPress(object sender, KeyPressEventArgs e)
    {
        if (e.KeyChar == '\r')
        {
            e.Handled = true;
            this.Mueble();
        }
    }
        #endregion

    //#region Metodos
    private void Mueble()
    {
        Cursor.Current = Cursors.WaitCursor;
        ValidaNumero Verificador = new ValidaNumero();
        if (this.txtMueble.Text.Length == 0)
        {
            this.lblMensaje.Text = "Especifique el número del mueble!!";
            this.lblMensaje.ForeColor = Color.Red;
            this.lblMensaje.Visible = true;
        }
        else if (!Verificador.Validar(this.txtMueble.Text))
        {
            this.lblMensaje.Text = "El mueble debe ser de tipo numérico!!";
            this.lblMensaje.ForeColor = Color.Red;
            this.lblMensaje.Visible = true;
        }
        else
        {
            PerfilAutenticacion.MuebleFaltanteAnaquel = this.txtMueble.Text.ToString();

            if (this.mueble != null)
            {
                if (!this.mueble.Equals(this.txtMueble.Text.ToString()))
                {
                    FaltanteAnaquelTemp.BorrarMueble(mueble);
                }
            }

            Form miForma = new EscaneoFaltanteAnaquelForm();
            miForma.Show();
            this.Close();
        }
        Cursor.Current = Cursors.Default;
    }
    //#endregion


*/


}
