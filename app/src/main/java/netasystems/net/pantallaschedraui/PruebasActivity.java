package netasystems.net.pantallaschedraui;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.kobjects.base64.Base64;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import netasystems.net.pantallaschedraui.DTOS.zep_get_parameters.TableOfZtmprParam;
import netasystems.net.pantallaschedraui.DTOS.zep_get_parameters.ZepGetParametersResponse;
import netasystems.net.pantallaschedraui.DTOS.zep_get_parameters.ZtmprParam;
import netasystems.net.pantallaschedraui.DTOS.zusuariosaudit.TableOfZtusuarioaudit;
import netasystems.net.pantallaschedraui.DTOS.zusuariosaudit.Ztusuarioaudit;
import netasystems.net.pantallaschedraui.ServicesClient.ZUSUARIOSAUDITService;

public class PruebasActivity extends AppCompatActivity {


    //Declaración de Web Services
    private final String nameSpaceSoap = "http://schemas.xmlsoap.org/soap/envelope/";

    private final String nameSpaceSap = "urn:sap-com:document:sap:soap:functions:mc-style";

    private final String methodSap = "ZepGetParameters";

    private final String soapActionSap = "urn:sap-com:document:sap:soap:functions:mc-style/ZepGetParameters";

    private final String urlSap = "http://chderp00.gcch.com:8010/sap/bc/srt/rfc/sap/zep_get_parameters/300/zep_get_parametersservice/zep_get_parametersservice";
    //private final String urlSap = "http://chqecc01.gcch.com:8010/sap/bc/srt/wsdl/bndg_4FEDDF11F9591F60E1008000AC1CFA8E/wsdl11/allinone/ws_policy/document?sap-client=300";

    //private final String urlSap = "http://chqecc01.gcch.com:8010/sap/bc/srt/wsdl/bndg_532992F94B860660E1008000AC1CFA8E/wsdl11/allinone/ws_policy/document?sap-client=300";

    boolean error;


    Button btnConsulta;
    TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pruebas);

        btnConsulta = (Button) findViewById(R.id.btn_Consulta);
        tvResultado = (TextView) findViewById(R.id.tv_Resultado);

        btnConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TareaWSConsulta hilo = new TareaWSConsulta();
                hilo.execute();

            }
        });
    }





    //Metodo en Segundo Plano
    private class TareaWSConsulta extends AsyncTask<String, String, String> {

        //Se ejecuta al Inicio
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            consulta();
            return null;
        }

        //Despues de la ejecucion
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //CargarDatos(s);
        }
    }


    public void consulta() {

        SoapObject soapObject = new SoapObject(nameSpaceSap,methodSap);

        PropertyInfo propertyInfoZobj1 = new PropertyInfo();
        PropertyInfo propertyInfoZprog = new PropertyInfo();

        //Declaracion de variables de la clase y asignacion de valores
        propertyInfoZobj1.setName("Zobj1");
        propertyInfoZobj1.setValue("VERSION");
        propertyInfoZobj1.setType(String.class);
        soapObject.addProperty(propertyInfoZobj1);


        propertyInfoZprog.setName("Zprog");
        propertyInfoZprog.setValue("AUDITORIAPRECIOS");
        propertyInfoZprog.setType(String.class);
        soapObject.addProperty(propertyInfoZprog);

        //Parametros de la clase input
        TableOfZtmprParam tableOfZtmprParam = new TableOfZtmprParam();
        soapObject.addProperty("TyTParametros", tableOfZtmprParam);

        ZtmprParam tableOfZtmprParam2 = new ZtmprParam();
        soapObject.addProperty("item", tableOfZtmprParam2);


        //Response
        ZepGetParametersResponse zepGetParametersResponse = new ZepGetParametersResponse();
        soapObject.addProperty("TyTParametros", zepGetParametersResponse);




        //Parametros de la clase input
        //TableOfZtusuarioaudit tableOfZtusuarioaudit = new TableOfZtusuarioaudit();
        //soapObject.addProperty("TUsuarios", tableOfZtusuarioaudit);

        //Ztusuarioaudit ztusuarioaudit = new Ztusuarioaudit();
        //soapObject.addProperty("item", ztusuarioaudit);


        SoapSerializationEnvelope envelopeSap = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelopeSap.env = nameSpaceSoap;
        envelopeSap.dotNet = false;
        envelopeSap.implicitTypes = true;

        //envelopeSap.addMapping(nameSpaceSap, "TUsuarios", new TableOfZtusuarioaudit().getClass());
        //envelopeSap.addMapping(nameSpaceSap,"item", new Ztusuarioaudit().getClass());

        envelopeSap.addMapping(nameSpaceSap,"TyTParametros", new TableOfZtmprParam().getClass());
        envelopeSap.addMapping(nameSpaceSap, "item", new ZtmprParam().getClass());


        envelopeSap.setOutputSoapObject(soapObject);

        HttpTransportSE httpTransportSE = new HttpTransportSE(urlSap,3000);
        httpTransportSE.debug = true;


        //List<HeaderProperty> c = new ArrayList<HeaderProperty>();
        //c.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode("USER:PASS".getBytes())));
        //c.add(new HeaderProperty("Connection", "Keep-Alive"));
        //httpTransportSE.debug = true;

        //c.add(new HeaderProperty("Authorization","Basic dmNhcm1vbmE6QmFkc2Vhc3Nvbi4yMDE4"));
        //c.add(new HeaderProperty("Username","vcarmona"));
        //c.add(new HeaderProperty("Password","pale1481"));




        //List<HeaderProperty> c = new ArrayList<HeaderProperty>();
        //c.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode("vcarmona:pale1481".getBytes())));

        // c.add(new HeaderProperty("Username",org.kobjects.base64.Base64.encode("vcarmona".getBytes())));
        //c.add(new HeaderProperty("Password",org.kobjects.base64.Base64.encode("pale1481".getBytes())));

        //c.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode("vcarmona pale1481".getBytes())));

        //String test = org.kobjects.base64.Base64.encode("vcarmona pale1481".getBytes());

        //List<HeaderProperty> c = new ArrayList<HeaderProperty>();

        //String test = org.kobjects.base64.Base64.encode("vcarmona:pale1481".getBytes());
        //System.out.println("------Me canso ganso< "+test+" >");

        //c.add(new HeaderProperty("Authorization","Basic " + test + ""));
        //c.add(new HeaderProperty("Authorization", "Basic dmNhcm1vbmE6cGFsZTE0ODE="));
        //String test = org.kobjects.base64.Base64.encode("vcarmona pale1481".getBytes());


        //c.add(new HeaderProperty("Authorization","Basic dmNhcm1vbmE6cGFsZTE0ODE="));

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            //headerList.add(new HeaderProperty("Authorization", "Basic :" + Base64.encode("vcarmona:pale1481".getBytes())));
            headerList.add(new HeaderProperty("Authorization","Basic dmNhcm1vbmE6QmFkc2Vhc3Nvbi4yMDE4"));


            httpTransportSE.call(soapActionSap, envelopeSap, headerList);

            SoapObject response = (SoapObject) envelopeSap.getResponse();

            Log.d("dump Request: ",httpTransportSE.requestDump);
            Log.d("dump Respondes: ",httpTransportSE.responseDump);


            //SoapObject response = (SoapObject) envelopeSap.bodyIn;

        } catch (IOException ioe) {
            Log.d("IOException", ioe.toString());
            //Toast.makeText(getApplicationContext(),"No hay conexión hacia el servidor", Toast.LENGTH_LONG).show();
            error = true;
        } catch (XmlPullParserException xppe) {
            Log.d("XmlPullParserException",xppe.toString());
        } catch (Exception ex) {
            Log.d("Exception",ex.toString());
            //ex.printStackTrace();
        }
    }

}

