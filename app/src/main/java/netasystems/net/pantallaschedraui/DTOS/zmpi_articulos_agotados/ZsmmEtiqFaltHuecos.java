package netasystems.net.pantallaschedraui.DTOS.zmpi_articulos_agotados;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZsmmEtiqFaltHuecos {

    /**
     * Declaración variables
     */

    private String Matnr;
    private String Werks;
    private String ShelfId;
    private double Trame;
    private String Mstae;
    private String Maktx;
    private String Noimp;
    private String Etiqueta;

    /**
     * Declaración de constructor
     * @param matnr
     * @param werks
     * @param shelfId
     * @param trame
     * @param mstae
     * @param maktx
     * @param noimp
     * @param etiqueta
     */

    public ZsmmEtiqFaltHuecos(String matnr, String werks, String shelfId, double trame, String mstae, String maktx, String noimp, String etiqueta) {
        Matnr = matnr;
        Werks = werks;
        ShelfId = shelfId;
        Trame = trame;
        Mstae = mstae;
        Maktx = maktx;
        Noimp = noimp;
        Etiqueta = etiqueta;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getMatnr() {
        return Matnr;
    }

    public void setMatnr(String matnr) {
        Matnr = matnr;
    }

    public String getWerks() {
        return Werks;
    }

    public void setWerks(String werks) {
        Werks = werks;
    }

    public String getShelfId() {
        return ShelfId;
    }

    public void setShelfId(String shelfId) {
        ShelfId = shelfId;
    }

    public double getTrame() {
        return Trame;
    }

    public void setTrame(double trame) {
        Trame = trame;
    }

    public String getMstae() {
        return Mstae;
    }

    public void setMstae(String mstae) {
        Mstae = mstae;
    }

    public String getMaktx() {
        return Maktx;
    }

    public void setMaktx(String maktx) {
        Maktx = maktx;
    }

    public String getNoimp() {
        return Noimp;
    }

    public void setNoimp(String noimp) {
        Noimp = noimp;
    }

    public String getEtiqueta() {
        return Etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        Etiqueta = etiqueta;
    }
}
