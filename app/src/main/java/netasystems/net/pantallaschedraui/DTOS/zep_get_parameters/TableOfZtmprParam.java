package netasystems.net.pantallaschedraui.DTOS.zep_get_parameters;

import android.renderscript.Sampler;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class TableOfZtmprParam implements KvmSerializable {

    /**
     * Declaración de clase
     */
    private ZtmprParam ztmprParam = new ZtmprParam();


    @Override
    public Object getProperty(int index) {
        switch (index){
            case 0:
                return ztmprParam;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index){
            case 0:
                this.ztmprParam = (ZtmprParam) value;
                break;
            default:
                break;
        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        switch (index){
            case 0:
                info.type = ZtmprParam.class;
                info.name = "item";
                break;
            default:
                break;
        }

    }
}
