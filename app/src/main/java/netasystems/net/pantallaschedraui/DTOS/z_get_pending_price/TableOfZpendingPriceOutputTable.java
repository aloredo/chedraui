package netasystems.net.pantallaschedraui.DTOS.z_get_pending_price;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class TableOfZpendingPriceOutputTable implements KvmSerializable {

    /**
     * Declaración de lista
     */

    private List<ZpendingPriceOutputTable> item;

    /**
     * Declaraciín de constructor
     * @param item
     */

    public TableOfZpendingPriceOutputTable(List<ZpendingPriceOutputTable> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<ZpendingPriceOutputTable> getItem() {
        return item;
    }

    public void setItem(List<ZpendingPriceOutputTable> item) {
        this.item = item;
    }

    private Zreturndata zreturndata = new Zreturndata();

    @Override
    public Object getProperty(int index) {
        switch (index){
            case 0:
                return zreturndata;
                default:
                    break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index){
            case 0:
                this.zreturndata = (Zreturndata) value;
                break;
            default:
                break;
        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable hashtable, PropertyInfo info) {
        switch (index){
            case 0:
                info.type = Zreturndata.class;
                info.name = "item";
                break;
            default:
                break;
        }

    }
}
