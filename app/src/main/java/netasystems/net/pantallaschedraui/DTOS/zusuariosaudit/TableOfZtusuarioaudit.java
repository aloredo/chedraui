package netasystems.net.pantallaschedraui.DTOS.zusuariosaudit;

import java.io.Serializable;
import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class TableOfZtusuarioaudit implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Declaración de lista
     */
    private List<Ztusuarioaudit> item;

    /**
     * Declaracion de constructor vacio
     */
    public TableOfZtusuarioaudit() {

    }
    /**
     * Declaración de constructor
     * @param item
     */
    public TableOfZtusuarioaudit(List<Ztusuarioaudit> item) {
        this.item = item;
    }



    /**
     * Getters y Setters
     * @return
     */

    public List<Ztusuarioaudit> getItem() {
        return item;
    }

    public void setItem(List<Ztusuarioaudit> item) {
        this.item = item;
    }
}
