package netasystems.net.pantallaschedraui.DTOS.zmpi_art_anaquel;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

class ZartAnaquel {

    /**
     * Declaración de variables
     */

    private String Mandt;   //clnt3
    private String Ztienda; //char4
    private String Zmueble; //char10
    private String Zarticulo;   //char18
    private String FechaCreacion;   //char8
    private String Zgpoarticulo;    //char9
    private String Zescaneado;  //char2
    private String Zdepto;  //char18
    private String Znnomdepto;  //char40
    private String Zsubdepto;   //char18
    private String Znomsubdepto;    //char40
    private String Zclase;  //char18
    private String Znomclase;   //char40
    private String Zsubclase;   //char18
    private String Znomsubclase;    //char40
    private String Zupc;    //char18
    private String Zdescripcion;    //char40
    private String Zproveedor;  //char10
    private String Zpnombre;    //char35
    private double Zexistencia;    //quantum13.3
    private double Zventa; //quantum13.3
    private String Estatus; //char2
    private String UsuarioCreacion; //char12
    private String HoraCreacion;    //time
    private String Zarticulomms;    //char18
    private String Zmstae;  //char2
    private String Zsurtido;    //char1
    private String Zmmsta;  //char2

    /**
     * Declaración de constructor
     * @param mandt
     * @param ztienda
     * @param zmueble
     * @param zarticulo
     * @param fechaCreacion
     * @param zgpoarticulo
     * @param zescaneado
     * @param zdepto
     * @param znnomdepto
     * @param zsubdepto
     * @param znomsubdepto
     * @param zclase
     * @param znomclase
     * @param zsubclase
     * @param znomsubclase
     * @param zupc
     * @param zdescripcion
     * @param zproveedor
     * @param zpnombre
     * @param zexistencia
     * @param zventa
     * @param estatus
     * @param usuarioCreacion
     * @param horaCreacion
     * @param zarticulomms
     * @param zmstae
     * @param zsurtido
     * @param zmmsta
     */

    public ZartAnaquel(String mandt, String ztienda, String zmueble, String zarticulo, String fechaCreacion, String zgpoarticulo, String zescaneado, String zdepto, String znnomdepto, String zsubdepto, String znomsubdepto, String zclase, String znomclase, String zsubclase, String znomsubclase, String zupc, String zdescripcion, String zproveedor, String zpnombre, double zexistencia, double zventa, String estatus, String usuarioCreacion, String horaCreacion, String zarticulomms, String zmstae, String zsurtido, String zmmsta) {
        Mandt = mandt;
        Ztienda = ztienda;
        Zmueble = zmueble;
        Zarticulo = zarticulo;
        FechaCreacion = fechaCreacion;
        Zgpoarticulo = zgpoarticulo;
        Zescaneado = zescaneado;
        Zdepto = zdepto;
        Znnomdepto = znnomdepto;
        Zsubdepto = zsubdepto;
        Znomsubdepto = znomsubdepto;
        Zclase = zclase;
        Znomclase = znomclase;
        Zsubclase = zsubclase;
        Znomsubclase = znomsubclase;
        Zupc = zupc;
        Zdescripcion = zdescripcion;
        Zproveedor = zproveedor;
        Zpnombre = zpnombre;
        Zexistencia = zexistencia;
        Zventa = zventa;
        Estatus = estatus;
        UsuarioCreacion = usuarioCreacion;
        HoraCreacion = horaCreacion;
        Zarticulomms = zarticulomms;
        Zmstae = zmstae;
        Zsurtido = zsurtido;
        Zmmsta = zmmsta;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getMandt() {
        return Mandt;
    }

    public void setMandt(String mandt) {
        Mandt = mandt;
    }

    public String getZtienda() {
        return Ztienda;
    }

    public void setZtienda(String ztienda) {
        Ztienda = ztienda;
    }

    public String getZmueble() {
        return Zmueble;
    }

    public void setZmueble(String zmueble) {
        Zmueble = zmueble;
    }

    public String getZarticulo() {
        return Zarticulo;
    }

    public void setZarticulo(String zarticulo) {
        Zarticulo = zarticulo;
    }

    public String getFechaCreacion() {
        return FechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        FechaCreacion = fechaCreacion;
    }

    public String getZgpoarticulo() {
        return Zgpoarticulo;
    }

    public void setZgpoarticulo(String zgpoarticulo) {
        Zgpoarticulo = zgpoarticulo;
    }

    public String getZescaneado() {
        return Zescaneado;
    }

    public void setZescaneado(String zescaneado) {
        Zescaneado = zescaneado;
    }

    public String getZdepto() {
        return Zdepto;
    }

    public void setZdepto(String zdepto) {
        Zdepto = zdepto;
    }

    public String getZnnomdepto() {
        return Znnomdepto;
    }

    public void setZnnomdepto(String znnomdepto) {
        Znnomdepto = znnomdepto;
    }

    public String getZsubdepto() {
        return Zsubdepto;
    }

    public void setZsubdepto(String zsubdepto) {
        Zsubdepto = zsubdepto;
    }

    public String getZnomsubdepto() {
        return Znomsubdepto;
    }

    public void setZnomsubdepto(String znomsubdepto) {
        Znomsubdepto = znomsubdepto;
    }

    public String getZclase() {
        return Zclase;
    }

    public void setZclase(String zclase) {
        Zclase = zclase;
    }

    public String getZnomclase() {
        return Znomclase;
    }

    public void setZnomclase(String znomclase) {
        Znomclase = znomclase;
    }

    public String getZsubclase() {
        return Zsubclase;
    }

    public void setZsubclase(String zsubclase) {
        Zsubclase = zsubclase;
    }

    public String getZnomsubclase() {
        return Znomsubclase;
    }

    public void setZnomsubclase(String znomsubclase) {
        Znomsubclase = znomsubclase;
    }

    public String getZupc() {
        return Zupc;
    }

    public void setZupc(String zupc) {
        Zupc = zupc;
    }

    public String getZdescripcion() {
        return Zdescripcion;
    }

    public void setZdescripcion(String zdescripcion) {
        Zdescripcion = zdescripcion;
    }

    public String getZproveedor() {
        return Zproveedor;
    }

    public void setZproveedor(String zproveedor) {
        Zproveedor = zproveedor;
    }

    public String getZpnombre() {
        return Zpnombre;
    }

    public void setZpnombre(String zpnombre) {
        Zpnombre = zpnombre;
    }

    public double getZexistencia() {
        return Zexistencia;
    }

    public void setZexistencia(double zexistencia) {
        Zexistencia = zexistencia;
    }

    public double getZventa() {
        return Zventa;
    }

    public void setZventa(double zventa) {
        Zventa = zventa;
    }

    public String getEstatus() {
        return Estatus;
    }

    public void setEstatus(String estatus) {
        Estatus = estatus;
    }

    public String getUsuarioCreacion() {
        return UsuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        UsuarioCreacion = usuarioCreacion;
    }

    public String getHoraCreacion() {
        return HoraCreacion;
    }

    public void setHoraCreacion(String horaCreacion) {
        HoraCreacion = horaCreacion;
    }

    public String getZarticulomms() {
        return Zarticulomms;
    }

    public void setZarticulomms(String zarticulomms) {
        Zarticulomms = zarticulomms;
    }

    public String getZmstae() {
        return Zmstae;
    }

    public void setZmstae(String zmstae) {
        Zmstae = zmstae;
    }

    public String getZsurtido() {
        return Zsurtido;
    }

    public void setZsurtido(String zsurtido) {
        Zsurtido = zsurtido;
    }

    public String getZmmsta() {
        return Zmmsta;
    }

    public void setZmmsta(String zmmsta) {
        Zmmsta = zmmsta;
    }
}
