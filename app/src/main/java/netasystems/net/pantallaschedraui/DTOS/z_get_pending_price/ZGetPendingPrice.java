package netasystems.net.pantallaschedraui.DTOS.z_get_pending_price;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZGetPendingPrice implements KvmSerializable {

    /**
     * Declaración de listas y variables
     */

    private String IClass;
    private String IConfirArt;
    private String IEan11;
    private String IEstatusPos;
    private String IImprimir;
    private String ISinCensoDetalle;
    private String ISinCensoTotal;
    private String IWerks;
    private String IZmaterial;
    private List<Zreturndata> IZreturndata;
    private String IZshelfId;
    private List<TableOfZpendingPriceOutputTable> ItOutputTable;
    private List<TableOfZpreciosPendtes> ItPreciosPendtes;

    public ZGetPendingPrice(){
        this.IClass = "";
        this.IConfirArt = "";
        this.IEan11 = "";
        this.IEstatusPos = "";
        this.IImprimir = "";
        this.ISinCensoDetalle = "";
        this.ISinCensoTotal = "";
        this.IWerks = "";
        this.IZmaterial = "";
        this.IZshelfId = "";
    }

    /**
     * Declaración de constructor
     * @param IClass
     * @param IConfirArt
     * @param IEan11
     * @param IEstatusPos
     * @param IImprimir
     * @param ISinCensoDetalle
     * @param ISinCensoTotal
     * @param IWerks
     * @param IZmaterial
     * @param IZreturndata
     * @param IZshelfId
     * @param itOutputTable
     * @param itPreciosPendtes
     */

    public ZGetPendingPrice(String IClass, String IConfirArt, String IEan11, String IEstatusPos, String IImprimir, String ISinCensoDetalle, String ISinCensoTotal, String IWerks, String IZmaterial, List<Zreturndata> IZreturndata, String IZshelfId, List<TableOfZpendingPriceOutputTable> itOutputTable, List<TableOfZpreciosPendtes> itPreciosPendtes) {
        this.IClass = IClass;
        this.IConfirArt = IConfirArt;
        this.IEan11 = IEan11;
        this.IEstatusPos = IEstatusPos;
        this.IImprimir = IImprimir;
        this.ISinCensoDetalle = ISinCensoDetalle;
        this.ISinCensoTotal = ISinCensoTotal;
        this.IWerks = IWerks;
        this.IZmaterial = IZmaterial;
        this.IZreturndata = IZreturndata;
        this.IZshelfId = IZshelfId;
        ItOutputTable = itOutputTable;
        ItPreciosPendtes = itPreciosPendtes;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getIClass() {
        return IClass;
    }

    public void setIClass(String IClass) {
        this.IClass = IClass;
    }

    public String getIConfirArt() {
        return IConfirArt;
    }

    public void setIConfirArt(String IConfirArt) {
        this.IConfirArt = IConfirArt;
    }

    public String getIEan11() {
        return IEan11;
    }

    public void setIEan11(String IEan11) {
        this.IEan11 = IEan11;
    }

    public String getIEstatusPos() {
        return IEstatusPos;
    }

    public void setIEstatusPos(String IEstatusPos) {
        this.IEstatusPos = IEstatusPos;
    }

    public String getIImprimir() {
        return IImprimir;
    }

    public void setIImprimir(String IImprimir) {
        this.IImprimir = IImprimir;
    }

    public String getISinCensoDetalle() {
        return ISinCensoDetalle;
    }

    public void setISinCensoDetalle(String ISinCensoDetalle) {
        this.ISinCensoDetalle = ISinCensoDetalle;
    }

    public String getISinCensoTotal() {
        return ISinCensoTotal;
    }

    public void setISinCensoTotal(String ISinCensoTotal) {
        this.ISinCensoTotal = ISinCensoTotal;
    }

    public String getIWerks() {
        return IWerks;
    }

    public void setIWerks(String IWerks) {
        this.IWerks = IWerks;
    }

    public String getIZmaterial() {
        return IZmaterial;
    }

    public void setIZmaterial(String IZmaterial) {
        this.IZmaterial = IZmaterial;
    }

    public List<Zreturndata> getIZreturndata() {
        return IZreturndata;
    }

    public void setIZreturndata(List<Zreturndata> IZreturndata) {
        this.IZreturndata = IZreturndata;
    }

    public String getIZshelfId() {
        return IZshelfId;
    }

    public void setIZshelfId(String IZshelfId) {
        this.IZshelfId = IZshelfId;
    }

    public List<TableOfZpendingPriceOutputTable> getItOutputTable() {
        return ItOutputTable;
    }

    public void setItOutputTable(List<TableOfZpendingPriceOutputTable> itOutputTable) {
        ItOutputTable = itOutputTable;
    }

    public List<TableOfZpreciosPendtes> getItPreciosPendtes() {
        return ItPreciosPendtes;
    }

    public void setItPreciosPendtes(List<TableOfZpreciosPendtes> itPreciosPendtes) {
        ItPreciosPendtes = itPreciosPendtes;
    }

    @Override
    public Object getProperty(int index) {
        switch (index) {
            case 0:
                return this.IClass;
            case 1:
                return this.IConfirArt;
            case 2:
                return this.IEan11;
            case 3:
                return this.IEstatusPos;
            case 4:
                return this.IImprimir;
            case 5:
                return this.ISinCensoDetalle;
            case 6:
                return this.ISinCensoTotal;
            case 7:
                return this.IWerks;
            case 8:
                return this.IZmaterial;
            case 9:
                return this.IZreturndata;
            case 10:
                return this.IZshelfId;
            case 11:
                return this.ItOutputTable;
            case 12:
                return this.ItPreciosPendtes;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index) {
            case 0:
                this.IClass = value.toString();
            case 1:
                this.IConfirArt = value.toString();
            case 2:
                this.IEan11 = value.toString();
            case 3:
                this.IEstatusPos = value.toString();
            case 4:
                this.IImprimir = value.toString();
            case 5:
                this.ISinCensoDetalle = value.toString();
            case 6:
                this.ISinCensoTotal = value.toString();
            case 7:
                this.IWerks = value.toString();
            case 8:
                this.IZmaterial = value.toString();
            case 9:
                this.IZshelfId = value.toString();
            default:
                break;

        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable hashtable, PropertyInfo info) {
        switch (index) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IClass";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IConfirArt";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IEan11";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IEstatusPos";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IImprimir";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ISinCensoDetalle";
                break;
            case 6:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ISinCensoTotal";
                break;
            case 7:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IWerks";
                break;
            case 8:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IZmaterial";
                break;
            case 9:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IZreturndata";
                break;
            case 10:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IZshelfId";
                break;
            case 11:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ItOutputTable";
                break;
            case 12:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ItPreciosPendtes";
                break;
            default:
                break;
        }

    }
}
