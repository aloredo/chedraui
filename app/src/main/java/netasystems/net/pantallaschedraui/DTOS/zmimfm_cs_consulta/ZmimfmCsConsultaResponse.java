package netasystems.net.pantallaschedraui.DTOS.zmimfm_cs_consulta;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZmimfmCsConsultaResponse {

    /**
     * Declaración de variable y lista
     */

    private String EEan;    //char18
    private List<Bapiret2> ERet;    //Bapiret2
    private List<ZsmimCabCons> ESCsHdr; //ZsmimCabCons
    private List<ZttMimDetCons> TtCsDet;   //ZttMimDetCons

    /**
     * Declaración de constructor
     * @param EEan
     * @param ERet
     * @param ESCsHdr
     * @param ttCsDet
     */

    public ZmimfmCsConsultaResponse(String EEan, List<Bapiret2> ERet, List<ZsmimCabCons> ESCsHdr, List<ZttMimDetCons> ttCsDet) {
        this.EEan = EEan;
        this.ERet = ERet;
        this.ESCsHdr = ESCsHdr;
        TtCsDet = ttCsDet;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getEEan() {
        return EEan;
    }

    public void setEEan(String EEan) {
        this.EEan = EEan;
    }

    public List<Bapiret2> getERet() {
        return ERet;
    }

    public void setERet(List<Bapiret2> ERet) {
        this.ERet = ERet;
    }

    public List<ZsmimCabCons> getESCsHdr() {
        return ESCsHdr;
    }

    public void setESCsHdr(List<ZsmimCabCons> ESCsHdr) {
        this.ESCsHdr = ESCsHdr;
    }

    public List<ZttMimDetCons> getTtCsDet() {
        return TtCsDet;
    }

    public void setTtCsDet(List<ZttMimDetCons> ttCsDet) {
        TtCsDet = ttCsDet;
    }
}
