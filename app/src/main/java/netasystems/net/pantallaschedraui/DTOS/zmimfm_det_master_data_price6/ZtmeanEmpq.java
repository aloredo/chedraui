package netasystems.net.pantallaschedraui.DTOS.zmimfm_det_master_data_price6;

public class ZtmeanEmpq {

     private String Mandt;
     private String Matnr;
     private String Meinh;
     private String Lfnum;
     private String Ean11;
     private String Eantp;
     private String Hpean;
     private String Zeanvta;
     private String Zpiezas;

     /**
      * Declaración de constructor
      * @param mandt
      * @param matnr
      * @param meinh
      * @param lfnum
      * @param ean11
      * @param eantp
      * @param hpean
      * @param zeanvta
      * @param zpiezas
      */

     public ZtmeanEmpq(String mandt, String matnr, String meinh, String lfnum, String ean11, String eantp, String hpean, String zeanvta, String zpiezas) {
          Mandt = mandt;
          Matnr = matnr;
          Meinh = meinh;
          Lfnum = lfnum;
          Ean11 = ean11;
          Eantp = eantp;
          Hpean = hpean;
          Zeanvta = zeanvta;
          Zpiezas = zpiezas;
     }

     /**
      *Getters y Setters
      */

     public String getMandt() {
          return Mandt;
     }

     public void setMandt(String mandt) {
          Mandt = mandt;
     }

     public String getMatnr() {
          return Matnr;
     }

     public void setMatnr(String matnr) {
          Matnr = matnr;
     }

     public String getMeinh() {
          return Meinh;
     }

     public void setMeinh(String meinh) {
          Meinh = meinh;
     }

     public String getLfnum() {
          return Lfnum;
     }

     public void setLfnum(String lfnum) {
          Lfnum = lfnum;
     }

     public String getEan11() {
          return Ean11;
     }

     public void setEan11(String ean11) {
          Ean11 = ean11;
     }

     public String getEantp() {
          return Eantp;
     }

     public void setEantp(String eantp) {
          Eantp = eantp;
     }

     public String getHpean() {
          return Hpean;
     }

     public void setHpean(String hpean) {
          Hpean = hpean;
     }

     public String getZeanvta() {
          return Zeanvta;
     }

     public void setZeanvta(String zeanvta) {
          Zeanvta = zeanvta;
     }

     public String getZpiezas() {
          return Zpiezas;
     }

     public void setZpiezas(String zpiezas) {
          Zpiezas = zpiezas;
     }
}
