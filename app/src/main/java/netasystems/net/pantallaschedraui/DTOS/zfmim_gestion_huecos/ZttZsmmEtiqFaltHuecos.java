package netasystems.net.pantallaschedraui.DTOS.zfmim_gestion_huecos;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class ZttZsmmEtiqFaltHuecos {

    /**
     * Declaración de lista
     */

    private List<ZsmmEtiqFaltHuecos> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public ZttZsmmEtiqFaltHuecos(List<ZsmmEtiqFaltHuecos> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<ZsmmEtiqFaltHuecos> getItem() {
        return item;
    }

    public void setItem(List<ZsmmEtiqFaltHuecos> item) {
        this.item = item;
    }
}
