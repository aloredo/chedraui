package netasystems.net.pantallaschedraui.DTOS.zmimfm_cs_consulta;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class ZsmimCabCons {

    /**
     * Declaración de variables y listas
     */

    private String Matnr;   //char18
    private String Maktx;   //char40
    private String CAnaquel;    //numeric8
    private double Desviacion; //decimal16.5
    private double PorcDesv;   //decimal14.2

    /**
     * Declaración de constructor
     * @param matnr
     * @param maktx
     * @param CAnaquel
     * @param desviacion
     * @param porcDesv
     */

    public ZsmimCabCons(String matnr, String maktx, String CAnaquel, double desviacion, double porcDesv) {
        Matnr = matnr;
        Maktx = maktx;
        this.CAnaquel = CAnaquel;
        Desviacion = desviacion;
        PorcDesv = porcDesv;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getMatnr() {
        return Matnr;
    }

    public void setMatnr(String matnr) {
        Matnr = matnr;
    }

    public String getMaktx() {
        return Maktx;
    }

    public void setMaktx(String maktx) {
        Maktx = maktx;
    }

    public String getCAnaquel() {
        return CAnaquel;
    }

    public void setCAnaquel(String CAnaquel) {
        this.CAnaquel = CAnaquel;
    }

    public double getDesviacion() {
        return Desviacion;
    }

    public void setDesviacion(double desviacion) {
        Desviacion = desviacion;
    }

    public double getPorcDesv() {
        return PorcDesv;
    }

    public void setPorcDesv(double porcDesv) {
        PorcDesv = porcDesv;
    }
}
