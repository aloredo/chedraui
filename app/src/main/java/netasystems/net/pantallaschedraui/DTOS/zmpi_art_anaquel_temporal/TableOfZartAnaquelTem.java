package netasystems.net.pantallaschedraui.DTOS.zmpi_art_anaquel_temporal;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

class TableOfZartAnaquelTem {

    /**
     * Declaración de Lista
     */

    private List<ZartAnaquelTem> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public TableOfZartAnaquelTem(List<ZartAnaquelTem> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<ZartAnaquelTem> getItem() {
        return item;
    }

    public void setItem(List<ZartAnaquelTem> item) {
        this.item = item;
    }
}
