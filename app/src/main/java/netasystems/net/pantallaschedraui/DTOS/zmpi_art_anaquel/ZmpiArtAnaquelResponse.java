package netasystems.net.pantallaschedraui.DTOS.zmpi_art_anaquel;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiArtAnaquelResponse {

    /**
     * Declaración de variable y Listas
     */

    private List<TableOfZeean> Ean;    //TableOfZeean
    private List<TableOfZeean> EanError;   //TableOfZeean
    private Integer Guardado;   //int
    private List<TableOfZartAnaquel> ItDetalle;    //TableOfZartAnaquel

    /**
     * Delcaración
     * @param ean
     * @param eanError
     * @param guardado
     * @param itDetalle
     */

    public ZmpiArtAnaquelResponse(List<TableOfZeean> ean, List<TableOfZeean> eanError, Integer guardado, List<TableOfZartAnaquel> itDetalle) {
        Ean = ean;
        EanError = eanError;
        Guardado = guardado;
        ItDetalle = itDetalle;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZeean> getEan() {
        return Ean;
    }

    public void setEan(List<TableOfZeean> ean) {
        Ean = ean;
    }

    public List<TableOfZeean> getEanError() {
        return EanError;
    }

    public void setEanError(List<TableOfZeean> eanError) {
        EanError = eanError;
    }

    public Integer getGuardado() {
        return Guardado;
    }

    public void setGuardado(Integer guardado) {
        Guardado = guardado;
    }

    public List<TableOfZartAnaquel> getItDetalle() {
        return ItDetalle;
    }

    public void setItDetalle(List<TableOfZartAnaquel> itDetalle) {
        ItDetalle = itDetalle;
    }
}
