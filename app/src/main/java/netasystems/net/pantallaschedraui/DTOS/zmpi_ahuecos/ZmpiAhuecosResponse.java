package netasystems.net.pantallaschedraui.DTOS.zmpi_ahuecos;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiAhuecosResponse {

    /**
     * Declaración de variable
     */

    private int Guardado;

    /**
     * Declaración de constructor
     * @param guardado
     */

    public ZmpiAhuecosResponse(int guardado) {
        Guardado = guardado;
    }

    /**
     * Gettesr y Setters
     * @return
     */

    public int getGuardado() {
        return Guardado;
    }

    public void setGuardado(int guardado) {
        Guardado = guardado;
    }
}
