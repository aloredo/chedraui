package netasystems.net.pantallaschedraui.DTOS.zmimfm_cs_consulta;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZmimfmCsConsulta {

    /**
     * Declaración de variables y lista
     */

    private String IEan;    //char18
    private String IMatnr;  //char18
    private String IWerks;  //char4
    private List<ZttMimDetCons> TtCsDet;   //ZttMimDetCons

    /**
     * Declaración de constructor
     * @param IEan
     * @param IMatnr
     * @param IWerks
     * @param ttCsDet
     */

    public ZmimfmCsConsulta(String IEan, String IMatnr, String IWerks, List<ZttMimDetCons> ttCsDet) {
        this.IEan = IEan;
        this.IMatnr = IMatnr;
        this.IWerks = IWerks;
        TtCsDet = ttCsDet;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getIEan() {
        return IEan;
    }

    public void setIEan(String IEan) {
        this.IEan = IEan;
    }

    public String getIMatnr() {
        return IMatnr;
    }

    public void setIMatnr(String IMatnr) {
        this.IMatnr = IMatnr;
    }

    public String getIWerks() {
        return IWerks;
    }

    public void setIWerks(String IWerks) {
        this.IWerks = IWerks;
    }

    public List<ZttMimDetCons> getTtCsDet() {
        return TtCsDet;
    }

    public void setTtCsDet(List<ZttMimDetCons> ttCsDet) {
        TtCsDet = ttCsDet;
    }
}
