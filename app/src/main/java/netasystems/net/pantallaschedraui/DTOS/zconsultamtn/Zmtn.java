package netasystems.net.pantallaschedraui.DTOS.zconsultamtn;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class Zmtn {

    /**
     * Declaración de variables
     */

    private String Zmueble; //char10
    private int Ztramo; //numeric10
    private int Znivel; //numeric10

    /**
     * Declaración de constructor
     * @param zmueble
     * @param ztramo
     * @param znivel
     */

    public Zmtn(String zmueble, int ztramo, int znivel) {
        Zmueble = zmueble;
        Ztramo = ztramo;
        Znivel = znivel;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getZmueble() {
        return Zmueble;
    }

    public void setZmueble(String zmueble) {
        Zmueble = zmueble;
    }

    public int getZtramo() {
        return Ztramo;
    }

    public void setZtramo(int ztramo) {
        Ztramo = ztramo;
    }

    public int getZnivel() {
        return Znivel;
    }

    public void setZnivel(int znivel) {
        Znivel = znivel;
    }
}
