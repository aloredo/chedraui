package netasystems.net.pantallaschedraui.DTOS.zfmpp_merma_carnes;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class TtBapiret2 {

    /**
     * Declaración de lista
     */

    private List<Bapiret2> Bapiret2;

    /**
     * Declaración de constructor
     * @param bapiret2
     */

    public TtBapiret2(List<netasystems.net.pantallaschedraui.DTOS.zfmpp_merma_carnes.Bapiret2> bapiret2) {
        Bapiret2 = bapiret2;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<netasystems.net.pantallaschedraui.DTOS.zfmpp_merma_carnes.Bapiret2> getBapiret2() {
        return Bapiret2;
    }

    public void setBapiret2(List<netasystems.net.pantallaschedraui.DTOS.zfmpp_merma_carnes.Bapiret2> bapiret2) {
        Bapiret2 = bapiret2;
    }
}
