package netasystems.net.pantallaschedraui.DTOS.z_get_pending_price;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class Zreturndata {

    /**
     * Declaración de variables
     */

    private String Ztype;
    private String Zid;
    //Numeric
    private String Znumber;
    private String Zmessage;
    private String Zmaterial;
    private String ZmaterialDesc;
    private String Zean;
    //Decimal
    private double Zprecio;
    private String ZimprimirEtiqueta;
    private String Zdate;
    private String ZdeSdprIpadr1;
    private String ZpuertoCom1;
    private String ZdeSdprIpadr2;
    private String ZpuertoCom2;
    private String ZCodBarraCom;
    private String ZactualizarPos;
    private String Ziva;
    private String ZclaveSkuKilo;
    private String Zieps;
    private String ZimptoEsp;
    private String Zlifnr;
    private String Zdepto;
    private String Zsubdepto;
    private String Zclass;
    private String Zsubclass;
    private String ZbrandId;
    private String ZtipoProm;
    private String ZplantillaPromo;
    //Decimal
    private double ZdifPrecio;
     //Decimal
    private double Zcaducidad;
    private String ZtipoEtiqueta;
    private String Zreceta;

    /**
     * Declaración de constructor
     * @param ztype
     * @param zid
     * @param znumber
     * @param zmessage
     * @param zmaterial
     * @param zmaterialDesc
     * @param zean
     * @param zprecio
     * @param zimprimirEtiqueta
     * @param zdate
     * @param zdeSdprIpadr1
     * @param zpuertoCom1
     * @param zdeSdprIpadr2
     * @param zpuertoCom2
     * @param ZCodBarraCom
     * @param zactualizarPos
     * @param ziva
     * @param zclaveSkuKilo
     * @param zieps
     * @param zimptoEsp
     * @param zlifnr
     * @param zdepto
     * @param zsubdepto
     * @param zclass
     * @param zsubclass
     * @param zbrandId
     * @param ztipoProm
     * @param zplantillaPromo
     * @param zdifPrecio
     * @param zcaducidad
     * @param ztipoEtiqueta
     * @param zreceta
     */

    public Zreturndata(String ztype, String zid, String znumber, String zmessage, String zmaterial, String zmaterialDesc, String zean, double zprecio, String zimprimirEtiqueta, String zdate, String zdeSdprIpadr1, String zpuertoCom1, String zdeSdprIpadr2, String zpuertoCom2, String ZCodBarraCom, String zactualizarPos, String ziva, String zclaveSkuKilo, String zieps, String zimptoEsp, String zlifnr, String zdepto, String zsubdepto, String zclass, String zsubclass, String zbrandId, String ztipoProm, String zplantillaPromo, double zdifPrecio, double zcaducidad, String ztipoEtiqueta, String zreceta) {
        Ztype = ztype;
        Zid = zid;
        Znumber = znumber;
        Zmessage = zmessage;
        Zmaterial = zmaterial;
        ZmaterialDesc = zmaterialDesc;
        Zean = zean;
        Zprecio = zprecio;
        ZimprimirEtiqueta = zimprimirEtiqueta;
        Zdate = zdate;
        ZdeSdprIpadr1 = zdeSdprIpadr1;
        ZpuertoCom1 = zpuertoCom1;
        ZdeSdprIpadr2 = zdeSdprIpadr2;
        ZpuertoCom2 = zpuertoCom2;
        this.ZCodBarraCom = ZCodBarraCom;
        ZactualizarPos = zactualizarPos;
        Ziva = ziva;
        ZclaveSkuKilo = zclaveSkuKilo;
        Zieps = zieps;
        ZimptoEsp = zimptoEsp;
        Zlifnr = zlifnr;
        Zdepto = zdepto;
        Zsubdepto = zsubdepto;
        Zclass = zclass;
        Zsubclass = zsubclass;
        ZbrandId = zbrandId;
        ZtipoProm = ztipoProm;
        ZplantillaPromo = zplantillaPromo;
        ZdifPrecio = zdifPrecio;
        Zcaducidad = zcaducidad;
        ZtipoEtiqueta = ztipoEtiqueta;
        Zreceta = zreceta;
    }

    public Zreturndata() {

    }

    /**
     * Getters y Setters
     * @return
     */

    public String getZtype() {
        return Ztype;
    }

    public void setZtype(String ztype) {
        Ztype = ztype;
    }

    public String getZid() {
        return Zid;
    }

    public void setZid(String zid) {
        Zid = zid;
    }

    public String getZnumber() {
        return Znumber;
    }

    public void setZnumber(String znumber) {
        Znumber = znumber;
    }

    public String getZmessage() {
        return Zmessage;
    }

    public void setZmessage(String zmessage) {
        Zmessage = zmessage;
    }

    public String getZmaterial() {
        return Zmaterial;
    }

    public void setZmaterial(String zmaterial) {
        Zmaterial = zmaterial;
    }

    public String getZmaterialDesc() {
        return ZmaterialDesc;
    }

    public void setZmaterialDesc(String zmaterialDesc) {
        ZmaterialDesc = zmaterialDesc;
    }

    public String getZean() {
        return Zean;
    }

    public void setZean(String zean) {
        Zean = zean;
    }

    public double getZprecio() {
        return Zprecio;
    }

    public void setZprecio(double zprecio) {
        Zprecio = zprecio;
    }

    public String getZimprimirEtiqueta() {
        return ZimprimirEtiqueta;
    }

    public void setZimprimirEtiqueta(String zimprimirEtiqueta) {
        ZimprimirEtiqueta = zimprimirEtiqueta;
    }

    public String getZdate() {
        return Zdate;
    }

    public void setZdate(String zdate) {
        Zdate = zdate;
    }

    public String getZdeSdprIpadr1() {
        return ZdeSdprIpadr1;
    }

    public void setZdeSdprIpadr1(String zdeSdprIpadr1) {
        ZdeSdprIpadr1 = zdeSdprIpadr1;
    }

    public String getZpuertoCom1() {
        return ZpuertoCom1;
    }

    public void setZpuertoCom1(String zpuertoCom1) {
        ZpuertoCom1 = zpuertoCom1;
    }

    public String getZdeSdprIpadr2() {
        return ZdeSdprIpadr2;
    }

    public void setZdeSdprIpadr2(String zdeSdprIpadr2) {
        ZdeSdprIpadr2 = zdeSdprIpadr2;
    }

    public String getZpuertoCom2() {
        return ZpuertoCom2;
    }

    public void setZpuertoCom2(String zpuertoCom2) {
        ZpuertoCom2 = zpuertoCom2;
    }

    public String getZCodBarraCom() {
        return ZCodBarraCom;
    }

    public void setZCodBarraCom(String ZCodBarraCom) {
        this.ZCodBarraCom = ZCodBarraCom;
    }

    public String getZactualizarPos() {
        return ZactualizarPos;
    }

    public void setZactualizarPos(String zactualizarPos) {
        ZactualizarPos = zactualizarPos;
    }

    public String getZiva() {
        return Ziva;
    }

    public void setZiva(String ziva) {
        Ziva = ziva;
    }

    public String getZclaveSkuKilo() {
        return ZclaveSkuKilo;
    }

    public void setZclaveSkuKilo(String zclaveSkuKilo) {
        ZclaveSkuKilo = zclaveSkuKilo;
    }

    public String getZieps() {
        return Zieps;
    }

    public void setZieps(String zieps) {
        Zieps = zieps;
    }

    public String getZimptoEsp() {
        return ZimptoEsp;
    }

    public void setZimptoEsp(String zimptoEsp) {
        ZimptoEsp = zimptoEsp;
    }

    public String getZlifnr() {
        return Zlifnr;
    }

    public void setZlifnr(String zlifnr) {
        Zlifnr = zlifnr;
    }

    public String getZdepto() {
        return Zdepto;
    }

    public void setZdepto(String zdepto) {
        Zdepto = zdepto;
    }

    public String getZsubdepto() {
        return Zsubdepto;
    }

    public void setZsubdepto(String zsubdepto) {
        Zsubdepto = zsubdepto;
    }

    public String getZclass() {
        return Zclass;
    }

    public void setZclass(String zclass) {
        Zclass = zclass;
    }

    public String getZsubclass() {
        return Zsubclass;
    }

    public void setZsubclass(String zsubclass) {
        Zsubclass = zsubclass;
    }

    public String getZbrandId() {
        return ZbrandId;
    }

    public void setZbrandId(String zbrandId) {
        ZbrandId = zbrandId;
    }

    public String getZtipoProm() {
        return ZtipoProm;
    }

    public void setZtipoProm(String ztipoProm) {
        ZtipoProm = ztipoProm;
    }

    public String getZplantillaPromo() {
        return ZplantillaPromo;
    }

    public void setZplantillaPromo(String zplantillaPromo) {
        ZplantillaPromo = zplantillaPromo;
    }

    public double getZdifPrecio() {
        return ZdifPrecio;
    }

    public void setZdifPrecio(double zdifPrecio) {
        ZdifPrecio = zdifPrecio;
    }

    public double getZcaducidad() {
        return Zcaducidad;
    }

    public void setZcaducidad(double zcaducidad) {
        Zcaducidad = zcaducidad;
    }

    public String getZtipoEtiqueta() {
        return ZtipoEtiqueta;
    }

    public void setZtipoEtiqueta(String ztipoEtiqueta) {
        ZtipoEtiqueta = ztipoEtiqueta;
    }

    public String getZreceta() {
        return Zreceta;
    }

    public void setZreceta(String zreceta) {
        Zreceta = zreceta;
    }
}
