package netasystems.net.pantallaschedraui.DTOS.zmimfm_det_master_data_price6;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

class TableOfZtmeanEmpq {

    /**
     * Declaración de lista
     */

    private List<ZtmeanEmpq> item;

    /**
     * Declaración de constructor
     */

    public TableOfZtmeanEmpq(List<ZtmeanEmpq> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     *
     */

    public List<ZtmeanEmpq> getItem() {
        return item;
    }

    public void setItem(List<ZtmeanEmpq> item) {
        this.item = item;
    }
}
