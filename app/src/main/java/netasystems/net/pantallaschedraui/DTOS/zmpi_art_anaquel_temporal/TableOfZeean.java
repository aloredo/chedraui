package netasystems.net.pantallaschedraui.DTOS.zmpi_art_anaquel_temporal;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class TableOfZeean {
    /**
     * Declaración de Lista
     */

    private List<Zeean> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public TableOfZeean(List<Zeean> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<Zeean> getItem() {
        return item;
    }

    public void setItem(List<Zeean> item) {
        this.item = item;
    }
}
