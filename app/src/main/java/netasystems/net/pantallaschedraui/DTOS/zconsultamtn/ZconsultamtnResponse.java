package netasystems.net.pantallaschedraui.DTOS.zconsultamtn;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZconsultamtnResponse {

    /**
     * Declaración de lista
     */

    private List<TableOfZmtn> TMuebles;    //TableOfZmtn

    /**
     * Declaración de constructor
     * @param TMuebles
     */

    public ZconsultamtnResponse(List<TableOfZmtn> TMuebles) {
        this.TMuebles = TMuebles;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZmtn> getTMuebles() {
        return TMuebles;
    }

    public void setTMuebles(List<TableOfZmtn> TMuebles) {
        this.TMuebles = TMuebles;
    }
}
