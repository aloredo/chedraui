package netasystems.net.pantallaschedraui.DTOS.zconsultamtn;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class Zconsultamtn {

    /**
     * Declaración de variables y lista
     */

    private String IEan;    //char18
    private String IMatnr;  //char18
    private String IWerks;  //char4
    private List<TableOfZmtn> TMuebles;    //TableOfZmtn

    /**
     * Declaración de constructor
     * @param IEan
     * @param IMatnr
     * @param IWerks
     * @param TMuebles
     */

    public Zconsultamtn(String IEan, String IMatnr, String IWerks, List<TableOfZmtn> TMuebles) {
        this.IEan = IEan;
        this.IMatnr = IMatnr;
        this.IWerks = IWerks;
        this.TMuebles = TMuebles;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getIEan() {
        return IEan;
    }

    public void setIEan(String IEan) {
        this.IEan = IEan;
    }

    public String getIMatnr() {
        return IMatnr;
    }

    public void setIMatnr(String IMatnr) {
        this.IMatnr = IMatnr;
    }

    public String getIWerks() {
        return IWerks;
    }

    public void setIWerks(String IWerks) {
        this.IWerks = IWerks;
    }

    public List<TableOfZmtn> getTMuebles() {
        return TMuebles;
    }

    public void setTMuebles(List<TableOfZmtn> TMuebles) {
        this.TMuebles = TMuebles;
    }

}
