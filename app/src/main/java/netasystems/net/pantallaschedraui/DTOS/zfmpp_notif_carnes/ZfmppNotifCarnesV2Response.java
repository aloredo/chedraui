package netasystems.net.pantallaschedraui.DTOS.zfmpp_notif_carnes;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZfmppNotifCarnesV2Response {

    /**
     * Declaración de listas
     */

    private List<TableOfZsppEtiquetaCarnes> Etiqueta;
    private List<ZttBapiret2> Return;

    /**
     * Declaración de constructor
     * @param etiqueta
     * @param aReturn
     */

    public ZfmppNotifCarnesV2Response(List<TableOfZsppEtiquetaCarnes> etiqueta, List<ZttBapiret2> aReturn) {
        Etiqueta = etiqueta;
        Return = aReturn;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZsppEtiquetaCarnes> getEtiqueta() {
        return Etiqueta;
    }

    public void setEtiqueta(List<TableOfZsppEtiquetaCarnes> etiqueta) {
        Etiqueta = etiqueta;
    }

    public List<ZttBapiret2> getReturn() {
        return Return;
    }

    public void setReturn(List<ZttBapiret2> aReturn) {
        Return = aReturn;
    }
}
