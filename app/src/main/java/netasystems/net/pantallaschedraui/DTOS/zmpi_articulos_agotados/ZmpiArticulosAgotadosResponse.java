package netasystems.net.pantallaschedraui.DTOS.zmpi_articulos_agotados;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiArticulosAgotadosResponse {

    /**
     * Declaración variable y lista
     */

    private String EMsg;
    private List<TableOfZsmmEtiqFaltHuecos> TiArticulos;

    /**
     * Declaración de constructor
     * @param EMsg
     * @param tiArticulos
     */

    public ZmpiArticulosAgotadosResponse(String EMsg, List<TableOfZsmmEtiqFaltHuecos> tiArticulos) {
        this.EMsg = EMsg;
        TiArticulos = tiArticulos;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getEMsg() {
        return EMsg;
    }

    public void setEMsg(String EMsg) {
        this.EMsg = EMsg;
    }

    public List<TableOfZsmmEtiqFaltHuecos> getTiArticulos() {
        return TiArticulos;
    }

    public void setTiArticulos(List<TableOfZsmmEtiqFaltHuecos> tiArticulos) {
        TiArticulos = tiArticulos;
    }
}
