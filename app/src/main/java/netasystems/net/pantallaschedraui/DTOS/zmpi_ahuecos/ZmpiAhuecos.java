package netasystems.net.pantallaschedraui.DTOS.zmpi_ahuecos;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiAhuecos {

    /**
     * Declaración de variables
     */

    private String Articulo;
    private String Guardar;
    private String Mueble;
    private String Tienda;

    /**
     * Declaración de constructor
     * @param articulo
     * @param guardar
     * @param mueble
     * @param tienda
     */

    public ZmpiAhuecos(String articulo, String guardar, String mueble, String tienda) {
        Articulo = articulo;
        Guardar = guardar;
        Mueble = mueble;
        Tienda = tienda;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getArticulo() {
        return Articulo;
    }

    public void setArticulo(String articulo) {
        Articulo = articulo;
    }

    public String getGuardar() {
        return Guardar;
    }

    public void setGuardar(String guardar) {
        Guardar = guardar;
    }

    public String getMueble() {
        return Mueble;
    }

    public void setMueble(String mueble) {
        Mueble = mueble;
    }

    public String getTienda() {
        return Tienda;
    }

    public void setTienda(String tienda) {
        Tienda = tienda;
    }
}
