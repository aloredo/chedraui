package netasystems.net.pantallaschedraui.DTOS.zcheck_shelf;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZcheckShelf {

    /**
     * Declaración de variables y lista
     */

    private String ILgort;
    private String IWerks;
    private String IZshelfId;
    private List<ZttMimCsmue> TtCsmue;

    /**
     * Declaración de constructor
     * @param ILgort
     * @param IWerks
     * @param IZshelfId
     * @param ttCsmue
     */

    public ZcheckShelf(String ILgort, String IWerks, String IZshelfId, List<ZttMimCsmue> ttCsmue) {
        this.ILgort = ILgort;
        this.IWerks = IWerks;
        this.IZshelfId = IZshelfId;
        TtCsmue = ttCsmue;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getILgort() {
        return ILgort;
    }

    public void setILgort(String ILgort) {
        this.ILgort = ILgort;
    }

    public String getIWerks() {
        return IWerks;
    }

    public void setIWerks(String IWerks) {
        this.IWerks = IWerks;
    }

    public String getIZshelfId() {
        return IZshelfId;
    }

    public void setIZshelfId(String IZshelfId) {
        this.IZshelfId = IZshelfId;
    }

    public List<ZttMimCsmue> getTtCsmue() {
        return TtCsmue;
    }

    public void setTtCsmue(List<ZttMimCsmue> ttCsmue) {
        TtCsmue = ttCsmue;
    }
}
