package netasystems.net.pantallaschedraui.DTOS.zfmim_gestion_huecos;

import java.util.List;

public class ZfmimGestionHuecosResponse {

    private String EMsg;
    private String ESubrc;
    private List<ZttZsmmEtiqFaltHuecos> ExImpEtiqFalt;
    private List<TableOfZsppArticulo> TScanArticulos;
}
