package netasystems.net.pantallaschedraui.DTOS.zconsultamtn;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class TableOfZmtn {

    /**
     * Declaración de variables
     */

    private List<Zmtn> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public TableOfZmtn(List<Zmtn> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<Zmtn> getItem() {
        return item;
    }

    public void setItem(List<Zmtn> item) {
        this.item = item;
    }
}
