package netasystems.net.pantallaschedraui.DTOS.zfmpp_merma_carnes;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class TableOfZsppEtiquetaCarnes {

    /**
     * Declaración de lista
     */

    private List<ZsppEtiquetaCarnes> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public TableOfZsppEtiquetaCarnes(List<ZsppEtiquetaCarnes> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<ZsppEtiquetaCarnes> getItem() {
        return item;
    }

    public void setItem(List<ZsppEtiquetaCarnes> item) {
        this.item = item;
    }
}
