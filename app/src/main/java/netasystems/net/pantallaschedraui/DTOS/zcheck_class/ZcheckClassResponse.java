package netasystems.net.pantallaschedraui.DTOS.zcheck_class;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZcheckClassResponse {

    /**
     * Declaración de variable y lista
     */

    private List<ZtmimClases> Clase;
    private String ERet;

    /**
     * Declaración de constructor
     * @param clase
     * @param ERet
     */

    public ZcheckClassResponse(List<ZtmimClases> clase, String ERet) {
        Clase = clase;
        this.ERet = ERet;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<ZtmimClases> getClase() {
        return Clase;
    }

    public void setClase(List<ZtmimClases> clase) {
        Clase = clase;
    }

    public String getERet() {
        return ERet;
    }

    public void setERet(String ERet) {
        this.ERet = ERet;
    }


}
