package netasystems.net.pantallaschedraui.DTOS.zmimifm_det_master_data_price7;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmimfmDetMasterDataPrice7 {

    /**
     * Declaración de listas y variables
     */

    private String IEan11;
    private String IWerks;
    private List<TableOfZtmeanEmpq> TMean;
    private List<TableOfZmtn> TMuebles;

    /**
     * Declaración de constructor
     * @param IEan11
     * @param IWerks
     * @param TMean
     * @param TMuebles
     */

    public ZmimfmDetMasterDataPrice7(String IEan11, String IWerks, List<TableOfZtmeanEmpq> TMean, List<TableOfZmtn> TMuebles) {
        this.IEan11 = IEan11;
        this.IWerks = IWerks;
        this.TMean = TMean;
        this.TMuebles = TMuebles;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getIEan11() {
        return IEan11;
    }

    public void setIEan11(String IEan11) {
        this.IEan11 = IEan11;
    }

    public String getIWerks() {
        return IWerks;
    }

    public void setIWerks(String IWerks) {
        this.IWerks = IWerks;
    }

    public List<TableOfZtmeanEmpq> getTMean() {
        return TMean;
    }

    public void setTMean(List<TableOfZtmeanEmpq> TMean) {
        this.TMean = TMean;
    }

    public List<TableOfZmtn> getTMuebles() {
        return TMuebles;
    }

    public void setTMuebles(List<TableOfZmtn> TMuebles) {
        this.TMuebles = TMuebles;
    }
}
