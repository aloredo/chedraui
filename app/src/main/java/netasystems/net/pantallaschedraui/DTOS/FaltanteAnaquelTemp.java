package netasystems.net.pantallaschedraui.DTOS;

public class FaltanteAnaquelTemp {
/*
    /// <summary>
    /// Proxy general para los métodos de la función zmpi_art_anaquel_temporal.
    /// </summary>
    public static FaltanteAnaquelTemporal.ZMPI_ART_ANAQUEL_TEMPORAL MetodosFaltanteAnaquelTemporal;

    /// <summary>
    /// Inicializa la clase <see cref="FaltanteAnaquelTemp"/>.
    /// </summary>
    static FaltanteAnaquelTemp()
    {
        //Construye proxy único para el uso de la función zmpi_art_anaquel_temporal.
        FaltanteAnaquelTemp.MetodosFaltanteAnaquelTemporal = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZMPI_ART_ANAQUEL_TEMPORAL();
        System.Net.ServicePoint metodoFaltanteAnaquelTemporalServicePoint = System.Net.ServicePointManager.FindServicePoint(new Uri(FaltanteAnaquelTemp.MetodosFaltanteAnaquelTemporal.Url));
        if (metodoFaltanteAnaquelTemporalServicePoint != null)
            metodoFaltanteAnaquelTemporalServicePoint.Expect100Continue = false;
    }

    public static String ObtenerMueble()
    {
        try
        {
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse import = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse();
            FaltanteAnaquelTemporal.ZMPI_ART_ANAQUEL_TEMPORAL servicio = FaltanteAnaquelTemp.MetodosFaltanteAnaquelTemporal;
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal export = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal();

            servicio.Credentials = new System.Net.NetworkCredential(PerfilAutenticacion.Usuario, PerfilAutenticacion.Pwd);

            FaltanteAnaquelTemporal.Zeean[] arregloEan = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.Zeean[1];
            FaltanteAnaquelTemporal.ZartAnaquelTem[] arregloRecupera = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZartAnaquelTem[1];

            export.Tienda = PerfilAutenticacion.Tienda;
            export.Usuario = PerfilAutenticacion.Usuario.ToUpper();
            export.GRecupera = "X";
            export.Ean = arregloEan;
            export.ItRecupera = arregloRecupera;

                import = servicio.ZmpiArtAnaquelTemporal(export);

            String mueble;

            if (import.ItRecupera.Length > 0)
            mueble = import.ItRecupera[0].Zmueble;
                else
            mueble = "";

            return mueble;
        }
        catch (Exception ex)
        {
            ErrorLogger.RegistrarError("ZMPI_ART_ANAQUEL_TEMPORAL,Obtener mueble", PerfilAutenticacion.Usuario, PerfilAutenticacion.Tienda, ex.ToString());
            return null;
        }
    }

    public static void BorrarMueble(string mueble)
    {
        try
        {
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse import = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse();
            FaltanteAnaquelTemporal.ZMPI_ART_ANAQUEL_TEMPORAL servicio = FaltanteAnaquelTemp.MetodosFaltanteAnaquelTemporal;
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal export = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal();

            servicio.Credentials = new System.Net.NetworkCredential(PerfilAutenticacion.Usuario, PerfilAutenticacion.Pwd);

            FaltanteAnaquelTemporal.Zeean[] arregloEan = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.Zeean[1];
            FaltanteAnaquelTemporal.ZartAnaquelTem[] arregloRecupera = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZartAnaquelTem[1];

            export.Tienda = PerfilAutenticacion.Tienda;
            export.Usuario = PerfilAutenticacion.Usuario.ToUpper();
            export.GBorrar = "X";
            export.Mueble = mueble;
            export.Ean = arregloEan;
            export.ItRecupera = arregloRecupera;

                import = servicio.ZmpiArtAnaquelTemporal(export);
        }
        catch (Exception ex)
        {
            ErrorLogger.RegistrarError("ZMPI_ART_ANAQUEL_TEMPORAL,Borrar mueble", PerfilAutenticacion.Usuario, PerfilAutenticacion.Tienda, ex.ToString());
        }
    }

    public static string[] ObtenerResumen()
    {
        try
        {
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse import = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse();
            FaltanteAnaquelTemporal.ZMPI_ART_ANAQUEL_TEMPORAL servicio = FaltanteAnaquelTemp.MetodosFaltanteAnaquelTemporal;
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal export = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal();

            servicio.Credentials = new System.Net.NetworkCredential(PerfilAutenticacion.Usuario, PerfilAutenticacion.Pwd);

            FaltanteAnaquelTemporal.Zeean[] arregloEan = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.Zeean[1];
            FaltanteAnaquelTemporal.ZartAnaquelTem[] arregloRecupera = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZartAnaquelTem[1];

            export.Tienda = PerfilAutenticacion.Tienda;
            export.Usuario = PerfilAutenticacion.Usuario.ToUpper();
            export.GRecupera = "X";
            export.Mueble = PerfilAutenticacion.MuebleFaltanteAnaquel;
            export.Ean = arregloEan;
            export.ItRecupera = arregloRecupera;

                import = servicio.ZmpiArtAnaquelTemporal(export);
            string[] miArreglo = { };

            if (import.ItRecupera != null && import.ItRecupera.Length > 0)
            {
                miArreglo = new string[import.ItRecupera.Length];

                for (int i = 0; i < import.ItRecupera.Length; i++)
                {
                    miArreglo[i] = import.ItRecupera[i].Zupc;
                }
            }

            return miArreglo;
        }
        catch (Exception ex)
        {
            ErrorLogger.RegistrarError("ZMPI_ART_ANAQUEL_TEMPORAL,Obtener resumen", PerfilAutenticacion.Usuario, PerfilAutenticacion.Tienda, ex.ToString());
            return null;
        }
    }

    public static FaltanteAnaquelTemporal.ZartAnaquelTem[] ObtenerResumen2()
    {
        try
        {
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse import = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse();
            FaltanteAnaquelTemporal.ZMPI_ART_ANAQUEL_TEMPORAL servicio = FaltanteAnaquelTemp.MetodosFaltanteAnaquelTemporal;
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal export = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal();

            servicio.Credentials = new System.Net.NetworkCredential(PerfilAutenticacion.Usuario, PerfilAutenticacion.Pwd);

            FaltanteAnaquelTemporal.Zeean[] arregloEan = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.Zeean[1];
            FaltanteAnaquelTemporal.ZartAnaquelTem[] arregloRecupera = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZartAnaquelTem[1];

            export.Tienda = PerfilAutenticacion.Tienda;
            export.Usuario = PerfilAutenticacion.Usuario.ToUpper();
            export.GRecupera = "X";
            export.Mueble = PerfilAutenticacion.MuebleFaltanteAnaquel;
            export.Ean = arregloEan;
            export.ItRecupera = arregloRecupera;

                import = servicio.ZmpiArtAnaquelTemporal(export);

            return import.ItRecupera;
        }
        catch (Exception ex)
        {
            ErrorLogger.RegistrarError("ZMPI_ART_ANAQUEL_TEMPORAL,Obtener resumen2", PerfilAutenticacion.Usuario, PerfilAutenticacion.Tienda, ex.ToString());
            return null;
        }
    }

    public static FaltanteAnaquelTemporal.ZartAnaquelTem[] ObtenerResumen2(string ean)
    {
        try
        {
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse import = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse();
            FaltanteAnaquelTemporal.ZMPI_ART_ANAQUEL_TEMPORAL servicio = FaltanteAnaquelTemp.MetodosFaltanteAnaquelTemporal;
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal export = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal();

            servicio.Credentials = new System.Net.NetworkCredential(PerfilAutenticacion.Usuario, PerfilAutenticacion.Pwd);

            FaltanteAnaquelTemporal.Zeean[] arregloEan = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.Zeean[1];
            FaltanteAnaquelTemporal.ZartAnaquelTem[] arregloRecupera = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZartAnaquelTem[1];

            export.Tienda = PerfilAutenticacion.Tienda;
            export.Usuario = PerfilAutenticacion.Usuario.ToUpper();
            export.GRecupera = "X";
            export.Mueble = PerfilAutenticacion.MuebleFaltanteAnaquel;
            export.GEan = ean;
            export.Ean = arregloEan;
            export.ItRecupera = arregloRecupera;

                import = servicio.ZmpiArtAnaquelTemporal(export);

            return import.ItRecupera;
        }
        catch (Exception ex)
        {
            ErrorLogger.RegistrarError("ZMPI_ART_ANAQUEL_TEMPORAL,Obtener resumen2", PerfilAutenticacion.Usuario, PerfilAutenticacion.Tienda, ex.ToString());
            return null;
        }
    }

    public static FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse ObtenerDetalleArticulo(string mueble, string ean)
    {
        try
        {
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse import = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse();
            FaltanteAnaquelTemporal.ZMPI_ART_ANAQUEL_TEMPORAL servicio = FaltanteAnaquelTemp.MetodosFaltanteAnaquelTemporal;
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal export = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal();

            servicio.Credentials = new System.Net.NetworkCredential(PerfilAutenticacion.Usuario, PerfilAutenticacion.Pwd);

            FaltanteAnaquelTemporal.Zeean[] arregloEan = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.Zeean[1];
            FaltanteAnaquelTemporal.ZartAnaquelTem[] arregloRecupera = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZartAnaquelTem[1];

            export.Tienda = PerfilAutenticacion.Tienda;
            export.Usuario = PerfilAutenticacion.Usuario.ToUpper();
            export.GRecupera = "X";
            export.Mueble = mueble;
            export.GEan = ean;
            export.Ean = arregloEan;
            export.ItRecupera = arregloRecupera;

                import = servicio.ZmpiArtAnaquelTemporal(export);

            return import;
        }
        catch (Exception ex)
        {
            ErrorLogger.RegistrarError("ZMPI_ART_ANAQUEL_TEMPORAL,Obtener detalle artículo", PerfilAutenticacion.Usuario, PerfilAutenticacion.Tienda, ex.ToString());
            return null;
        }
    }

    public static void GuardarTemporal(string[] eans)
    {
        try
        {
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse import = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse();
            FaltanteAnaquelTemporal.ZMPI_ART_ANAQUEL_TEMPORAL servicio = FaltanteAnaquelTemp.MetodosFaltanteAnaquelTemporal;
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal export = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal();

            servicio.Credentials = new System.Net.NetworkCredential(PerfilAutenticacion.Usuario, PerfilAutenticacion.Pwd);

            FaltanteAnaquelTemporal.Zeean[] arregloEan = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.Zeean[20];
            FaltanteAnaquelTemporal.ZartAnaquelTem[] arregloRecupera = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZartAnaquelTem[1];

            for (int i = 0; i < eans.Length; i++)
            {
                arregloEan[i] = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.Zeean();
                arregloEan[i].Ean = eans[i];
            }

            export.Tienda = PerfilAutenticacion.Tienda;
            export.Usuario = PerfilAutenticacion.Usuario.ToUpper();
            export.Mueble = PerfilAutenticacion.MuebleFaltanteAnaquel;
            export.Ean = arregloEan;
            export.ItRecupera = arregloRecupera;

                import = servicio.ZmpiArtAnaquelTemporal(export);
        }
        catch (Exception ex)
        {
            ErrorLogger.RegistrarError("ZMPI_ART_ANAQUEL_TEMPORAL,Guardar información tabla temporal", PerfilAutenticacion.Usuario, PerfilAutenticacion.Tienda, ex.ToString());
        }
    }

    public static void BorrarTemporal()
    {
        try
        {
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse import = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporalResponse();
            FaltanteAnaquelTemporal.ZMPI_ART_ANAQUEL_TEMPORAL servicio = FaltanteAnaquelTemp.MetodosFaltanteAnaquelTemporal;
            FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal export = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZmpiArtAnaquelTemporal();

            servicio.Credentials = new System.Net.NetworkCredential(PerfilAutenticacion.Usuario, PerfilAutenticacion.Pwd);

            FaltanteAnaquelTemporal.Zeean[] arregloEan = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.Zeean[1];
            FaltanteAnaquelTemporal.ZartAnaquelTem[] arregloRecupera = new Com.Chedraui.AuditoriaPrecio.FaltanteAnaquelTemporal.ZartAnaquelTem[1];

            export.Tienda = PerfilAutenticacion.Tienda;
            export.Usuario = PerfilAutenticacion.Usuario.ToUpper();
            export.Mueble = PerfilAutenticacion.MuebleFaltanteAnaquel;
            export.GBorrar = "X";
            export.Ean = arregloEan;
            export.ItRecupera = arregloRecupera;

                import = servicio.ZmpiArtAnaquelTemporal(export);
        }
        catch (Exception ex)
        {
            ErrorLogger.RegistrarError("ZMPI_ART_ANAQUEL_TEMPORAL,Borrar información tabla temporal", PerfilAutenticacion.Usuario, PerfilAutenticacion.Tienda, ex.ToString());
        }
    }


    */
}

