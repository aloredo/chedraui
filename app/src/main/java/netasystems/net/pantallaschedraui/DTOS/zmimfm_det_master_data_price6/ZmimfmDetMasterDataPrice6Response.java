package netasystems.net.pantallaschedraui.DTOS.zmimfm_det_master_data_price6;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmimfmDetMasterDataPrice6Response {

    /**
     * Declaración de listas y variables
     */

    private List<TableOfZtmeanEmpq> TMean;
    private List<TableOfZmtn> TMuebles;
    private String Zcodsat;
    private String Zdecil;
    private String Zdescripcionart;
    private String Zmeinh;
    private String Zmmsta;
    private List<netasystems.net.pantallaschedraui.DTOS.zmimfm_det_master_data_price6.Zreturndata> Zreturndata;
    private String Zumsat;

    /**
     * Declaración de constructor
     * @param TMean
     * @param TMuebles
     * @param zcodsat
     * @param zdecil
     * @param zdescripcionart
     * @param zmeinh
     * @param zmmsta
     * @param zreturndata
     * @param zumsat
     */

    public ZmimfmDetMasterDataPrice6Response(List<TableOfZtmeanEmpq> TMean, List<TableOfZmtn> TMuebles, String zcodsat, String zdecil, String zdescripcionart, String zmeinh, String zmmsta, List<netasystems.net.pantallaschedraui.DTOS.zmimfm_det_master_data_price6.Zreturndata> zreturndata, String zumsat) {
        this.TMean = TMean;
        this.TMuebles = TMuebles;
        Zcodsat = zcodsat;
        Zdecil = zdecil;
        Zdescripcionart = zdescripcionart;
        Zmeinh = zmeinh;
        Zmmsta = zmmsta;
        Zreturndata = zreturndata;
        Zumsat = zumsat;
    }

    /**
     * Getters y Setters
     */

    public List<TableOfZtmeanEmpq> getTMean() {
        return TMean;
    }

    public void setTMean(List<TableOfZtmeanEmpq> TMean) {
        this.TMean = TMean;
    }

    public List<TableOfZmtn> getTMuebles() {
        return TMuebles;
    }

    public void setTMuebles(List<TableOfZmtn> TMuebles) {
        this.TMuebles = TMuebles;
    }

    public String getZcodsat() {
        return Zcodsat;
    }

    public void setZcodsat(String zcodsat) {
        Zcodsat = zcodsat;
    }

    public String getZdecil() {
        return Zdecil;
    }

    public void setZdecil(String zdecil) {
        Zdecil = zdecil;
    }

    public String getZdescripcionart() {
        return Zdescripcionart;
    }

    public void setZdescripcionart(String zdescripcionart) {
        Zdescripcionart = zdescripcionart;
    }

    public String getZmeinh() {
        return Zmeinh;
    }

    public void setZmeinh(String zmeinh) {
        Zmeinh = zmeinh;
    }

    public String getZmmsta() {
        return Zmmsta;
    }

    public void setZmmsta(String zmmsta) {
        Zmmsta = zmmsta;
    }

    public List<netasystems.net.pantallaschedraui.DTOS.zmimfm_det_master_data_price6.Zreturndata> getZreturndata() {
        return Zreturndata;
    }

    public void setZreturndata(List<netasystems.net.pantallaschedraui.DTOS.zmimfm_det_master_data_price6.Zreturndata> zreturndata) {
        Zreturndata = zreturndata;
    }

    public String getZumsat() {
        return Zumsat;
    }

    public void setZumsat(String zumsat) {
        Zumsat = zumsat;
    }


}
