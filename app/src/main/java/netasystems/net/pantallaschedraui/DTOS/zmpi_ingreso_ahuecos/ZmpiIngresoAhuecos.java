package netasystems.net.pantallaschedraui.DTOS.zmpi_ingreso_ahuecos;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiIngresoAhuecos {

    /**
     * Declaración lista
     */

    private String CheckProveedor;  //char1
    private String Empleado;    //char12
    private String Mueble;  //char10
    private String Proveedor;   //char10
    private List<TableOfBapiret2> Return;  //TableOfBapiret2

    /**
     * Declaración de constructor
     * @param checkProveedor
     * @param empleado
     * @param mueble
     * @param proveedor
     * @param aReturn
     */

    public ZmpiIngresoAhuecos(String checkProveedor, String empleado, String mueble, String proveedor, List<TableOfBapiret2> aReturn) {
        CheckProveedor = checkProveedor;
        Empleado = empleado;
        Mueble = mueble;
        Proveedor = proveedor;
        Return = aReturn;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getCheckProveedor() {
        return CheckProveedor;
    }

    public void setCheckProveedor(String checkProveedor) {
        CheckProveedor = checkProveedor;
    }

    public String getEmpleado() {
        return Empleado;
    }

    public void setEmpleado(String empleado) {
        Empleado = empleado;
    }

    public String getMueble() {
        return Mueble;
    }

    public void setMueble(String mueble) {
        Mueble = mueble;
    }

    public String getProveedor() {
        return Proveedor;
    }

    public void setProveedor(String proveedor) {
        Proveedor = proveedor;
    }

    public List<TableOfBapiret2> getReturn() {
        return Return;
    }

    public void setReturn(List<TableOfBapiret2> aReturn) {
        Return = aReturn;
    }
}
