package netasystems.net.pantallaschedraui.DTOS.zmimfm_print_accessvia;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmimfmPrintAccessviaResponse {

    /**
     * Declaración de lista
     */

    private List<Bapireturn> EReturn;

    /**
     * Declaración de constructor
     * @param EReturn
     */

    public ZmimfmPrintAccessviaResponse(List<Bapireturn> EReturn) {
        this.EReturn = EReturn;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<Bapireturn> getEReturn() {
        return EReturn;
    }

    public void setEReturn(List<Bapireturn> EReturn) {
        this.EReturn = EReturn;
    }
}
