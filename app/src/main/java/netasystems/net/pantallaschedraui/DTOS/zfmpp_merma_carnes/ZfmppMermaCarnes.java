package netasystems.net.pantallaschedraui.DTOS.zfmpp_merma_carnes;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZfmppMermaCarnes {

    /**
     * Declaración de lista
     */

    private List<TableOfZsppEtiquetaCarnes> Etiqueta;

    /**
     * Declaración de constructor
     * @param etiqueta
     */

    public ZfmppMermaCarnes(List<TableOfZsppEtiquetaCarnes> etiqueta) {
        Etiqueta = etiqueta;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZsppEtiquetaCarnes> getEtiqueta() {
        return Etiqueta;
    }

    public void setEtiqueta(List<TableOfZsppEtiquetaCarnes> etiqueta) {
        Etiqueta = etiqueta;
    }
}
