package netasystems.net.pantallaschedraui.DTOS.zmpi_art_anaquel;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

class Zeean {

    /**
     * Declaración de variable
     */

    private String Ean; //char18

    /**
     * Declaración de constructor
     * @param ean
     */

    public Zeean(String ean) {
        Ean = ean;
    }

    /**
     * Getters y Stters
     * @return
     */

    public String getEan() {
        return Ean;
    }

    public void setEan(String ean) {
        Ean = ean;
    }
}
