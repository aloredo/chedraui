package netasystems.net.pantallaschedraui.DTOS.zmimfm_cs_consulta;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class Bapiret2 {

    /**
     * Declaración de variables
     */

    private String Type;    //char1
    private String Id;  //char20
    private String Number;  //numeric3
    private String Message; //char220
    private String LogNo;   //char20
    private String LogMsgNo;    //numeric6
    private String MessageV1;   //char50
    private String MessageV2;   //char50
    private String MessageV3;   //char50
    private String MessageV4;   //char50
    private String Parameter;   //char32
    private int Row;    //int
    private String Field;   //char30
    private String System;  //char10

    /**
     * Declaración de constructor
     * @param type
     * @param id
     * @param number
     * @param message
     * @param logNo
     * @param logMsgNo
     * @param messageV1
     * @param messageV2
     * @param messageV3
     * @param messageV4
     * @param parameter
     * @param row
     * @param field
     * @param system
     */

    public Bapiret2(String type, String id, String number, String message, String logNo, String logMsgNo, String messageV1, String messageV2, String messageV3, String messageV4, String parameter, int row, String field, String system) {
        Type = type;
        Id = id;
        Number = number;
        Message = message;
        LogNo = logNo;
        LogMsgNo = logMsgNo;
        MessageV1 = messageV1;
        MessageV2 = messageV2;
        MessageV3 = messageV3;
        MessageV4 = messageV4;
        Parameter = parameter;
        Row = row;
        Field = field;
        System = system;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getLogNo() {
        return LogNo;
    }

    public void setLogNo(String logNo) {
        LogNo = logNo;
    }

    public String getLogMsgNo() {
        return LogMsgNo;
    }

    public void setLogMsgNo(String logMsgNo) {
        LogMsgNo = logMsgNo;
    }

    public String getMessageV1() {
        return MessageV1;
    }

    public void setMessageV1(String messageV1) {
        MessageV1 = messageV1;
    }

    public String getMessageV2() {
        return MessageV2;
    }

    public void setMessageV2(String messageV2) {
        MessageV2 = messageV2;
    }

    public String getMessageV3() {
        return MessageV3;
    }

    public void setMessageV3(String messageV3) {
        MessageV3 = messageV3;
    }

    public String getMessageV4() {
        return MessageV4;
    }

    public void setMessageV4(String messageV4) {
        MessageV4 = messageV4;
    }

    public String getParameter() {
        return Parameter;
    }

    public void setParameter(String parameter) {
        Parameter = parameter;
    }

    public int getRow() {
        return Row;
    }

    public void setRow(int row) {
        Row = row;
    }

    public String getField() {
        return Field;
    }

    public void setField(String field) {
        Field = field;
    }

    public String getSystem() {
        return System;
    }

    public void setSystem(String system) {
        System = system;
    }
}
