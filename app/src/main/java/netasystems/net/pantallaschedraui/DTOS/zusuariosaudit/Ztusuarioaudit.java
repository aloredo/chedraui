package netasystems.net.pantallaschedraui.DTOS.zusuariosaudit;

import java.io.Serializable;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class Ztusuarioaudit implements Serializable {

     private static final long serialVersionUID = 1L;

     /**
      * Declaración de variables
      */

     private String Mandt;
     private String Usuario;
     private String Auditoria;
     private String Etiquetamasiva;
     private String Confirmacion;
     private String Opcion1;
     private String Opcion2;
     private String Opcion3;
     private String Opcion4;
     private String Opcion5;
     private String Opcion6;
     private String Opcion7;
     private String Opcion8;
     private String Opcion9;
     private String Opcion10;
     private String Opcion11;
     private String Opcion12;
     private String Opcion13;
     private String Opcion14;
     private String Opcion15;

     //Declaracion de constructor vacio
     public Ztusuarioaudit() {
     }

     /**
      * Declaración de constructor
      * @param mandt
      * @param usuario
      * @param auditoria
      * @param etiquetamasiva
      * @param confirmacion
      * @param opcion1
      * @param opcion2
      * @param opcion3
      * @param opcion4
      * @param opcion5
      * @param opcion6
      * @param opcion7
      * @param opcion8
      * @param opcion9
      * @param opcion10
      * @param opcion11
      * @param opcion12
      * @param opcion13
      * @param opcion14
      * @param opcion15
      */

     public Ztusuarioaudit(String mandt, String usuario, String auditoria, String etiquetamasiva, String confirmacion, String opcion1, String opcion2, String opcion3, String opcion4, String opcion5, String opcion6, String opcion7, String opcion8, String opcion9, String opcion10, String opcion11, String opcion12, String opcion13, String opcion14, String opcion15) {
          Mandt = mandt;
          Usuario = usuario;
          Auditoria = auditoria;
          Etiquetamasiva = etiquetamasiva;
          Confirmacion = confirmacion;
          Opcion1 = opcion1;
          Opcion2 = opcion2;
          Opcion3 = opcion3;
          Opcion4 = opcion4;
          Opcion5 = opcion5;
          Opcion6 = opcion6;
          Opcion7 = opcion7;
          Opcion8 = opcion8;
          Opcion9 = opcion9;
          Opcion10 = opcion10;
          Opcion11 = opcion11;
          Opcion12 = opcion12;
          Opcion13 = opcion13;
          Opcion14 = opcion14;
          Opcion15 = opcion15;
     }

     /**
      * Getters y Setters
      * @return
      */

     public String getMandt() {
          return Mandt;
     }

     public void setMandt(String mandt) {
          Mandt = mandt;
     }

     public String getUsuario() {
          return Usuario;
     }

     public void setUsuario(String usuario) {
          Usuario = usuario;
     }

     public String getAuditoria() {
          return Auditoria;
     }

     public void setAuditoria(String auditoria) {
          Auditoria = auditoria;
     }

     public String getEtiquetamasiva() {
          return Etiquetamasiva;
     }

     public void setEtiquetamasiva(String etiquetamasiva) {
          Etiquetamasiva = etiquetamasiva;
     }

     public String getConfirmacion() {
          return Confirmacion;
     }

     public void setConfirmacion(String confirmacion) {
          Confirmacion = confirmacion;
     }

     public String getOpcion1() {
          return Opcion1;
     }

     public void setOpcion1(String opcion1) {
          Opcion1 = opcion1;
     }

     public String getOpcion2() {
          return Opcion2;
     }

     public void setOpcion2(String opcion2) {
          Opcion2 = opcion2;
     }

     public String getOpcion3() {
          return Opcion3;
     }

     public void setOpcion3(String opcion3) {
          Opcion3 = opcion3;
     }

     public String getOpcion4() {
          return Opcion4;
     }

     public void setOpcion4(String opcion4) {
          Opcion4 = opcion4;
     }

     public String getOpcion5() {
          return Opcion5;
     }

     public void setOpcion5(String opcion5) {
          Opcion5 = opcion5;
     }

     public String getOpcion6() {
          return Opcion6;
     }

     public void setOpcion6(String opcion6) {
          Opcion6 = opcion6;
     }

     public String getOpcion7() {
          return Opcion7;
     }

     public void setOpcion7(String opcion7) {
          Opcion7 = opcion7;
     }

     public String getOpcion8() {
          return Opcion8;
     }

     public void setOpcion8(String opcion8) {
          Opcion8 = opcion8;
     }

     public String getOpcion9() {
          return Opcion9;
     }

     public void setOpcion9(String opcion9) {
          Opcion9 = opcion9;
     }

     public String getOpcion10() {
          return Opcion10;
     }

     public void setOpcion10(String opcion10) {
          Opcion10 = opcion10;
     }

     public String getOpcion11() {
          return Opcion11;
     }

     public void setOpcion11(String opcion11) {
          Opcion11 = opcion11;
     }

     public String getOpcion12() {
          return Opcion12;
     }

     public void setOpcion12(String opcion12) {
          Opcion12 = opcion12;
     }

     public String getOpcion13() {
          return Opcion13;
     }

     public void setOpcion13(String opcion13) {
          Opcion13 = opcion13;
     }

     public String getOpcion14() {
          return Opcion14;
     }

     public void setOpcion14(String opcion14) {
          Opcion14 = opcion14;
     }

     public String getOpcion15() {
          return Opcion15;
     }

     public void setOpcion15(String opcion15) {
          Opcion15 = opcion15;
     }
}
