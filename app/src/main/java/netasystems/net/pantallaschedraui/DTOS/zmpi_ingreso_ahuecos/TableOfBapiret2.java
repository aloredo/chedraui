package netasystems.net.pantallaschedraui.DTOS.zmpi_ingreso_ahuecos;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

class TableOfBapiret2 {

    /**
     * Declaración lista
     */

    private List<Bapiret2> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public TableOfBapiret2(List<Bapiret2> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<Bapiret2> getItem() {
        return item;
    }

    public void setItem(List<Bapiret2> item) {
        this.item = item;
    }
}
