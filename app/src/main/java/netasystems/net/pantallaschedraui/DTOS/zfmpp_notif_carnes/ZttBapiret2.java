package netasystems.net.pantallaschedraui.DTOS.zfmpp_notif_carnes;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class ZttBapiret2 {

    /**
     * Declaración de listas
     */

    private List<Bapiret2> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public ZttBapiret2(List<Bapiret2> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<Bapiret2> getItem() {
        return item;
    }

    public void setItem(List<Bapiret2> item) {
        this.item = item;
    }
}
