package netasystems.net.pantallaschedraui.DTOS.zfmpp_notif_carnes;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZsppEtiquetaCarnes {

    /**
     * Declaración de variables
     */

    private String Etiqueta;

    /**
     * Declaración de constructor
     * @param etiqueta
     */

    public ZsppEtiquetaCarnes(String etiqueta) {
        Etiqueta = etiqueta;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getEtiqueta() {
        return Etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        Etiqueta = etiqueta;
    }
}
