package netasystems.net.pantallaschedraui.DTOS.zep_get_parameters;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZepGetParameters implements KvmSerializable {

    /**
     * Declaración de lista y variables
     */

    private List<TableOfZtmprParam> TyTParametros;
    private String Zobj1;
    private String Zprog;

    public ZepGetParameters(){
        this.Zobj1 = "";
        this.Zprog = "";

    }


    /**
     * Declaración de constructor
     * @param tyTParametros
     * @param zobj1
     * @param zprog
     */

    public ZepGetParameters(List<TableOfZtmprParam> tyTParametros, String zobj1, String zprog) {
        TyTParametros = tyTParametros;
        Zobj1 = zobj1;
        Zprog = zprog;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZtmprParam> getTyTParametros() {
        return TyTParametros;
    }

    public void setTyTParametros(List<TableOfZtmprParam> tyTParametros) {
        TyTParametros = tyTParametros;
    }

    public String getZobj1() {
        return Zobj1;
    }

    public void setZobj1(String zobj1) {
        Zobj1 = zobj1;
    }

    public String getZprog() {
        return Zprog;
    }

    public void setZprog(String zprog) {
        Zprog = zprog;
    }

    /***********************************************************************/

    @Override
    public Object getProperty(int index) {
        switch (index){
            case 0:
                return this.Zprog;
            case 1:
                return this.Zobj1;
             default:
                 break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 0;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index){
            case 0:
                this.Zobj1 = value.toString();
            case 1:
                this.Zprog = value.toString();
            default:
                break;
        }
    }

    @Override
    public void getPropertyInfo(int index, Hashtable hashtable, PropertyInfo info) {
        switch (index){
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zobj1";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zprog";
                break;
            default:
                break;
        }

    }
}
