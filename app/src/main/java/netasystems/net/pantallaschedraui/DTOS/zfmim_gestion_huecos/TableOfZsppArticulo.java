package netasystems.net.pantallaschedraui.DTOS.zfmim_gestion_huecos;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class TableOfZsppArticulo {

    /**
     * Declaración de lista
     */

    private List<ZsppArticulo> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public TableOfZsppArticulo(List<ZsppArticulo> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<ZsppArticulo> getItem() {
        return item;
    }

    public void setItem(List<ZsppArticulo> item) {
        this.item = item;
    }
}
