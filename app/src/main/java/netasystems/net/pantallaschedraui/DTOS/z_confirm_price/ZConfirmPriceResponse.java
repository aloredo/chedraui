package netasystems.net.pantallaschedraui.DTOS.z_confirm_price;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZConfirmPriceResponse implements KvmSerializable {

    /**
     * Declaración lista
     */

    private List<TableOfZpreciosPendtes> ItPreciosPendtes;

    /**
     * Declaración de constructor
     * @param itPreciosPendtes
     */

    public ZConfirmPriceResponse(List<TableOfZpreciosPendtes> itPreciosPendtes) {
        ItPreciosPendtes = itPreciosPendtes;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZpreciosPendtes> getItPreciosPendtes() {
        return ItPreciosPendtes;
    }

    public void setItPreciosPendtes(List<TableOfZpreciosPendtes> itPreciosPendtes) {
        ItPreciosPendtes = itPreciosPendtes;
    }

    /**
     * Declaración de clase
     */

    private  Zreturndata zreturndata = new Zreturndata();

    @Override
    public Object getProperty(int index) {
        switch (index){
            case 0:
                return zreturndata;
                default:
                    break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index){
            case 0:
                this.zreturndata = (Zreturndata) value;
                break;
            default:
                break;
        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable hashtable, PropertyInfo info) {
        switch (index){
            case 0:
                info.type = Zreturndata.class;
                info.name = "ItPreciosPendtes";
                break;
            default:
                break;
        }

    }
}
