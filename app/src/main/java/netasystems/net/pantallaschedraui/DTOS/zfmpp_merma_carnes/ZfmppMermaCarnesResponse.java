package netasystems.net.pantallaschedraui.DTOS.zfmpp_merma_carnes;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZfmppMermaCarnesResponse {

    /**
     * Declaración de listas
     */

    private List<TableOfZsppEtiquetaCarnes> Etiqueta;
    private List<TtBapiret2> Return;

    /**
     * Declaración de constructor
     * @param etiqueta
     * @param aReturn
     */

    public ZfmppMermaCarnesResponse(List<TableOfZsppEtiquetaCarnes> etiqueta, List<TtBapiret2> aReturn) {
        Etiqueta = etiqueta;
        Return = aReturn;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZsppEtiquetaCarnes> getEtiqueta() {
        return Etiqueta;
    }

    public void setEtiqueta(List<TableOfZsppEtiquetaCarnes> etiqueta) {
        Etiqueta = etiqueta;
    }

    public List<TtBapiret2> getReturn() {
        return Return;
    }

    public void setReturn(List<TtBapiret2> aReturn) {
        Return = aReturn;
    }
}
