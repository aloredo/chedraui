package netasystems.net.pantallaschedraui.DTOS.zmimfm_det_master_data_price6;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmimfmDetMasterDataPrice6 {

    /**
     * Declaración de contructor
     * @param IEan11
     * @param IMatnrAndPrice
     * @param IWerks
     * @param TMean
     * @param TMuebles
     */

    public ZmimfmDetMasterDataPrice6(String IEan11, String IMatnrAndPrice, String IWerks, List<TableOfZtmeanEmpq> TMean, List<TableOfZmtn> TMuebles) {
        this.IEan11 = IEan11;
        this.IMatnrAndPrice = IMatnrAndPrice;
        this.IWerks = IWerks;
        this.TMean = TMean;
        this.TMuebles = TMuebles;
    }

    /**
     * Declaración de listas y varibles
     */

    private String IEan11;
    private String IMatnrAndPrice;
    private String IWerks;
    private List<TableOfZtmeanEmpq> TMean;
    private List<TableOfZmtn> TMuebles;

    /**
     * Getters y Setters
     */

    public String getIEan11() {
        return IEan11;
    }

    public void setIEan11(String IEan11) {
        this.IEan11 = IEan11;
    }

    public String getIMatnrAndPrice() {
        return IMatnrAndPrice;
    }

    public void setIMatnrAndPrice(String IMatnrAndPrice) {
        this.IMatnrAndPrice = IMatnrAndPrice;
    }

    public String getIWerks() {
        return IWerks;
    }

    public void setIWerks(String IWerks) {
        this.IWerks = IWerks;
    }

    public List<TableOfZtmeanEmpq> getTMean() {
        return TMean;
    }

    public void setTMean(List<TableOfZtmeanEmpq> TMean) {
        this.TMean = TMean;
    }

    public List<TableOfZmtn> getTMuebles() {
        return TMuebles;
    }

    public void setTMuebles(List<TableOfZmtn> TMuebles) {
        this.TMuebles = TMuebles;
    }


}
