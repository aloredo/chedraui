package netasystems.net.pantallaschedraui.DTOS.zmimfm_pos_response;


/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmimfmPosResponse {

    /**
     * Declaración de variables
     */

    private String IDeSdprIpadr;
    private String IEan11;
    private String IMatl;
    private double IPrecio;
    private String IPuertoCom;
    private String IStatus;
    private String IWerks;
    private double IZlprc;
    private double IZmdpr;
    private Integer IZprnt;

    /**
     * Declaración de constructor
     * @param IDeSdprIpadr
     * @param IEan11
     * @param IMatl
     * @param IPrecio
     * @param IPuertoCom
     * @param IStatus
     * @param IWerks
     * @param IZlprc
     * @param IZmdpr
     * @param IZprnt
     */

    public ZmimfmPosResponse(String IDeSdprIpadr, String IEan11, String IMatl, double IPrecio, String IPuertoCom, String IStatus, String IWerks, double IZlprc, double IZmdpr, Integer IZprnt) {
        this.IDeSdprIpadr = IDeSdprIpadr;
        this.IEan11 = IEan11;
        this.IMatl = IMatl;
        this.IPrecio = IPrecio;
        this.IPuertoCom = IPuertoCom;
        this.IStatus = IStatus;
        this.IWerks = IWerks;
        this.IZlprc = IZlprc;
        this.IZmdpr = IZmdpr;
        this.IZprnt = IZprnt;
    }

    /**
     * Settars y Getters
     * @return
     */

    public String getIDeSdprIpadr() {
        return IDeSdprIpadr;
    }

    public void setIDeSdprIpadr(String IDeSdprIpadr) {
        this.IDeSdprIpadr = IDeSdprIpadr;
    }

    public String getIEan11() {
        return IEan11;
    }

    public void setIEan11(String IEan11) {
        this.IEan11 = IEan11;
    }

    public String getIMatl() {
        return IMatl;
    }

    public void setIMatl(String IMatl) {
        this.IMatl = IMatl;
    }

    public double getIPrecio() {
        return IPrecio;
    }

    public void setIPrecio(double IPrecio) {
        this.IPrecio = IPrecio;
    }

    public String getIPuertoCom() {
        return IPuertoCom;
    }

    public void setIPuertoCom(String IPuertoCom) {
        this.IPuertoCom = IPuertoCom;
    }

    public String getIStatus() {
        return IStatus;
    }

    public void setIStatus(String IStatus) {
        this.IStatus = IStatus;
    }

    public String getIWerks() {
        return IWerks;
    }

    public void setIWerks(String IWerks) {
        this.IWerks = IWerks;
    }

    public double getIZlprc() {
        return IZlprc;
    }

    public void setIZlprc(double IZlprc) {
        this.IZlprc = IZlprc;
    }

    public double getIZmdpr() {
        return IZmdpr;
    }

    public void setIZmdpr(double IZmdpr) {
        this.IZmdpr = IZmdpr;
    }

    public Integer getIZprnt() {
        return IZprnt;
    }

    public void setIZprnt(Integer IZprnt) {
        this.IZprnt = IZprnt;
    }
}
