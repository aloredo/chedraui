package netasystems.net.pantallaschedraui.DTOS.z_get_pending_price;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZGetPendingPriceResponse implements KvmSerializable {

    /**
     * Declaración de listas y variables
     */

    private String ESendMess;
    private String EZdescripcionart;
    private List<TableOfZpendingPriceOutputTable> ItOutputTable;
    private List<TableOfZpreciosPendtes> ItPreciosPendtes;

    public ZGetPendingPriceResponse(){
        this.ESendMess = "";
        this.EZdescripcionart = "";
    }

    /**
     * Delcaración de constructor
     * @param ESendMess
     * @param EZdescripcionart
     * @param itOutputTable
     * @param itPreciosPendtes
     */

    public ZGetPendingPriceResponse(String ESendMess, String EZdescripcionart, List<TableOfZpendingPriceOutputTable> itOutputTable, List<TableOfZpreciosPendtes> itPreciosPendtes) {
        this.ESendMess = ESendMess;
        this.EZdescripcionart = EZdescripcionart;
        ItOutputTable = itOutputTable;
        ItPreciosPendtes = itPreciosPendtes;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getESendMess() {
        return ESendMess;
    }

    public void setESendMess(String ESendMess) {
        this.ESendMess = ESendMess;
    }

    public String getEZdescripcionart() {
        return EZdescripcionart;
    }

    public void setEZdescripcionart(String EZdescripcionart) {
        this.EZdescripcionart = EZdescripcionart;
    }

    public List<TableOfZpendingPriceOutputTable> getItOutputTable() {
        return ItOutputTable;
    }

    public void setItOutputTable(List<TableOfZpendingPriceOutputTable> itOutputTable) {
        ItOutputTable = itOutputTable;
    }

    public List<TableOfZpreciosPendtes> getItPreciosPendtes() {
        return ItPreciosPendtes;
    }

    public void setItPreciosPendtes(List<TableOfZpreciosPendtes> itPreciosPendtes) {
        ItPreciosPendtes = itPreciosPendtes;
    }

    @Override
    public Object getProperty(int index) {
        switch (index) {
            case 0:
                return this.ESendMess;
            case 1:
                return this.EZdescripcionart;
            case 2:
                return this.ItOutputTable;
            case 3:
                return this.ItPreciosPendtes;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index) {
            case 0:
                this.ESendMess = value.toString();
            case 1:
                this.EZdescripcionart = value.toString();
            default:
                break;

        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable hashtable, PropertyInfo info) {
        switch (index) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ESendMess";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "EZdescripcionart";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ItOutputTable";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ItPreciosPendtes";
                break;
            default:
                break;
        }

    }
}
