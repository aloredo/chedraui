package netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

class ZtmmAhuecosTem {

    /**
     * Declaración de variables
     */

    private String Mandt;
    private String Ztienda;
    private String Zmueble;
    private String Zupc;
    private String Zdepto;
    private String Znnomdepto;
    private String Zsubdepto;
    private String Znomsubdepto;
    private String Zclase;
    private String Znomclase;
    private String Zsubclase;
    private String Znomsubclase;
    private String Zarticulo;
    private String Zgpoarticulo;
    private String Zdescripcion;
    private String Zproveedor;
    private String Zpnombre;
    private double Zexistencia;
    private double Zventa;
    private String Estatus;
    private String UsuarioCreacion;
    private String FechaCreacion;
    private String HoraCreacion;
    private String Zarticulomms;
    private String Zmstae;
    private String Zescaneado;
    private double Zsurtido;
    private String Zmmsta;

    /**
     * Declaraión de constructor
     * @param mandt
     * @param ztienda
     * @param zmueble
     * @param zupc
     * @param zdepto
     * @param znnomdepto
     * @param zsubdepto
     * @param znomsubdepto
     * @param zclase
     * @param znomclase
     * @param zsubclase
     * @param znomsubclase
     * @param zarticulo
     * @param zgpoarticulo
     * @param zdescripcion
     * @param zproveedor
     * @param zpnombre
     * @param zexistencia
     * @param zventa
     * @param estatus
     * @param usuarioCreacion
     * @param fechaCreacion
     * @param horaCreacion
     * @param zarticulomms
     * @param zmstae
     * @param zescaneado
     * @param zsurtido
     * @param zmmsta
     */

    public ZtmmAhuecosTem(String mandt, String ztienda, String zmueble, String zupc, String zdepto, String znnomdepto, String zsubdepto, String znomsubdepto, String zclase, String znomclase, String zsubclase, String znomsubclase, String zarticulo, String zgpoarticulo, String zdescripcion, String zproveedor, String zpnombre, double zexistencia, double zventa, String estatus, String usuarioCreacion, String fechaCreacion, String horaCreacion, String zarticulomms, String zmstae, String zescaneado, double zsurtido, String zmmsta) {
        Mandt = mandt;
        Ztienda = ztienda;
        Zmueble = zmueble;
        Zupc = zupc;
        Zdepto = zdepto;
        Znnomdepto = znnomdepto;
        Zsubdepto = zsubdepto;
        Znomsubdepto = znomsubdepto;
        Zclase = zclase;
        Znomclase = znomclase;
        Zsubclase = zsubclase;
        Znomsubclase = znomsubclase;
        Zarticulo = zarticulo;
        Zgpoarticulo = zgpoarticulo;
        Zdescripcion = zdescripcion;
        Zproveedor = zproveedor;
        Zpnombre = zpnombre;
        Zexistencia = zexistencia;
        Zventa = zventa;
        Estatus = estatus;
        UsuarioCreacion = usuarioCreacion;
        FechaCreacion = fechaCreacion;
        HoraCreacion = horaCreacion;
        Zarticulomms = zarticulomms;
        Zmstae = zmstae;
        Zescaneado = zescaneado;
        Zsurtido = zsurtido;
        Zmmsta = zmmsta;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getMandt() {
        return Mandt;
    }

    public void setMandt(String mandt) {
        Mandt = mandt;
    }

    public String getZtienda() {
        return Ztienda;
    }

    public void setZtienda(String ztienda) {
        Ztienda = ztienda;
    }

    public String getZmueble() {
        return Zmueble;
    }

    public void setZmueble(String zmueble) {
        Zmueble = zmueble;
    }

    public String getZupc() {
        return Zupc;
    }

    public void setZupc(String zupc) {
        Zupc = zupc;
    }

    public String getZdepto() {
        return Zdepto;
    }

    public void setZdepto(String zdepto) {
        Zdepto = zdepto;
    }

    public String getZnnomdepto() {
        return Znnomdepto;
    }

    public void setZnnomdepto(String znnomdepto) {
        Znnomdepto = znnomdepto;
    }

    public String getZsubdepto() {
        return Zsubdepto;
    }

    public void setZsubdepto(String zsubdepto) {
        Zsubdepto = zsubdepto;
    }

    public String getZnomsubdepto() {
        return Znomsubdepto;
    }

    public void setZnomsubdepto(String znomsubdepto) {
        Znomsubdepto = znomsubdepto;
    }

    public String getZclase() {
        return Zclase;
    }

    public void setZclase(String zclase) {
        Zclase = zclase;
    }

    public String getZnomclase() {
        return Znomclase;
    }

    public void setZnomclase(String znomclase) {
        Znomclase = znomclase;
    }

    public String getZsubclase() {
        return Zsubclase;
    }

    public void setZsubclase(String zsubclase) {
        Zsubclase = zsubclase;
    }

    public String getZnomsubclase() {
        return Znomsubclase;
    }

    public void setZnomsubclase(String znomsubclase) {
        Znomsubclase = znomsubclase;
    }

    public String getZarticulo() {
        return Zarticulo;
    }

    public void setZarticulo(String zarticulo) {
        Zarticulo = zarticulo;
    }

    public String getZgpoarticulo() {
        return Zgpoarticulo;
    }

    public void setZgpoarticulo(String zgpoarticulo) {
        Zgpoarticulo = zgpoarticulo;
    }

    public String getZdescripcion() {
        return Zdescripcion;
    }

    public void setZdescripcion(String zdescripcion) {
        Zdescripcion = zdescripcion;
    }

    public String getZproveedor() {
        return Zproveedor;
    }

    public void setZproveedor(String zproveedor) {
        Zproveedor = zproveedor;
    }

    public String getZpnombre() {
        return Zpnombre;
    }

    public void setZpnombre(String zpnombre) {
        Zpnombre = zpnombre;
    }

    public double getZexistencia() {
        return Zexistencia;
    }

    public void setZexistencia(double zexistencia) {
        Zexistencia = zexistencia;
    }

    public double getZventa() {
        return Zventa;
    }

    public void setZventa(double zventa) {
        Zventa = zventa;
    }

    public String getEstatus() {
        return Estatus;
    }

    public void setEstatus(String estatus) {
        Estatus = estatus;
    }

    public String getUsuarioCreacion() {
        return UsuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        UsuarioCreacion = usuarioCreacion;
    }

    public String getFechaCreacion() {
        return FechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        FechaCreacion = fechaCreacion;
    }

    public String getHoraCreacion() {
        return HoraCreacion;
    }

    public void setHoraCreacion(String horaCreacion) {
        HoraCreacion = horaCreacion;
    }

    public String getZarticulomms() {
        return Zarticulomms;
    }

    public void setZarticulomms(String zarticulomms) {
        Zarticulomms = zarticulomms;
    }

    public String getZmstae() {
        return Zmstae;
    }

    public void setZmstae(String zmstae) {
        Zmstae = zmstae;
    }

    public String getZescaneado() {
        return Zescaneado;
    }

    public void setZescaneado(String zescaneado) {
        Zescaneado = zescaneado;
    }

    public double getZsurtido() {
        return Zsurtido;
    }

    public void setZsurtido(double zsurtido) {
        Zsurtido = zsurtido;
    }

    public String getZmmsta() {
        return Zmmsta;
    }

    public void setZmmsta(String zmmsta) {
        Zmmsta = zmmsta;
    }
}
