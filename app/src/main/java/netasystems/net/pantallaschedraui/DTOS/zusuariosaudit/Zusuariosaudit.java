package netasystems.net.pantallaschedraui.DTOS.zusuariosaudit;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class Zusuariosaudit {

    /**
     * Declaración de lista
     */

    private List<TableOfZtusuarioaudit> TUsuarios;

    /**
     * Declaración de constructor
     * @param TUsuarios
     */

    public Zusuariosaudit(List<TableOfZtusuarioaudit> TUsuarios) {
        this.TUsuarios = TUsuarios;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZtusuarioaudit> getTUsuarios() {
        return TUsuarios;
    }

    public void setTUsuarios(List<TableOfZtusuarioaudit> TUsuarios) {
        this.TUsuarios = TUsuarios;
    }
}
