package netasystems.net.pantallaschedraui.DTOS.zmpi_articulos_agotados;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class TableOfZsmmEtiqFaltHuecos {

    /**
     * Declaración Lista
     */

    private List<ZsmmEtiqFaltHuecos> ZsmmEtiqFaltHuecos;

    /**
     * Declaración de constructor
     * @param zsmmEtiqFaltHuecos
     */

    public TableOfZsmmEtiqFaltHuecos(List<netasystems.net.pantallaschedraui.DTOS.zmpi_articulos_agotados.ZsmmEtiqFaltHuecos> zsmmEtiqFaltHuecos) {
        ZsmmEtiqFaltHuecos = zsmmEtiqFaltHuecos;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<netasystems.net.pantallaschedraui.DTOS.zmpi_articulos_agotados.ZsmmEtiqFaltHuecos> getZsmmEtiqFaltHuecos() {
        return ZsmmEtiqFaltHuecos;
    }

    public void setZsmmEtiqFaltHuecos(List<netasystems.net.pantallaschedraui.DTOS.zmpi_articulos_agotados.ZsmmEtiqFaltHuecos> zsmmEtiqFaltHuecos) {
        ZsmmEtiqFaltHuecos = zsmmEtiqFaltHuecos;
    }
}
