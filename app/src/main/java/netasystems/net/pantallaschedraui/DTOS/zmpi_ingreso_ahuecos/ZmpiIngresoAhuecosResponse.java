package netasystems.net.pantallaschedraui.DTOS.zmpi_ingreso_ahuecos;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiIngresoAhuecosResponse {

     /**
      * Declaración de lista
      */

     private List<TableOfBapiret2> Return; //TableOfBapiret2

     /**
      * Declaración de constructor
      * @param aReturn
      */

     public ZmpiIngresoAhuecosResponse(List<TableOfBapiret2> aReturn) {
          Return = aReturn;
     }

     /**
      * Getters y Setters
      * @return
      */

     public List<TableOfBapiret2> getReturn() {
          return Return;
     }

     public void setReturn(List<TableOfBapiret2> aReturn) {
          Return = aReturn;
     }
}
