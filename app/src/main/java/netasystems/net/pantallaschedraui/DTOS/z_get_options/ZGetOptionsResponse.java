package netasystems.net.pantallaschedraui.DTOS.z_get_options;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZGetOptionsResponse implements KvmSerializable {

    /**
     * Declaración de varieble
     */

    private String Zdatfi;

    public ZGetOptionsResponse (){
        this.Zdatfi = "";
    }

    /**
     * Declaración de constructor
     * @param zdatfi
     */

    public ZGetOptionsResponse(String zdatfi) {
        Zdatfi = zdatfi;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getZdatfi() {
        return Zdatfi;
    }

    public void setZdatfi(String zdatfi) {
        Zdatfi = zdatfi;
    }

    @Override
    public Object getProperty(int index) {
        switch (index){
            case 0:
                return this.Zdatfi;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 0;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index){
            case 0:
                this.Zdatfi = value.toString();
            default:
                break;
        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable hashtable, PropertyInfo info) {
        switch (index){
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zdatfi";
                break;
            default:
                break;
        }

    }
}
