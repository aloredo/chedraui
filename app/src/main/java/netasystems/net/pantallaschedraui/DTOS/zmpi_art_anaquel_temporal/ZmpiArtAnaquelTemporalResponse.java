package netasystems.net.pantallaschedraui.DTOS.zmpi_art_anaquel_temporal;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiArtAnaquelTemporalResponse {

    /**
     * Declaración de variable y Listas
     */

    private List<TableOfZeean> Ean;
    private Integer Error;
    private List<TableOfZartAnaquelTem> ItRecupera;

    /**
     * Declaración de constructor
     * @param ean
     * @param error
     * @param itRecupera
     */

    public ZmpiArtAnaquelTemporalResponse(List<TableOfZeean> ean, Integer error, List<TableOfZartAnaquelTem> itRecupera) {
        Ean = ean;
        Error = error;
        ItRecupera = itRecupera;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZeean> getEan() {
        return Ean;
    }

    public void setEan(List<TableOfZeean> ean) {
        Ean = ean;
    }

    public Integer getError() {
        return Error;
    }

    public void setError(Integer error) {
        Error = error;
    }

    public List<TableOfZartAnaquelTem> getItRecupera() {
        return ItRecupera;
    }

    public void setItRecupera(List<TableOfZartAnaquelTem> itRecupera) {
        ItRecupera = itRecupera;
    }
}
