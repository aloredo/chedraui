package netasystems.net.pantallaschedraui.DTOS.zmimfm_det_store;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmimfmDetStore {

    /**
     * Declaración de variable
     */

    private String Uname;

    /**
     * Declaración de constructor
     * @param uname
     */

    public ZmimfmDetStore(String uname) {
        Uname = uname;
    }

    /**
     * Getters y Setters
     */

    public String getUname() {
        return Uname;
    }

    public void setUname(String uname) {
        Uname = uname;
    }
}
