package netasystems.net.pantallaschedraui.DTOS.zmimfm_det_master_data_price6;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

class Zmtn {

    /**
     * Declaración de variables
     */

    //String
    private String Zmueble;
    //Numeric
    private String Ztramo;
    //Numeric
    private String Znivel;

    /**
     * Declaración de constructor
     * @param zmueble
     * @param ztramo
     * @param znivel
     */

    public Zmtn(String zmueble, String ztramo, String znivel) {
        Zmueble = zmueble;
        Ztramo = ztramo;
        Znivel = znivel;
    }

    /**
     *Getters y Setters
     */

    public String getZmueble() {
        return Zmueble;
    }

    public void setZmueble(String zmueble) {
        Zmueble = zmueble;
    }

    public String getZtramo() {
        return Ztramo;
    }

    public void setZtramo(String ztramo) {
        Ztramo = ztramo;
    }

    public String getZnivel() {
        return Znivel;
    }

    public void setZnivel(String znivel) {
        Znivel = znivel;
    }
}
