package netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiAhuecosTemporalResponse {

    /**
     * Declaración de variable y listas
     */

    private List<TableOfZeean> Ean;
    private Integer Error;
    private List<TableOfZtmmAhuecosTem> ItRecupera;

    /**
     * Declaración de constructor
     * @param ean
     * @param error
     * @param itRecupera
     */

    public ZmpiAhuecosTemporalResponse(List<TableOfZeean> ean, Integer error, List<TableOfZtmmAhuecosTem> itRecupera) {
        Ean = ean;
        Error = error;
        ItRecupera = itRecupera;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZeean> getEan() {
        return Ean;
    }

    public void setEan(List<TableOfZeean> ean) {
        Ean = ean;
    }

    public Integer getError() {
        return Error;
    }

    public void setError(Integer error) {
        Error = error;
    }

    public List<TableOfZtmmAhuecosTem> getItRecupera() {
        return ItRecupera;
    }

    public void setItRecupera(List<TableOfZtmmAhuecosTem> itRecupera) {
        ItRecupera = itRecupera;
    }
}
