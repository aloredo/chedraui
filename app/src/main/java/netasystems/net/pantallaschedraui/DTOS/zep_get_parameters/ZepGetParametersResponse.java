package netasystems.net.pantallaschedraui.DTOS.zep_get_parameters;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZepGetParametersResponse implements KvmSerializable {

    /**
     * Declaración de clase
     */
    private TableOfZtmprParam tyTParametros = new TableOfZtmprParam();


    @Override
    public Object getProperty(int index) {
        switch (index){
            case 0:
                return tyTParametros;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index){
            case 0:
                this.tyTParametros = (TableOfZtmprParam) value;
                break;
            default:
                break;
        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        switch (index){
            case 0:
                info.type = TableOfZtmprParam.class;
                info.name = "item";
                break;
            default:
                break;
        }

    }
}



