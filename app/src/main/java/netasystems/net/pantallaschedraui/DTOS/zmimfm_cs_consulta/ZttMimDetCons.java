package netasystems.net.pantallaschedraui.DTOS.zmimfm_cs_consulta;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class ZttMimDetCons {

    /**
     * Declaración de listas
     */

    private List<Ztmimcsubi> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public ZttMimDetCons(List<Ztmimcsubi> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<Ztmimcsubi> getItem() {
        return item;
    }

    public void setItem(List<Ztmimcsubi> item) {
        this.item = item;
    }
}
