package netasystems.net.pantallaschedraui.DTOS.zmimfm_det_master_data_price6;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

class TableOfZmtn {

    /**
     * Declaración de lista
     */

    private List<Zmtn> item;

    /**
     * Declaración de contructor
     */

    public TableOfZmtn(List<Zmtn> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     *
     */

    public List<Zmtn> getItem() {
        return item;
    }

    public void setItem(List<Zmtn> item) {
        this.item = item;
    }
}
