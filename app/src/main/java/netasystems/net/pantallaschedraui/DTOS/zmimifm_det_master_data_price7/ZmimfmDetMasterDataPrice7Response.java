package netasystems.net.pantallaschedraui.DTOS.zmimifm_det_master_data_price7;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmimfmDetMasterDataPrice7Response {

    /**
     * Declaración de listas y variables
     */

    private List<TableOfZtmeanEmpq> TMean;
    private List<TableOfZmtn> TMuebles;
    private String Zcodsat;
    private String Zdecil;
    private String Zmeinh;
    private String Zmmsta;
    private List<netasystems.net.pantallaschedraui.DTOS.zmimifm_det_master_data_price7.Zreturndata> Zreturndata;
    private String Zshelf;
    private String Zsorf1;
    private String Zumsat;
    private String ZzshelfId;

    /**
     * Declaración de constructor
     * @param TMean
     * @param TMuebles
     * @param zcodsat
     * @param zdecil
     * @param zmeinh
     * @param zmmsta
     * @param zreturndata
     * @param zshelf
     * @param zsorf1
     * @param zumsat
     * @param zzshelfId
     */

    public ZmimfmDetMasterDataPrice7Response(List<TableOfZtmeanEmpq> TMean, List<TableOfZmtn> TMuebles, String zcodsat, String zdecil, String zmeinh, String zmmsta, List<netasystems.net.pantallaschedraui.DTOS.zmimifm_det_master_data_price7.Zreturndata> zreturndata, String zshelf, String zsorf1, String zumsat, String zzshelfId) {
        this.TMean = TMean;
        this.TMuebles = TMuebles;
        Zcodsat = zcodsat;
        Zdecil = zdecil;
        Zmeinh = zmeinh;
        Zmmsta = zmmsta;
        Zreturndata = zreturndata;
        Zshelf = zshelf;
        Zsorf1 = zsorf1;
        Zumsat = zumsat;
        ZzshelfId = zzshelfId;
    }

    /**
     * Getters y Stters
     * @return
     */

    public List<TableOfZtmeanEmpq> getTMean() {
        return TMean;
    }

    public void setTMean(List<TableOfZtmeanEmpq> TMean) {
        this.TMean = TMean;
    }

    public List<TableOfZmtn> getTMuebles() {
        return TMuebles;
    }

    public void setTMuebles(List<TableOfZmtn> TMuebles) {
        this.TMuebles = TMuebles;
    }

    public String getZcodsat() {
        return Zcodsat;
    }

    public void setZcodsat(String zcodsat) {
        Zcodsat = zcodsat;
    }

    public String getZdecil() {
        return Zdecil;
    }

    public void setZdecil(String zdecil) {
        Zdecil = zdecil;
    }

    public String getZmeinh() {
        return Zmeinh;
    }

    public void setZmeinh(String zmeinh) {
        Zmeinh = zmeinh;
    }

    public String getZmmsta() {
        return Zmmsta;
    }

    public void setZmmsta(String zmmsta) {
        Zmmsta = zmmsta;
    }

    public List<netasystems.net.pantallaschedraui.DTOS.zmimifm_det_master_data_price7.Zreturndata> getZreturndata() {
        return Zreturndata;
    }

    public void setZreturndata(List<netasystems.net.pantallaschedraui.DTOS.zmimifm_det_master_data_price7.Zreturndata> zreturndata) {
        Zreturndata = zreturndata;
    }

    public String getZshelf() {
        return Zshelf;
    }

    public void setZshelf(String zshelf) {
        Zshelf = zshelf;
    }

    public String getZsorf1() {
        return Zsorf1;
    }

    public void setZsorf1(String zsorf1) {
        Zsorf1 = zsorf1;
    }

    public String getZumsat() {
        return Zumsat;
    }

    public void setZumsat(String zumsat) {
        Zumsat = zumsat;
    }

    public String getZzshelfId() {
        return ZzshelfId;
    }

    public void setZzshelfId(String zzshelfId) {
        ZzshelfId = zzshelfId;
    }
}
