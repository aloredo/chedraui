package netasystems.net.pantallaschedraui.DTOS.zmpi_art_anaquel;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiArtAnaquel {

    /**
     * Declaración de variables y Listas
     */

    private String Articulo;    //char18
    private List<TableOfZeean> Ean;    //TableOfZeean
    private List<TableOfZeean> EanError;   //TableOfZeean
    private String Flag;    //char1
    private String Guardar; //char1
    private List<TableOfZartAnaquel> ItDetalle;    //TableOfZartAnaquel
    private String Mueble;  //char10
    private String Tienda;  //char4

    /**
     * Declaración de constructor
     * @param articulo
     * @param ean
     * @param eanError
     * @param flag
     * @param guardar
     * @param itDetalle
     * @param mueble
     * @param tienda
     */

    public ZmpiArtAnaquel(String articulo, List<TableOfZeean> ean, List<TableOfZeean> eanError, String flag, String guardar, List<TableOfZartAnaquel> itDetalle, String mueble, String tienda) {
        Articulo = articulo;
        Ean = ean;
        EanError = eanError;
        Flag = flag;
        Guardar = guardar;
        ItDetalle = itDetalle;
        Mueble = mueble;
        Tienda = tienda;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getArticulo() {
        return Articulo;
    }

    public void setArticulo(String articulo) {
        Articulo = articulo;
    }

    public List<TableOfZeean> getEan() {
        return Ean;
    }

    public void setEan(List<TableOfZeean> ean) {
        Ean = ean;
    }

    public List<TableOfZeean> getEanError() {
        return EanError;
    }

    public void setEanError(List<TableOfZeean> eanError) {
        EanError = eanError;
    }

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public String getGuardar() {
        return Guardar;
    }

    public void setGuardar(String guardar) {
        Guardar = guardar;
    }

    public List<TableOfZartAnaquel> getItDetalle() {
        return ItDetalle;
    }

    public void setItDetalle(List<TableOfZartAnaquel> itDetalle) {
        ItDetalle = itDetalle;
    }

    public String getMueble() {
        return Mueble;
    }

    public void setMueble(String mueble) {
        Mueble = mueble;
    }

    public String getTienda() {
        return Tienda;
    }

    public void setTienda(String tienda) {
        Tienda = tienda;
    }
}
