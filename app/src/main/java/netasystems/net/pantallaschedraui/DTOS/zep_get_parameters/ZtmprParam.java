package netasystems.net.pantallaschedraui.DTOS.zep_get_parameters;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZtmprParam implements KvmSerializable {

    /**
     * Declaración de variables
     */
    private String Mandt;
    private String Zprog;
    private String Zobj1;
    private String Zobj2;
    private String Zdatfi;
    private String Zusr;
    private String Zfecre;
    private String Zfeact;


    public ZtmprParam() {
        this.Mandt = "";
        this.Zprog = "";
        this.Zobj1 = "";
        this.Zobj2 = "";
        this.Zdatfi = "";
        this.Zusr = "";
        this.Zfecre = "";
        this.Zfeact = "";
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getMandt() {
        return Mandt;
    }

    public void setMandt(String mandt) {
        Mandt = mandt;
    }

    public String getZprog() {
        return Zprog;
    }

    public void setZprog(String zprog) {
        Zprog = zprog;
    }

    public String getZobj1() {
        return Zobj1;
    }

    public void setZobj1(String zobj1) {
        Zobj1 = zobj1;
    }

    public String getZobj2() {
        return Zobj2;
    }

    public void setZobj2(String zobj2) {
        Zobj2 = zobj2;
    }

    public String getZdatfi() {
        return Zdatfi;
    }

    public void setZdatfi(String zdatfi) {
        Zdatfi = zdatfi;
    }

    public String getZusr() {
        return Zusr;
    }

    public void setZusr(String zusr) {
        Zusr = zusr;
    }

    public String getZfecre() {
        return Zfecre;
    }

    public void setZfecre(String zfecre) {
        Zfecre = zfecre;
    }

    public String getZfeact() {
        return Zfeact;
    }

    public void setZfeact(String zfeact) {
        Zfeact = zfeact;
    }






    @Override
    public Object getProperty(int index) {
        switch (index){
            case 0:
                return this.Mandt;
            case 1:
                return this.Zprog;
            case 2:
                return this.Zobj1;
            case 3:
                return this.Zobj2;
            case 4:
                return this.Zdatfi;
            case 5:
                return this.Zusr;
            case 6:
                return this.Zfecre;
            case 7:
                return this.Zfeact;
            default:
                break;
        }
        return null;
    }

    //private String Mandt;
    //private String Zprog;
    //private String Zobj1;
    //private String Zobj2;
    //private String Zdatfi;
    //private String Zusr;
    //private String Zfecre;
    //private String Zfeact;

    @Override
    public int getPropertyCount() {
        return 8;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index){
            case 0:
                this.Mandt = value.toString();
            case 1:
                this.Zprog = value.toString();
            case 2:
                this.Zobj1 = value.toString();
            case 3:
                this.Zobj2 = value.toString();
            case 4:
                this.Zdatfi = value.toString();
            case 5:
                this.Zusr = value.toString();
            case 6:
                this.Zfecre = value.toString();
            case 7:
                this.Zfeact = value.toString();
            default:
                break;
        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        switch (index){
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Mandt";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zprog";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zobj1";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zobj2";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zdatfi";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zusr";
                break;
            case 6:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zfecre";
                break;
            case 7:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zfeact";
                break;
            default:
                break;
        }


    }
}
