package netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiAhuecosTemporal {

    /**
     * Declaración de variables y listas
     */

    private List<netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal.TableOfZeean> TableOfZeean;
    private String GArticulo;
    private String GBorrar;
    private String GEan;
    private String GRecupera;
    private List<netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal.TableOfZtmmAhuecosTem> TableOfZtmmAhuecosTem;
    private String Mueble;
    private String Tienda;
    private String Usuario;

    /**
     * Declaración de construcor
     * @param tableOfZeean
     * @param GArticulo
     * @param GBorrar
     * @param GEan
     * @param GRecupera
     * @param tableOfZtmmAhuecosTem
     * @param mueble
     * @param tienda
     * @param usuario
     */

    public ZmpiAhuecosTemporal(List<netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal.TableOfZeean> tableOfZeean, String GArticulo, String GBorrar, String GEan, String GRecupera, List<netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal.TableOfZtmmAhuecosTem> tableOfZtmmAhuecosTem, String mueble, String tienda, String usuario) {
        TableOfZeean = tableOfZeean;
        this.GArticulo = GArticulo;
        this.GBorrar = GBorrar;
        this.GEan = GEan;
        this.GRecupera = GRecupera;
        TableOfZtmmAhuecosTem = tableOfZtmmAhuecosTem;
        Mueble = mueble;
        Tienda = tienda;
        Usuario = usuario;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal.TableOfZeean> getTableOfZeean() {
        return TableOfZeean;
    }

    public void setTableOfZeean(List<netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal.TableOfZeean> tableOfZeean) {
        TableOfZeean = tableOfZeean;
    }

    public String getGArticulo() {
        return GArticulo;
    }

    public void setGArticulo(String GArticulo) {
        this.GArticulo = GArticulo;
    }

    public String getGBorrar() {
        return GBorrar;
    }

    public void setGBorrar(String GBorrar) {
        this.GBorrar = GBorrar;
    }

    public String getGEan() {
        return GEan;
    }

    public void setGEan(String GEan) {
        this.GEan = GEan;
    }

    public String getGRecupera() {
        return GRecupera;
    }

    public void setGRecupera(String GRecupera) {
        this.GRecupera = GRecupera;
    }

    public List<netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal.TableOfZtmmAhuecosTem> getTableOfZtmmAhuecosTem() {
        return TableOfZtmmAhuecosTem;
    }

    public void setTableOfZtmmAhuecosTem(List<netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal.TableOfZtmmAhuecosTem> tableOfZtmmAhuecosTem) {
        TableOfZtmmAhuecosTem = tableOfZtmmAhuecosTem;
    }

    public String getMueble() {
        return Mueble;
    }

    public void setMueble(String mueble) {
        Mueble = mueble;
    }

    public String getTienda() {
        return Tienda;
    }

    public void setTienda(String tienda) {
        Tienda = tienda;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }
}
