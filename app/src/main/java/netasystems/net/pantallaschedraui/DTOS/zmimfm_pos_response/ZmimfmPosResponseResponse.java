package netasystems.net.pantallaschedraui.DTOS.zmimfm_pos_response;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmimfmPosResponseResponse {

    /**
     * Declaración de variables
     */

    private String EMensaje;

    /**
     * Declaración de constructor
     * @param EMensaje
     */

    public ZmimfmPosResponseResponse(String EMensaje) {
        this.EMensaje = EMensaje;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getEMensaje() {
        return EMensaje;
    }

    public void setEMensaje(String EMensaje) {
        this.EMensaje = EMensaje;
    }
}
