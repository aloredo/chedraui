package netasystems.net.pantallaschedraui.DTOS.zmpi_art_anaquel;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

class TableOfZartAnaquel {

    /**
     * Declaración de lista
     */

    private List<ZartAnaquel> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public TableOfZartAnaquel(List<ZartAnaquel> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */
    public List<ZartAnaquel> getItem() {
        return item;
    }

    public void setItem(List<ZartAnaquel> item) {
        this.item = item;
    }
}
