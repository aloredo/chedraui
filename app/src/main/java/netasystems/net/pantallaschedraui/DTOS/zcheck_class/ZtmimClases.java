package netasystems.net.pantallaschedraui.DTOS.zcheck_class;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class ZtmimClases {

    /**
     * Declaración de variables
     */

    private String Mandt;   //clnt3
    private String Class1;  //char18
    private String Class2;  //char18
    private String Class3;  //char18
    private String Class4;  //char18
    private String Class5;  //char18
    private String Desc1;   //char40
    private String Desc2;   //char40
    private String Desc3;   //char40
    private String Desc4;   //char40
    private String Desc5;   //char40

    /**
     * Declaración de constructor
     * @param mandt
     * @param class1
     * @param class2
     * @param class3
     * @param class4
     * @param class5
     * @param desc1
     * @param desc2
     * @param desc3
     * @param desc4
     * @param desc5
     */

    public ZtmimClases(String mandt, String class1, String class2, String class3, String class4, String class5, String desc1, String desc2, String desc3, String desc4, String desc5) {
        Mandt = mandt;
        Class1 = class1;
        Class2 = class2;
        Class3 = class3;
        Class4 = class4;
        Class5 = class5;
        Desc1 = desc1;
        Desc2 = desc2;
        Desc3 = desc3;
        Desc4 = desc4;
        Desc5 = desc5;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getMandt() {
        return Mandt;
    }

    public void setMandt(String mandt) {
        Mandt = mandt;
    }

    public String getClass1() {
        return Class1;
    }

    public void setClass1(String class1) {
        Class1 = class1;
    }

    public String getClass2() {
        return Class2;
    }

    public void setClass2(String class2) {
        Class2 = class2;
    }

    public String getClass3() {
        return Class3;
    }

    public void setClass3(String class3) {
        Class3 = class3;
    }

    public String getClass4() {
        return Class4;
    }

    public void setClass4(String class4) {
        Class4 = class4;
    }

    public String getClass5() {
        return Class5;
    }

    public void setClass5(String class5) {
        Class5 = class5;
    }

    public String getDesc1() {
        return Desc1;
    }

    public void setDesc1(String desc1) {
        Desc1 = desc1;
    }

    public String getDesc2() {
        return Desc2;
    }

    public void setDesc2(String desc2) {
        Desc2 = desc2;
    }

    public String getDesc3() {
        return Desc3;
    }

    public void setDesc3(String desc3) {
        Desc3 = desc3;
    }

    public String getDesc4() {
        return Desc4;
    }

    public void setDesc4(String desc4) {
        Desc4 = desc4;
    }

    public String getDesc5() {
        return Desc5;
    }

    public void setDesc5(String desc5) {
        Desc5 = desc5;
    }
}
