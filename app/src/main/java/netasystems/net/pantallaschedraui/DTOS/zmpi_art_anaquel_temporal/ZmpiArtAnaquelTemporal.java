package netasystems.net.pantallaschedraui.DTOS.zmpi_art_anaquel_temporal;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiArtAnaquelTemporal {

    /**
     * Declaración de variables y Listas
     */

    private List<TableOfZeean> Ean;
    private String GArticulo;
    private String GBorrar;
    private String GEan;
    private String GRecupera;
    private List<TableOfZartAnaquelTem> ItRecupera;
    private String Mueble;
    private String Tienda;
    private String Usuario;

    /**
     * Declaración de constructor
     * @param ean
     * @param GArticulo
     * @param GBorrar
     * @param GEan
     * @param GRecupera
     * @param itRecupera
     * @param mueble
     * @param tienda
     * @param usuario
     */

    public ZmpiArtAnaquelTemporal(List<TableOfZeean> ean, String GArticulo, String GBorrar, String GEan, String GRecupera, List<TableOfZartAnaquelTem> itRecupera, String mueble, String tienda, String usuario) {
        Ean = ean;
        this.GArticulo = GArticulo;
        this.GBorrar = GBorrar;
        this.GEan = GEan;
        this.GRecupera = GRecupera;
        ItRecupera = itRecupera;
        Mueble = mueble;
        Tienda = tienda;
        Usuario = usuario;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZeean> getEan() {
        return Ean;
    }

    public void setEan(List<TableOfZeean> ean) {
        Ean = ean;
    }

    public String getGArticulo() {
        return GArticulo;
    }

    public void setGArticulo(String GArticulo) {
        this.GArticulo = GArticulo;
    }

    public String getGBorrar() {
        return GBorrar;
    }

    public void setGBorrar(String GBorrar) {
        this.GBorrar = GBorrar;
    }

    public String getGEan() {
        return GEan;
    }

    public void setGEan(String GEan) {
        this.GEan = GEan;
    }

    public String getGRecupera() {
        return GRecupera;
    }

    public void setGRecupera(String GRecupera) {
        this.GRecupera = GRecupera;
    }

    public List<TableOfZartAnaquelTem> getItRecupera() {
        return ItRecupera;
    }

    public void setItRecupera(List<TableOfZartAnaquelTem> itRecupera) {
        ItRecupera = itRecupera;
    }

    public String getMueble() {
        return Mueble;
    }

    public void setMueble(String mueble) {
        Mueble = mueble;
    }

    public String getTienda() {
        return Tienda;
    }

    public void setTienda(String tienda) {
        Tienda = tienda;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }
}
