package netasystems.net.pantallaschedraui.DTOS.zfmpp_merma_carnes;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZsppEtiquetaCarnes {

    /**
     * Declaración de variable
     */

    private String Etiqueta;

    /**
     * Declaración de constructor
     * @param etiqueta
     */

    public ZsppEtiquetaCarnes(String etiqueta) {
        Etiqueta = etiqueta;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getEtiqueta() {
        return Etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        Etiqueta = etiqueta;
    }
}
