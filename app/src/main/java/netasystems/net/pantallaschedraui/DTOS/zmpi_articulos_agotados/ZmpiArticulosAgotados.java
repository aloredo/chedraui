package netasystems.net.pantallaschedraui.DTOS.zmpi_articulos_agotados;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmpiArticulosAgotados {

    /**
     * Declaración variables y lista
     */

    private String Mueble;
    private List<netasystems.net.pantallaschedraui.DTOS.zmpi_articulos_agotados.TableOfZsmmEtiqFaltHuecos> TableOfZsmmEtiqFaltHuecos;
    private String Tienda;

    /**
     * Declaración de constructor
     * @param mueble
     * @param tableOfZsmmEtiqFaltHuecos
     * @param tienda
     */

    public ZmpiArticulosAgotados(String mueble, List<netasystems.net.pantallaschedraui.DTOS.zmpi_articulos_agotados.TableOfZsmmEtiqFaltHuecos> tableOfZsmmEtiqFaltHuecos, String tienda) {
        Mueble = mueble;
        TableOfZsmmEtiqFaltHuecos = tableOfZsmmEtiqFaltHuecos;
        Tienda = tienda;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getMueble() {
        return Mueble;
    }

    public void setMueble(String mueble) {
        Mueble = mueble;
    }

    public List<netasystems.net.pantallaschedraui.DTOS.zmpi_articulos_agotados.TableOfZsmmEtiqFaltHuecos> getTableOfZsmmEtiqFaltHuecos() {
        return TableOfZsmmEtiqFaltHuecos;
    }

    public void setTableOfZsmmEtiqFaltHuecos(List<netasystems.net.pantallaschedraui.DTOS.zmpi_articulos_agotados.TableOfZsmmEtiqFaltHuecos> tableOfZsmmEtiqFaltHuecos) {
        TableOfZsmmEtiqFaltHuecos = tableOfZsmmEtiqFaltHuecos;
    }

    public String getTienda() {
        return Tienda;
    }

    public void setTienda(String tienda) {
        Tienda = tienda;
    }
}
