package netasystems.net.pantallaschedraui.DTOS.zmimfm_det_store;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmimfmDetStoreResponse {

    /**
     * Declaración de variable
     */

    private String Ztienda;

    /**
     * Declaración de constructor
     * @param ztienda
     */

    public ZmimfmDetStoreResponse(String ztienda) {
        Ztienda = ztienda;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getZtienda() {
        return Ztienda;
    }

    public void setZtienda(String ztienda) {
        Ztienda = ztienda;
    }
}
