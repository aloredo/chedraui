package netasystems.net.pantallaschedraui.DTOS.zfmpp_notif_carnes;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZfmppNotifCarnesV2 {

    /**
     * Declaración de lista
     */

    private List<TableOfZsppEtiquetaCarnes> Etiqueta;

    /**
     * Declaración de constructor
     * @param etiqueta
     */

    public ZfmppNotifCarnesV2(List<TableOfZsppEtiquetaCarnes> etiqueta) {
        Etiqueta = etiqueta;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<TableOfZsppEtiquetaCarnes> getEtiqueta() {
        return Etiqueta;
    }

    public void setEtiqueta(List<TableOfZsppEtiquetaCarnes> etiqueta) {
        Etiqueta = etiqueta;
    }
}
