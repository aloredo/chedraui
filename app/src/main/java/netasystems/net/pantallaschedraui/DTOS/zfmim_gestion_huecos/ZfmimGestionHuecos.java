package netasystems.net.pantallaschedraui.DTOS.zfmim_gestion_huecos;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZfmimGestionHuecos {

    /**
     * Declaración de lista y variable
     */

    private String IShelfId;
    private List<TableOfZsppArticulo> TScanArticulos;

    /**
     * Declaración de constructor
     * @param IShelfId
     * @param TScanArticulos
     */

    public ZfmimGestionHuecos(String IShelfId, List<TableOfZsppArticulo> TScanArticulos) {
        this.IShelfId = IShelfId;
        this.TScanArticulos = TScanArticulos;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getIShelfId() {
        return IShelfId;
    }

    public void setIShelfId(String IShelfId) {
        this.IShelfId = IShelfId;
    }

    public List<TableOfZsppArticulo> getTScanArticulos() {
        return TScanArticulos;
    }

    public void setTScanArticulos(List<TableOfZsppArticulo> TScanArticulos) {
        this.TScanArticulos = TScanArticulos;
    }
}
