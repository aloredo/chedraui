package netasystems.net.pantallaschedraui.DTOS.zcheck_shelf;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZcheckShelfResponse {

    /**
     * Declaración de listas
     */

    private List<Bapiret1> ERet;    //Bapiret1
    private List<ZttMimCsmue> TtCsmue;   //ZttMimCsmue

    /**
     * Declaración de constructor
     * @param ERet
     * @param ttCsmue
     */

    public ZcheckShelfResponse(List<Bapiret1> ERet, List<ZttMimCsmue> ttCsmue) {
        this.ERet = ERet;
        TtCsmue = ttCsmue;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<Bapiret1> getERet() {
        return ERet;
    }

    public void setERet(List<Bapiret1> ERet) {
        this.ERet = ERet;
    }

    public List<ZttMimCsmue> getTtCsmue() {
        return TtCsmue;
    }

    public void setTtCsmue(List<ZttMimCsmue> ttCsmue) {
        TtCsmue = ttCsmue;
    }

    }
