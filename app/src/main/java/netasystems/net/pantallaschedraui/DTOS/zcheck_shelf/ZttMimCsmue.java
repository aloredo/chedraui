package netasystems.net.pantallaschedraui.DTOS.zcheck_shelf;

import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class ZttMimCsmue {

    /**
     * Declaración de variable
     */

    private List<Ztmimcsmue> item;

    /**
     * Declaració de constructor
     * @param item
     */

    public ZttMimCsmue(List<Ztmimcsmue> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<Ztmimcsmue> getItem() {
        return item;
    }

    public void setItem(List<Ztmimcsmue> item) {
        this.item = item;
    }
}
