package netasystems.net.pantallaschedraui.DTOS.zcheck_shelf;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class Ztmimcsmue {

    /**
     * Declaración de variables
     */

    private String Mandt;   //clnt3
    private String Werks;
    private String Lgort;
    private String ZshelfId;
    private String Zmueanc; //numeric4
    private String Zmuelar; //numeric4
    private String Zmuealt; //numeric4
    private String Zmuentr; //numeric10
    private String Zmuenni; //numeric10
    private String Zmuench; //numeric4
    private String Zmuegan; //numeric4
    private String Zmueach; //numeric4
    private String Zmuefch; //numeric4
    private String Zmueech; //numeric4
    private String Zmueal1;
    private String Zmueal2;
    private double Zmuenu1; //decimal12.3
    private double Zmuenu2; //decimal12.3
    private String Zintactix;
    private String Zmueuser;
    private String Zmuedate;    //date10
    private String Zmuetime;    //time

    /**
     * Declaración de constructor
     * @param mandt
     * @param werks
     * @param lgort
     * @param zshelfId
     * @param zmueanc
     * @param zmuelar
     * @param zmuealt
     * @param zmuentr
     * @param zmuenni
     * @param zmuench
     * @param zmuegan
     * @param zmueach
     * @param zmuefch
     * @param zmueech
     * @param zmueal1
     * @param zmueal2
     * @param zmuenu1
     * @param zmuenu2
     * @param zintactix
     * @param zmueuser
     * @param zmuedate
     * @param zmuetime
     */

    public Ztmimcsmue(String mandt, String werks, String lgort, String zshelfId, String zmueanc, String zmuelar, String zmuealt, String zmuentr, String zmuenni, String zmuench, String zmuegan, String zmueach, String zmuefch, String zmueech, String zmueal1, String zmueal2, double zmuenu1, double zmuenu2, String zintactix, String zmueuser, String zmuedate, String zmuetime) {
        Mandt = mandt;
        Werks = werks;
        Lgort = lgort;
        ZshelfId = zshelfId;
        Zmueanc = zmueanc;
        Zmuelar = zmuelar;
        Zmuealt = zmuealt;
        Zmuentr = zmuentr;
        Zmuenni = zmuenni;
        Zmuench = zmuench;
        Zmuegan = zmuegan;
        Zmueach = zmueach;
        Zmuefch = zmuefch;
        Zmueech = zmueech;
        Zmueal1 = zmueal1;
        Zmueal2 = zmueal2;
        Zmuenu1 = zmuenu1;
        Zmuenu2 = zmuenu2;
        Zintactix = zintactix;
        Zmueuser = zmueuser;
        Zmuedate = zmuedate;
        Zmuetime = zmuetime;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getMandt() {
        return Mandt;
    }

    public void setMandt(String mandt) {
        Mandt = mandt;
    }

    public String getWerks() {
        return Werks;
    }

    public void setWerks(String werks) {
        Werks = werks;
    }

    public String getLgort() {
        return Lgort;
    }

    public void setLgort(String lgort) {
        Lgort = lgort;
    }

    public String getZshelfId() {
        return ZshelfId;
    }

    public void setZshelfId(String zshelfId) {
        ZshelfId = zshelfId;
    }

    public String getZmueanc() {
        return Zmueanc;
    }

    public void setZmueanc(String zmueanc) {
        Zmueanc = zmueanc;
    }

    public String getZmuelar() {
        return Zmuelar;
    }

    public void setZmuelar(String zmuelar) {
        Zmuelar = zmuelar;
    }

    public String getZmuealt() {
        return Zmuealt;
    }

    public void setZmuealt(String zmuealt) {
        Zmuealt = zmuealt;
    }

    public String getZmuentr() {
        return Zmuentr;
    }

    public void setZmuentr(String zmuentr) {
        Zmuentr = zmuentr;
    }

    public String getZmuenni() {
        return Zmuenni;
    }

    public void setZmuenni(String zmuenni) {
        Zmuenni = zmuenni;
    }

    public String getZmuench() {
        return Zmuench;
    }

    public void setZmuench(String zmuench) {
        Zmuench = zmuench;
    }

    public String getZmuegan() {
        return Zmuegan;
    }

    public void setZmuegan(String zmuegan) {
        Zmuegan = zmuegan;
    }

    public String getZmueach() {
        return Zmueach;
    }

    public void setZmueach(String zmueach) {
        Zmueach = zmueach;
    }

    public String getZmuefch() {
        return Zmuefch;
    }

    public void setZmuefch(String zmuefch) {
        Zmuefch = zmuefch;
    }

    public String getZmueech() {
        return Zmueech;
    }

    public void setZmueech(String zmueech) {
        Zmueech = zmueech;
    }

    public String getZmueal1() {
        return Zmueal1;
    }

    public void setZmueal1(String zmueal1) {
        Zmueal1 = zmueal1;
    }

    public String getZmueal2() {
        return Zmueal2;
    }

    public void setZmueal2(String zmueal2) {
        Zmueal2 = zmueal2;
    }

    public double getZmuenu1() {
        return Zmuenu1;
    }

    public void setZmuenu1(double zmuenu1) {
        Zmuenu1 = zmuenu1;
    }

    public double getZmuenu2() {
        return Zmuenu2;
    }

    public void setZmuenu2(double zmuenu2) {
        Zmuenu2 = zmuenu2;
    }

    public String getZintactix() {
        return Zintactix;
    }

    public void setZintactix(String zintactix) {
        Zintactix = zintactix;
    }

    public String getZmueuser() {
        return Zmueuser;
    }

    public void setZmueuser(String zmueuser) {
        Zmueuser = zmueuser;
    }

    public String getZmuedate() {
        return Zmuedate;
    }

    public void setZmuedate(String zmuedate) {
        Zmuedate = zmuedate;
    }

    public String getZmuetime() {
        return Zmuetime;
    }

    public void setZmuetime(String zmuetime) {
        Zmuetime = zmuetime;
    }

}
