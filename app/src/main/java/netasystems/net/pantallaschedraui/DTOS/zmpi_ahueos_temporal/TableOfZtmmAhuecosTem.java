package netasystems.net.pantallaschedraui.DTOS.zmpi_ahueos_temporal;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class TableOfZtmmAhuecosTem {

    /**
     * Declaración de lista
     */

    private List<ZtmmAhuecosTem> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public TableOfZtmmAhuecosTem(List<ZtmmAhuecosTem> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<ZtmmAhuecosTem> getItem() {
        return item;
    }

    public void setItem(List<ZtmmAhuecosTem> item) {
        this.item = item;
    }
}
