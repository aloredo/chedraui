package netasystems.net.pantallaschedraui.DTOS.z_confirm_price;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZConfirmPrice implements KvmSerializable {

    /**
     * Declaración de variables y listas
     */

    private String IAuditoria;
    private String IConfirArt;
    private String IEan11;
    private String IImprimir;
    private String IMprok;
    private String ISinCensoDetalle;
    private String ISinCensoTotal;
    private String IZebtyp;
    private String IZfaltanteAnaquel;
    private String IZmaterial;
    private String IZprecio;
    private List<Zreturndata> IZreturndata;
    private String IZshelfId;
    private List<TableOfZpreciosPendtes> ItPreciosPendtes;

    public ZConfirmPrice(){
        this.IAuditoria = "";
        this.IConfirArt = "";
        this.IEan11 = "";
        this.IImprimir = "";
        this.IMprok = "";
        this.ISinCensoDetalle = "";
        this.ISinCensoTotal = "";
        this.IZebtyp = "";
        this.IZfaltanteAnaquel = "";
        this.IZmaterial = "";
        this.IZprecio = "";
        this.IZshelfId = "";
    }

    /**
     * Declaración de constructor
     * @param IAuditoria
     * @param IConfirArt
     * @param IEan11
     * @param IImprimir
     * @param IMprok
     * @param ISinCensoDetalle
     * @param ISinCensoTotal
     * @param IZebtyp
     * @param IZfaltanteAnaquel
     * @param IZmaterial
     * @param IZprecio
     * @param IZreturndata
     * @param IZshelfId
     * @param itPreciosPendtes
     */

    public ZConfirmPrice(String IAuditoria, String IConfirArt, String IEan11, String IImprimir, String IMprok, String ISinCensoDetalle, String ISinCensoTotal, String IZebtyp, String IZfaltanteAnaquel, String IZmaterial, String IZprecio, List<Zreturndata> IZreturndata, String IZshelfId, List<TableOfZpreciosPendtes> itPreciosPendtes) {
        this.IAuditoria = IAuditoria;
        this.IConfirArt = IConfirArt;
        this.IEan11 = IEan11;
        this.IImprimir = IImprimir;
        this.IMprok = IMprok;
        this.ISinCensoDetalle = ISinCensoDetalle;
        this.ISinCensoTotal = ISinCensoTotal;
        this.IZebtyp = IZebtyp;
        this.IZfaltanteAnaquel = IZfaltanteAnaquel;
        this.IZmaterial = IZmaterial;
        this.IZprecio = IZprecio;
        this.IZreturndata = IZreturndata;
        this.IZshelfId = IZshelfId;
        ItPreciosPendtes = itPreciosPendtes;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getIAuditoria() {
        return IAuditoria;
    }

    public void setIAuditoria(String IAuditoria) {
        this.IAuditoria = IAuditoria;
    }

    public String getIConfirArt() {
        return IConfirArt;
    }

    public void setIConfirArt(String IConfirArt) {
        this.IConfirArt = IConfirArt;
    }

    public String getIEan11() {
        return IEan11;
    }

    public void setIEan11(String IEan11) {
        this.IEan11 = IEan11;
    }

    public String getIImprimir() {
        return IImprimir;
    }

    public void setIImprimir(String IImprimir) {
        this.IImprimir = IImprimir;
    }

    public String getIMprok() {
        return IMprok;
    }

    public void setIMprok(String IMprok) {
        this.IMprok = IMprok;
    }

    public String getISinCensoDetalle() {
        return ISinCensoDetalle;
    }

    public void setISinCensoDetalle(String ISinCensoDetalle) {
        this.ISinCensoDetalle = ISinCensoDetalle;
    }

    public String getISinCensoTotal() {
        return ISinCensoTotal;
    }

    public void setISinCensoTotal(String ISinCensoTotal) {
        this.ISinCensoTotal = ISinCensoTotal;
    }

    public String getIZebtyp() {
        return IZebtyp;
    }

    public void setIZebtyp(String IZebtyp) {
        this.IZebtyp = IZebtyp;
    }

    public String getIZfaltanteAnaquel() {
        return IZfaltanteAnaquel;
    }

    public void setIZfaltanteAnaquel(String IZfaltanteAnaquel) {
        this.IZfaltanteAnaquel = IZfaltanteAnaquel;
    }

    public String getIZmaterial() {
        return IZmaterial;
    }

    public void setIZmaterial(String IZmaterial) {
        this.IZmaterial = IZmaterial;
    }

    public String getIZprecio() {
        return IZprecio;
    }

    public void setIZprecio(String IZprecio) {
        this.IZprecio = IZprecio;
    }

    public List<Zreturndata> getIZreturndata() {
        return IZreturndata;
    }

    public void setIZreturndata(List<Zreturndata> IZreturndata) {
        this.IZreturndata = IZreturndata;
    }

    public String getIZshelfId() {
        return IZshelfId;
    }

    public void setIZshelfId(String IZshelfId) {
        this.IZshelfId = IZshelfId;
    }

    public List<TableOfZpreciosPendtes> getItPreciosPendtes() {
        return ItPreciosPendtes;
    }

    public void setItPreciosPendtes(List<TableOfZpreciosPendtes> itPreciosPendtes) {
        ItPreciosPendtes = itPreciosPendtes;
    }

    @Override
    public Object getProperty(int index) {
        switch (index) {
            case 0:
                return this.IAuditoria;
            case 1:
                return this.IConfirArt;
            case 2:
                return this.IEan11;
            case 3:
                return this.IImprimir;
            case 4:
                return this.IMprok;
            case 5:
                return this.ISinCensoDetalle;
            case 6:
                return this.ISinCensoTotal;
            case 7:
                return this.IZebtyp;
            case 8:
                return this.IZfaltanteAnaquel;
            case 9:
                return this.IZmaterial;
            case 10:
                return this.IZprecio;
            case 11:
                return this.IZshelfId;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index) {
            case 0:
                this.IAuditoria = value.toString();
            case 1:
                this.IConfirArt = value.toString();
            case 2:
                this.IEan11 = value.toString();
            case 3:
                this.IImprimir = value.toString();
            case 4:
                this.IMprok = value.toString();
            case 5:
                this.ISinCensoDetalle = value.toString();
            case 6:
                this.ISinCensoTotal = value.toString();
            case 7:
                this.IZebtyp = value.toString();
            case 8:
                this.IZfaltanteAnaquel = value.toString();
            case 9:
                this.IZmaterial = value.toString();
            case 10:
                this.IZprecio = value.toString();
            case 11:
                this.IZshelfId = value.toString();
            default:
                break;

    }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable hashtable, PropertyInfo info) {
        switch (index) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IAuditoria";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IConfirArt";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IEan11";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IImprimir";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IMprok";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ISinCensoDetalle";
                break;
            case 6:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ISinCensoTotal";
                break;
            case 7:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IZebtyp";
                break;
            case 8:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IZfaltanteAnaquel";
                break;
            case 9:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IZmaterial";
                break;
            case 10:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IZprecio";
                break;
            case 11:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "IZshelfId";
                break;
            default:
                break;
        }

    }

}
