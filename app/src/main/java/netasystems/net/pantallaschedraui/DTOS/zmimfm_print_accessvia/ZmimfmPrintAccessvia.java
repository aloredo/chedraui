package netasystems.net.pantallaschedraui.DTOS.zmimfm_print_accessvia;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZmimfmPrintAccessvia {

    /**
     * Declaración de variables
     */

    private String ICodPosMatnr;
    private String ICodTipoRotulo;
    private String IEan11;
    private String IMatnrAndPrice;
    private String IWerks;

    /**
     * Declaración de constructor
     * @param ICodPosMatnr
     * @param ICodTipoRotulo
     * @param IEan11
     * @param IMatnrAndPrice
     * @param IWerks
     */

    public ZmimfmPrintAccessvia(String ICodPosMatnr, String ICodTipoRotulo, String IEan11, String IMatnrAndPrice, String IWerks) {
        this.ICodPosMatnr = ICodPosMatnr;
        this.ICodTipoRotulo = ICodTipoRotulo;
        this.IEan11 = IEan11;
        this.IMatnrAndPrice = IMatnrAndPrice;
        this.IWerks = IWerks;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getICodPosMatnr() {
        return ICodPosMatnr;
    }

    public void setICodPosMatnr(String ICodPosMatnr) {
        this.ICodPosMatnr = ICodPosMatnr;
    }

    public String getICodTipoRotulo() {
        return ICodTipoRotulo;
    }

    public void setICodTipoRotulo(String ICodTipoRotulo) {
        this.ICodTipoRotulo = ICodTipoRotulo;
    }

    public String getIEan11() {
        return IEan11;
    }

    public void setIEan11(String IEan11) {
        this.IEan11 = IEan11;
    }

    public String getIMatnrAndPrice() {
        return IMatnrAndPrice;
    }

    public void setIMatnrAndPrice(String IMatnrAndPrice) {
        this.IMatnrAndPrice = IMatnrAndPrice;
    }

    public String getIWerks() {
        return IWerks;
    }

    public void setIWerks(String IWerks) {
        this.IWerks = IWerks;
    }
}
