package netasystems.net.pantallaschedraui.DTOS.ZUSUARIOSAUDITService;

import java.util.List;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class ZusuariosauditResponse {

    /**
     * Declaración de lista
     */

    private List<Ztusuarioaudit> TUsuarios;

    /**
     * Declaración de constructor
     * @param TUsuarios
     */

    public ZusuariosauditResponse(List<Ztusuarioaudit> TUsuarios) {
        this.TUsuarios = TUsuarios;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<Ztusuarioaudit> getTUsuarios() {
        return TUsuarios;
    }

    public void setTUsuarios(List<Ztusuarioaudit> TUsuarios) {
        this.TUsuarios = TUsuarios;
    }
}
