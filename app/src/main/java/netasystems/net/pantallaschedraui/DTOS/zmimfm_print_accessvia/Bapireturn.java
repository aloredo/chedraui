package netasystems.net.pantallaschedraui.DTOS.zmimfm_print_accessvia;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

class Bapireturn {

    /**
     * Declaración de variables
     */

    private String Type;
    private String Code;
    private String Message;
    private String LogNo;
    private String LogMsgNo;
    private String MessageV1;
    private String MessageV2;
    private String MessageV3;
    private String MessageV4;

    /**
     * Declaración de constructor
     * @param type
     * @param code
     * @param message
     * @param logNo
     * @param logMsgNo
     * @param messageV1
     * @param messageV2
     * @param messageV3
     * @param messageV4
     */

    public Bapireturn(String type, String code, String message, String logNo, String logMsgNo, String messageV1, String messageV2, String messageV3, String messageV4) {
        Type = type;
        Code = code;
        Message = message;
        LogNo = logNo;
        LogMsgNo = logMsgNo;
        MessageV1 = messageV1;
        MessageV2 = messageV2;
        MessageV3 = messageV3;
        MessageV4 = messageV4;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getLogNo() {
        return LogNo;
    }

    public void setLogNo(String logNo) {
        LogNo = logNo;
    }

    public String getLogMsgNo() {
        return LogMsgNo;
    }

    public void setLogMsgNo(String logMsgNo) {
        LogMsgNo = logMsgNo;
    }

    public String getMessageV1() {
        return MessageV1;
    }

    public void setMessageV1(String messageV1) {
        MessageV1 = messageV1;
    }

    public String getMessageV2() {
        return MessageV2;
    }

    public void setMessageV2(String messageV2) {
        MessageV2 = messageV2;
    }

    public String getMessageV3() {
        return MessageV3;
    }

    public void setMessageV3(String messageV3) {
        MessageV3 = messageV3;
    }

    public String getMessageV4() {
        return MessageV4;
    }

    public void setMessageV4(String messageV4) {
        MessageV4 = messageV4;
    }
}
