package netasystems.net.pantallaschedraui.DTOS;

public class PerfilAutenticacion {

    public static String Usuario;
    /// <summary>
    /// Obtiene o establece la contraseña del usuario.
    /// </summary>
    /// <value>
    /// La contraseña del usuario.
    /// </value>
    public static String Pwd;
    /// <summary>
    /// Obtiene o estable el código de la tienda sobre la cual se realiza la auditoría de precios.
    /// </summary>
    /// <value>
    ///.El código de la tienda.
    /// </value>
    public static String Tienda;
    /// <summary>
    /// Obtiene o establece si tiene accesso a la operaciónes/tareas de auditoria de precios.
    /// </summary>
    /// <value>
    /// "X" si las opciones de auditoría de precios esta habilitadas. "" o cualquier valor de lo contrario.
    /// </value>
    public static String Auditoria;
    /// <summary>
    /// Obtiene o establece si tiene accesso a la operaciónes/tareas de generación masiva de etiquetas.
    /// </summary>
    /// <value>
    /// "X" si las opciones de generación masiva de etiquetas esta habilitadas. "" o cualquier valor de lo contrario.
    /// </value>
    public static String EtiquetaMasiva;
    /// <summary>
    /// Obtiene o establece si tiene accesso a la operaciónes/tareas de confirmación de precios.
    /// </summary>
    /// <value>
    /// "X" si las opciones de confirmación de precios esta habilitadas. "" o cualquier valor de lo contrario.
    /// </value>
    public static String Confirmacion;
    /// <sumary>
    /// Obtiene o establece el modelo de la impresora.
    /// </sumary>
    public static String ModeloImpresora;
    ///<sumary>
    /// Ontiene o estable la dirección de la impresora wi-fi.
    /// </sumary>
    public static String DireccionIp;
    /// <summary>
    /// Obtiene o establece el mueble en el cual se encuentra trabajando el usuario en el faltante de anquel.
    /// </summary>
    public static String MuebleFaltanteAnaquel;
    ///<sumary>
    /// Obtiene o establece el parámetro del timeout para los web services.
    ///</sumary>
    public static int TimeOut;
    /// <summary>
    /// Obtiene o establece el mueble en el cual se encuentra trabajando el usuario en la auditoría de huecos.
    /// </summary>
    public static String MuebleHuecos;
    /// <summary>
    /// Obtiene o establece el usuario en el cual se encuentra trabajando en la auditoría de huecos.
    /// </summary>
    public static String UsuarioHuecos;
    /// <summary>
    /// Obtiene o establece si el usuario tiene acceso a la opción de escaneos.
    /// </summary>
    public static String Escaneos;
    /// <summary>
    /// Obtiene o establece si el usuario tiene acceso a la opción de faltante de anaquel.
    /// </summary>
    public static String FaltanteAnaquel;
    /// <summary>
    /// Obtiene o establece si el usuario tiene acceso a la opción de faltante de anaquel bodega.
    /// </summary>
    public static String FaltanteAnaquelBodega;
    /// <summary>
    /// Obtiene o establece si el usuario tiene acceso a la opción de notificación de charolas de carne.
    /// </summary>
    public static String Notificacion;
    /// <summary>
    /// Obtiene o establece si el usuario tiene acceso a la opción de escaneo de merma de charolas de carne.
    /// </summary>
    public static String EscaneoMerma;
    /// <summary>
    /// Obtiene o establece si el usuario tiene acceso a la opción de escaneo de huecos.
    /// </summary>
    public static String Huecos;


    public static String getUsuario() {
        return Usuario;
    }

    public static void setUsuario(String usuario) {
        Usuario = usuario;
    }

    public static String getPwd() {
        return Pwd;
    }

    public static void setPwd(String pwd) {
        Pwd = pwd;
    }

    public static String getTienda() {
        return Tienda;
    }

    public static void setTienda(String tienda) {
        Tienda = tienda;
    }

    public static String getAuditoria() {
        return Auditoria;
    }

    public static void setAuditoria(String auditoria) {
        Auditoria = auditoria;
    }

    public static String getEtiquetaMasiva() {
        return EtiquetaMasiva;
    }

    public static void setEtiquetaMasiva(String etiquetaMasiva) {
        EtiquetaMasiva = etiquetaMasiva;
    }

    public static String getConfirmacion() {
        return Confirmacion;
    }

    public static void setConfirmacion(String confirmacion) {
        Confirmacion = confirmacion;
    }

    public static String getModeloImpresora() {
        return ModeloImpresora;
    }

    public static void setModeloImpresora(String modeloImpresora) {
        ModeloImpresora = modeloImpresora;
    }

    public static String getDireccionIp() {
        return DireccionIp;
    }

    public static void setDireccionIp(String direccionIp) {
        DireccionIp = direccionIp;
    }

    public static String getMuebleFaltanteAnaquel() {
        return MuebleFaltanteAnaquel;
    }

    public static void setMuebleFaltanteAnaquel(String muebleFaltanteAnaquel) {
        MuebleFaltanteAnaquel = muebleFaltanteAnaquel;
    }

    public static int getTimeOut() {
        return TimeOut;
    }

    public static void setTimeOut(int timeOut) {
        TimeOut = timeOut;
    }

    public static String getMuebleHuecos() {
        return MuebleHuecos;
    }

    public static void setMuebleHuecos(String muebleHuecos) {
        MuebleHuecos = muebleHuecos;
    }

    public static String getUsuarioHuecos() {
        return UsuarioHuecos;
    }

    public static void setUsuarioHuecos(String usuarioHuecos) {
        UsuarioHuecos = usuarioHuecos;
    }

    public static String getEscaneos() {
        return Escaneos;
    }

    public static void setEscaneos(String escaneos) {
        Escaneos = escaneos;
    }

    public static String getFaltanteAnaquel() {
        return FaltanteAnaquel;
    }

    public static void setFaltanteAnaquel(String faltanteAnaquel) {
        FaltanteAnaquel = faltanteAnaquel;
    }

    public static String getFaltanteAnaquelBodega() {
        return FaltanteAnaquelBodega;
    }

    public static void setFaltanteAnaquelBodega(String faltanteAnaquelBodega) {
        FaltanteAnaquelBodega = faltanteAnaquelBodega;
    }

    public static String getNotificacion() {
        return Notificacion;
    }

    public static void setNotificacion(String notificacion) {
        Notificacion = notificacion;
    }

    public static String getEscaneoMerma() {
        return EscaneoMerma;
    }

    public static void setEscaneoMerma(String escaneoMerma) {
        EscaneoMerma = escaneoMerma;
    }

    public static String getHuecos() {
        return Huecos;
    }

    public static void setHuecos(String huecos) {
        Huecos = huecos;
    }
}
