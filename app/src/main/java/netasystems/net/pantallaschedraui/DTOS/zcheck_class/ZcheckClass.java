package netasystems.net.pantallaschedraui.DTOS.zcheck_class;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZcheckClass {

    /**
     * Declaración de variable
     */

    private String Clas;

    /**
     * Declaración de constructor
     * @param clas
     */

    public ZcheckClass(String clas) {
        Clas = clas;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getClas() {
        return Clas;
    }

    public void setClas(String clas) {
        Clas = clas;
    }
}
