package netasystems.net.pantallaschedraui.DTOS.z_confirm_price;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;


/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class Zreturndata implements KvmSerializable {

    /**
     * Declaración de variables
     */

    private String Ztype;
    private String Zid;
    private String Znumber;
    private String Zmessage;
    private String Zmaterial;
    private String ZmaterialDesc;
    private String Zean;
    private double Zprecio;
    private String ZimprimirEtiqueta;
    private String Zdate;
    private String ZdeSdprIpadr1;
    private String ZpuertoCom1;
    private String ZdeSdprIpadr2;
    private String ZpuertoCom2;
    private String ZCodBarraCom;
    private String ZactualizarPos;
    private String Ziva;
    private String ZclaveSkuKilo;
    private String Zieps;
    private String ZimptoEsp;
    private String Zlifnr;
    private String Zdeptoprivate;
    private String Zsubdepto;
    private String Zclass;
    private String Zsubclass;
    private String ZbrandId;
    private String ZtipoProm;
    private String ZplantillaPromo;
    private double ZdifPrecio;
    private double Zcaducidad;
    private String ZtipoEtiqueta;
    private String Zreceta;

    public Zreturndata() {
        this.Ztype = "";
        this.Zid = "";
        this.Znumber = "";
        this.Zmessage = "";
        this.Zmaterial = "";
        this.ZmaterialDesc = "";
        this.Zean = "";
        this.Zprecio = Double.parseDouble("");
        this.ZimprimirEtiqueta = "";
        this.Zdate = "";
        this.ZdeSdprIpadr1 = "";
        this.ZpuertoCom1 = "";
        this.ZdeSdprIpadr2 = "";
        this.ZpuertoCom2 = "";
        this.ZCodBarraCom = "";
        this.ZactualizarPos = "";
        this.Ziva = "";
        this.ZclaveSkuKilo = "";
        this.Zieps = "";
        this.ZimptoEsp = "";
        this.Zlifnr = "";
        this.Zdeptoprivate = "";
        this.Zsubdepto = "";
        this.Zclass = "";
        this.Zsubclass = "";
        this.ZbrandId = "";
        this.ZtipoProm = "";
        this.ZplantillaPromo = "";
        this.ZdifPrecio = Double.parseDouble("");
        this.Zcaducidad = Double.parseDouble("");
        this.ZtipoEtiqueta = "";
        this.Zreceta = "";
    }

    /**
     * Declaración de constructor
     *
     * @param ztype
     * @param zid
     * @param znumber
     * @param zmessage
     * @param zmaterial
     * @param zmaterialDesc
     * @param zean
     * @param zprecio
     * @param zimprimirEtiqueta
     * @param zdate
     * @param zdeSdprIpadr1
     * @param zpuertoCom1
     * @param zdeSdprIpadr2
     * @param zpuertoCom2
     * @param ZCodBarraCom
     * @param zactualizarPos
     * @param ziva
     * @param zclaveSkuKilo
     * @param zieps
     * @param zimptoEsp
     * @param zlifnr
     * @param zdeptoprivate
     * @param zsubdepto
     * @param zclass
     * @param zsubclass
     * @param zbrandId
     * @param ztipoProm
     * @param zplantillaPromo
     * @param zdifPrecio
     * @param zcaducidad
     * @param ztipoEtiqueta
     * @param zreceta
     */

    public Zreturndata(String ztype, String zid, String znumber, String zmessage, String zmaterial, String zmaterialDesc, String zean, double zprecio, String zimprimirEtiqueta, String zdate, String zdeSdprIpadr1, String zpuertoCom1, String zdeSdprIpadr2, String zpuertoCom2, String ZCodBarraCom, String zactualizarPos, String ziva, String zclaveSkuKilo, String zieps, String zimptoEsp, String zlifnr, String zdeptoprivate, String zsubdepto, String zclass, String zsubclass, String zbrandId, String ztipoProm, String zplantillaPromo, double zdifPrecio, double zcaducidad, String ztipoEtiqueta, String zreceta) {
        Ztype = ztype;
        Zid = zid;
        Znumber = znumber;
        Zmessage = zmessage;
        Zmaterial = zmaterial;
        ZmaterialDesc = zmaterialDesc;
        Zean = zean;
        Zprecio = zprecio;
        ZimprimirEtiqueta = zimprimirEtiqueta;
        Zdate = zdate;
        ZdeSdprIpadr1 = zdeSdprIpadr1;
        ZpuertoCom1 = zpuertoCom1;
        ZdeSdprIpadr2 = zdeSdprIpadr2;
        ZpuertoCom2 = zpuertoCom2;
        this.ZCodBarraCom = ZCodBarraCom;
        ZactualizarPos = zactualizarPos;
        Ziva = ziva;
        ZclaveSkuKilo = zclaveSkuKilo;
        Zieps = zieps;
        ZimptoEsp = zimptoEsp;
        Zlifnr = zlifnr;
        Zdeptoprivate = zdeptoprivate;
        Zsubdepto = zsubdepto;
        Zclass = zclass;
        Zsubclass = zsubclass;
        ZbrandId = zbrandId;
        ZtipoProm = ztipoProm;
        ZplantillaPromo = zplantillaPromo;
        ZdifPrecio = zdifPrecio;
        Zcaducidad = zcaducidad;
        ZtipoEtiqueta = ztipoEtiqueta;
        Zreceta = zreceta;
    }


    /**
     * Getters y Setters
     *
     * @return
     */

    public String getZtype() {
        return Ztype;
    }

    public void setZtype(String ztype) {
        Ztype = ztype;
    }

    public String getZid() {
        return Zid;
    }

    public void setZid(String zid) {
        Zid = zid;
    }

    public String getZnumber() {
        return Znumber;
    }

    public void setZnumber(String znumber) {
        Znumber = znumber;
    }

    public String getZmessage() {
        return Zmessage;
    }

    public void setZmessage(String zmessage) {
        Zmessage = zmessage;
    }

    public String getZmaterial() {
        return Zmaterial;
    }

    public void setZmaterial(String zmaterial) {
        Zmaterial = zmaterial;
    }

    public String getZmaterialDesc() {
        return ZmaterialDesc;
    }

    public void setZmaterialDesc(String zmaterialDesc) {
        ZmaterialDesc = zmaterialDesc;
    }

    public String getZean() {
        return Zean;
    }

    public void setZean(String zean) {
        Zean = zean;
    }

    public double getZprecio() {
        return Zprecio;
    }

    public void setZprecio(double zprecio) {
        Zprecio = zprecio;
    }

    public String getZimprimirEtiqueta() {
        return ZimprimirEtiqueta;
    }

    public void setZimprimirEtiqueta(String zimprimirEtiqueta) {
        ZimprimirEtiqueta = zimprimirEtiqueta;
    }

    public String getZdate() {
        return Zdate;
    }

    public void setZdate(String zdate) {
        Zdate = zdate;
    }

    public String getZdeSdprIpadr1() {
        return ZdeSdprIpadr1;
    }

    public void setZdeSdprIpadr1(String zdeSdprIpadr1) {
        ZdeSdprIpadr1 = zdeSdprIpadr1;
    }

    public String getZpuertoCom1() {
        return ZpuertoCom1;
    }

    public void setZpuertoCom1(String zpuertoCom1) {
        ZpuertoCom1 = zpuertoCom1;
    }

    public String getZdeSdprIpadr2() {
        return ZdeSdprIpadr2;
    }

    public void setZdeSdprIpadr2(String zdeSdprIpadr2) {
        ZdeSdprIpadr2 = zdeSdprIpadr2;
    }

    public String getZpuertoCom2() {
        return ZpuertoCom2;
    }

    public void setZpuertoCom2(String zpuertoCom2) {
        ZpuertoCom2 = zpuertoCom2;
    }

    public String getZCodBarraCom() {
        return ZCodBarraCom;
    }

    public void setZCodBarraCom(String ZCodBarraCom) {
        this.ZCodBarraCom = ZCodBarraCom;
    }

    public String getZactualizarPos() {
        return ZactualizarPos;
    }

    public void setZactualizarPos(String zactualizarPos) {
        ZactualizarPos = zactualizarPos;
    }

    public String getZiva() {
        return Ziva;
    }

    public void setZiva(String ziva) {
        Ziva = ziva;
    }

    public String getZclaveSkuKilo() {
        return ZclaveSkuKilo;
    }

    public void setZclaveSkuKilo(String zclaveSkuKilo) {
        ZclaveSkuKilo = zclaveSkuKilo;
    }

    public String getZieps() {
        return Zieps;
    }

    public void setZieps(String zieps) {
        Zieps = zieps;
    }

    public String getZimptoEsp() {
        return ZimptoEsp;
    }

    public void setZimptoEsp(String zimptoEsp) {
        ZimptoEsp = zimptoEsp;
    }

    public String getZlifnr() {
        return Zlifnr;
    }

    public void setZlifnr(String zlifnr) {
        Zlifnr = zlifnr;
    }

    public String getZdeptoprivate() {
        return Zdeptoprivate;
    }

    public void setZdeptoprivate(String zdeptoprivate) {
        Zdeptoprivate = zdeptoprivate;
    }

    public String getZsubdepto() {
        return Zsubdepto;
    }

    public void setZsubdepto(String zsubdepto) {
        Zsubdepto = zsubdepto;
    }

    public String getZclass() {
        return Zclass;
    }

    public void setZclass(String zclass) {
        Zclass = zclass;
    }

    public String getZsubclass() {
        return Zsubclass;
    }

    public void setZsubclass(String zsubclass) {
        Zsubclass = zsubclass;
    }

    public String getZbrandId() {
        return ZbrandId;
    }

    public void setZbrandId(String zbrandId) {
        ZbrandId = zbrandId;
    }

    public String getZtipoProm() {
        return ZtipoProm;
    }

    public void setZtipoProm(String ztipoProm) {
        ZtipoProm = ztipoProm;
    }

    public String getZplantillaPromo() {
        return ZplantillaPromo;
    }

    public void setZplantillaPromo(String zplantillaPromo) {
        ZplantillaPromo = zplantillaPromo;
    }

    public double getZdifPrecio() {
        return ZdifPrecio;
    }

    public void setZdifPrecio(double zdifPrecio) {
        ZdifPrecio = zdifPrecio;
    }

    public double getZcaducidad() {
        return Zcaducidad;
    }

    public void setZcaducidad(double zcaducidad) {
        Zcaducidad = zcaducidad;
    }

    public String getZtipoEtiqueta() {
        return ZtipoEtiqueta;
    }

    public void setZtipoEtiqueta(String ztipoEtiqueta) {
        ZtipoEtiqueta = ztipoEtiqueta;
    }

    public String getZreceta() {
        return Zreceta;
    }

    public void setZreceta(String zreceta) {
        Zreceta = zreceta;
    }

    @Override
    public Object getProperty(int index) {
        switch (index) {
            case 0:
                return this.Ztype;
            case 1:
                return this.Zid;
            case 2:
                return this.Znumber;
            case 3:
                return this.Zmessage;
            case 4:
                return this.Zmaterial;
            case 5:
                return this.ZmaterialDesc;
            case 6:
                return this.Zean;
            case 7:
                return this.Zprecio;
            case 8:
                return this.ZimprimirEtiqueta;
            case 9:
                return this.Zdate;
            case 10:
                return this.ZdeSdprIpadr1;
            case 11:
                return this.ZpuertoCom1;
            case 12:
                return this.ZdeSdprIpadr2;
            case 13:
                return this.ZpuertoCom2;
            case 14:
                return this.ZCodBarraCom;
            case 15:
                return this.ZactualizarPos;
            case 16:
                return this.Ziva;
            case 17:
                return this.ZclaveSkuKilo;
            case 18:
                return this.Zieps;
            case 19:
                return this.ZimptoEsp;
            case 20:
                return this.Zlifnr;
            case 21:
                return this.Zdeptoprivate;
            case 22:
                return this.Zsubdepto;
            case 23:
                return this.Zclass;
            case 24:
                return this.Zsubclass;
            case 25:
                return this.ZbrandId;
            case 26:
                return this.ZtipoProm;
            case 27:
                return this.ZplantillaPromo;
            case 28:
                return this.ZdifPrecio;
            case 29:
                return this.Zcaducidad;
            case 30:
                return this.ZtipoEtiqueta;
            case 31:
                return this.Zreceta;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 0;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index) {
            case 0:
                this.Ztype = value.toString();
            case 1:
                this.Zid = value.toString();
            case 2:
                this.Znumber = value.toString();
            case 3:
                this.Zmessage = value.toString();
            case 4:
                this.Zmaterial = value.toString();
            case 5:
                this.ZmaterialDesc = value.toString();
            case 6:
                this.Zean = value.toString();
            case 7:
                this.Zprecio = Double.parseDouble(value.toString());
            case 8:
                this.ZimprimirEtiqueta = value.toString();
            case 9:
                this.Zdate = value.toString();
            case 10:
                this.ZdeSdprIpadr1 = value.toString();
            case 11:
                this.ZpuertoCom1 = value.toString();
            case 12:
                this.ZdeSdprIpadr2 = value.toString();
            case 13:
                this.ZpuertoCom2 = value.toString();
            case 14:
                this.ZCodBarraCom = value.toString();
            case 15:
                this.ZactualizarPos = value.toString();
            case 16:
                this.Ziva = value.toString();
            case 17:
                this.ZclaveSkuKilo = value.toString();
            case 18:
                this.Zieps = value.toString();
            case 19:
                this.ZimptoEsp = value.toString();
            case 20:
                this.Zlifnr = value.toString();
            case 21:
                this.Zdeptoprivate = value.toString();
            case 22:
                this.Zsubdepto = value.toString();
            case 23:
                this.Zclass = value.toString();
            case 24:
                this.Zsubclass = value.toString();
            case 25:
                this.ZbrandId = value.toString();
            case 26:
                this.ZtipoProm = value.toString();
            case 27:
                this.ZplantillaPromo = value.toString();
            case 28:
                this.ZdifPrecio = Double.parseDouble(value.toString());
            case 29:
                this.Zcaducidad = Double.parseDouble(value.toString());
            case 30:
                this.ZtipoEtiqueta = value.toString();
            case 31:
                this.Zreceta = value.toString();
            default:
                break;
        }


    }

    @Override
    public void getPropertyInfo(int index, Hashtable hashtable, PropertyInfo info) {
        switch (index) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Ztype";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zid";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Znumber";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zmessage";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zmaterial";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZmaterialDesc";
                break;
            case 6:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zean";
                break;
            case 7:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zprecio";
                break;
            case 8:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZimprimirEtiqueta";
                break;
            case 9:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZdeSdprIpadr1";
                break;
            case 10:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZpuertoCom1";
                break;
            case 11:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZdeSdprIpadr2";
                break;
            case 12:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZpuertoCom2";
                break;
            case 13:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZCodBarraCom";
                break;
            case 14:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZactualizarPos";
                break;
            case 15:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Ziva";
                break;
            case 16:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZclaveSkuKilo";
                break;
            case 17:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zieps";
                break;
            case 18:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZimptoEsp";
                break;
            case 19:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zlifnr";
                break;
            case 20:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zdeptoprivate";
                break;
            case 21:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zsubdepto";
                break;
            case 22:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zclass";
                break;
            case 23:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zsubclass";
                break;
            case 24:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZbrandId";
                break;
            case 25:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZtipoProm";
                break;
            case 26:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZplantillaPromo";
                break;
            case 27:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZdifPrecio";
                break;
            case 28:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zcaducidad";
                break;
            case 29:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZtipoEtiqueta";
                break;
            case 30:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zreceta";
                break;
            default:
                break;
        }

    }

}




