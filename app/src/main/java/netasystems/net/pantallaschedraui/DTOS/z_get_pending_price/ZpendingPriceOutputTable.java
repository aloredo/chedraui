package netasystems.net.pantallaschedraui.DTOS.z_get_pending_price;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZpendingPriceOutputTable implements KvmSerializable {

    /**
     * Declaración de listas y variables
     */

    private String Matnr;
    private String ZshelfId;
    //Numeric
    private String Sorf1;
    private String Shelf;
    private String Maktx;
    private String Meinh;
    private String  Ean11;

    public ZpendingPriceOutputTable(){
        this.Matnr = "";
        this.ZshelfId = "";
        this.Sorf1 = "";
        this.Shelf = "";
        this.Maktx = "";
        this.Meinh = "";
        this.Ean11 = "";
    }

    /**
     * Declaración de constructor
     * @param matnr
     * @param zshelfId
     * @param sorf1
     * @param shelf
     * @param maktx
     * @param meinh
     * @param ean11
     */

    public ZpendingPriceOutputTable(String matnr, String zshelfId, String sorf1, String shelf, String maktx, String meinh, String ean11) {
        Matnr = matnr;
        ZshelfId = zshelfId;
        Sorf1 = sorf1;
        Shelf = shelf;
        Maktx = maktx;
        Meinh = meinh;
        Ean11 = ean11;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getMatnr() {
        return Matnr;
    }

    public void setMatnr(String matnr) {
        Matnr = matnr;
    }

    public String getZshelfId() {
        return ZshelfId;
    }

    public void setZshelfId(String zshelfId) {
        ZshelfId = zshelfId;
    }

    public String getSorf1() {
        return Sorf1;
    }

    public void setSorf1(String sorf1) {
        Sorf1 = sorf1;
    }

    public String getShelf() {
        return Shelf;
    }

    public void setShelf(String shelf) {
        Shelf = shelf;
    }

    public String getMaktx() {
        return Maktx;
    }

    public void setMaktx(String maktx) {
        Maktx = maktx;
    }

    public String getMeinh() {
        return Meinh;
    }

    public void setMeinh(String meinh) {
        Meinh = meinh;
    }

    public String getEan11() {
        return Ean11;
    }

    public void setEan11(String ean11) {
        Ean11 = ean11;
    }

    @Override
    public Object getProperty(int index) {
        switch (index) {
            case 0:
                return this.Matnr;
            case 1:
                return this.ZshelfId;
            case 2:
                return this.Sorf1;
            case 3:
                return this.Shelf;
            case 4:
                return this.Maktx;
            case 5:
                return this.Meinh;
            case 6:
                return this.Ean11;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index) {
            case 0:
                this.Matnr = value.toString();
            case 1:
                this.ZshelfId = value.toString();
            case 2:
                this.Sorf1 = value.toString();
            case 3:
                this.Shelf = value.toString();
            case 4:
                this.Maktx = value.toString();
            case 5:
                this.Meinh = value.toString();
            case 6:
                this.Ean11 = value.toString();
            default:
                break;
        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable hashtable, PropertyInfo info) {
        switch (index) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Matnr";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZshelfId";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Sorf1";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Shelf";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Maktx";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Meinh";
                break;
            default:
                break;
        }

    }
}
