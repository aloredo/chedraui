package netasystems.net.pantallaschedraui.DTOS.zmimfm_cs_consulta;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

class Ztmimcsubi {

    /**
     * Declaración de variables
     */

    private String Mandt;   //clnt3
    private String Bukrs;   //char4
    private String Werks;   //char4
    private String Lgort;   //char4
    private String ZshelfId;    //char10
    private String Sorf1;   //numeric10
    private String Shelf;   //numeric10
    private String Matnr;   //char18"
    private String Zsdpos;  //numeric10
    private double Facin;   //quantum13.3
    private double Zdepth;  //quantum13.0
    private double Front;   //quantum13.3
    private double Zsdlarg;    //decimal7.2
    private double Zsdprof;    //decimal7.2
    private double Zsdanch;    //decimal7.2
    private String Zsdbtpv; //numeric1
    private String Zsdcaex; //numeric8
    private String Zsdcavi; //numeric8
    private String Zsdfeca; //date10
    private String Zsdtipo; //char1
    private String Zsdneti; //numeric2
    private String Zsdsts;  //char1
    private String Zsdfesi; //date10
    private String Zsdhosi; //time
    private String Zsduser; //char12

    /**
     * Declaración de constructor
     * @param mandt
     * @param bukrs
     * @param werks
     * @param lgort
     * @param zshelfId
     * @param sorf1
     * @param shelf
     * @param matnr
     * @param zsdpos
     * @param facin
     * @param zdepth
     * @param front
     * @param zsdlarg
     * @param zsdprof
     * @param zsdanch
     * @param zsdbtpv
     * @param zsdcaex
     * @param zsdcavi
     * @param zsdfeca
     * @param zsdtipo
     * @param zsdneti
     * @param zsdsts
     * @param zsdfesi
     * @param zsdhosi
     * @param zsduser
     */

    public Ztmimcsubi(String mandt, String bukrs, String werks, String lgort, String zshelfId, String sorf1, String shelf, String matnr, String zsdpos, double facin, double zdepth, double front, double zsdlarg, double zsdprof, double zsdanch, String zsdbtpv, String zsdcaex, String zsdcavi, String zsdfeca, String zsdtipo, String zsdneti, String zsdsts, String zsdfesi, String zsdhosi, String zsduser) {
        Mandt = mandt;
        Bukrs = bukrs;
        Werks = werks;
        Lgort = lgort;
        ZshelfId = zshelfId;
        Sorf1 = sorf1;
        Shelf = shelf;
        Matnr = matnr;
        Zsdpos = zsdpos;
        Facin = facin;
        Zdepth = zdepth;
        Front = front;
        Zsdlarg = zsdlarg;
        Zsdprof = zsdprof;
        Zsdanch = zsdanch;
        Zsdbtpv = zsdbtpv;
        Zsdcaex = zsdcaex;
        Zsdcavi = zsdcavi;
        Zsdfeca = zsdfeca;
        Zsdtipo = zsdtipo;
        Zsdneti = zsdneti;
        Zsdsts = zsdsts;
        Zsdfesi = zsdfesi;
        Zsdhosi = zsdhosi;
        Zsduser = zsduser;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getMandt() {
        return Mandt;
    }

    public void setMandt(String mandt) {
        Mandt = mandt;
    }

    public String getBukrs() {
        return Bukrs;
    }

    public void setBukrs(String bukrs) {
        Bukrs = bukrs;
    }

    public String getWerks() {
        return Werks;
    }

    public void setWerks(String werks) {
        Werks = werks;
    }

    public String getLgort() {
        return Lgort;
    }

    public void setLgort(String lgort) {
        Lgort = lgort;
    }

    public String getZshelfId() {
        return ZshelfId;
    }

    public void setZshelfId(String zshelfId) {
        ZshelfId = zshelfId;
    }

    public String getSorf1() {
        return Sorf1;
    }

    public void setSorf1(String sorf1) {
        Sorf1 = sorf1;
    }

    public String getShelf() {
        return Shelf;
    }

    public void setShelf(String shelf) {
        Shelf = shelf;
    }

    public String getMatnr() {
        return Matnr;
    }

    public void setMatnr(String matnr) {
        Matnr = matnr;
    }

    public String getZsdpos() {
        return Zsdpos;
    }

    public void setZsdpos(String zsdpos) {
        Zsdpos = zsdpos;
    }

    public double getFacin() {
        return Facin;
    }

    public void setFacin(double facin) {
        Facin = facin;
    }

    public double getZdepth() {
        return Zdepth;
    }

    public void setZdepth(double zdepth) {
        Zdepth = zdepth;
    }

    public double getFront() {
        return Front;
    }

    public void setFront(double front) {
        Front = front;
    }

    public double getZsdlarg() {
        return Zsdlarg;
    }

    public void setZsdlarg(double zsdlarg) {
        Zsdlarg = zsdlarg;
    }

    public double getZsdprof() {
        return Zsdprof;
    }

    public void setZsdprof(double zsdprof) {
        Zsdprof = zsdprof;
    }

    public double getZsdanch() {
        return Zsdanch;
    }

    public void setZsdanch(double zsdanch) {
        Zsdanch = zsdanch;
    }

    public String getZsdbtpv() {
        return Zsdbtpv;
    }

    public void setZsdbtpv(String zsdbtpv) {
        Zsdbtpv = zsdbtpv;
    }

    public String getZsdcaex() {
        return Zsdcaex;
    }

    public void setZsdcaex(String zsdcaex) {
        Zsdcaex = zsdcaex;
    }

    public String getZsdcavi() {
        return Zsdcavi;
    }

    public void setZsdcavi(String zsdcavi) {
        Zsdcavi = zsdcavi;
    }

    public String getZsdfeca() {
        return Zsdfeca;
    }

    public void setZsdfeca(String zsdfeca) {
        Zsdfeca = zsdfeca;
    }

    public String getZsdtipo() {
        return Zsdtipo;
    }

    public void setZsdtipo(String zsdtipo) {
        Zsdtipo = zsdtipo;
    }

    public String getZsdneti() {
        return Zsdneti;
    }

    public void setZsdneti(String zsdneti) {
        Zsdneti = zsdneti;
    }

    public String getZsdsts() {
        return Zsdsts;
    }

    public void setZsdsts(String zsdsts) {
        Zsdsts = zsdsts;
    }

    public String getZsdfesi() {
        return Zsdfesi;
    }

    public void setZsdfesi(String zsdfesi) {
        Zsdfesi = zsdfesi;
    }

    public String getZsdhosi() {
        return Zsdhosi;
    }

    public void setZsdhosi(String zsdhosi) {
        Zsdhosi = zsdhosi;
    }

    public String getZsduser() {
        return Zsduser;
    }

    public void setZsduser(String zsduser) {
        Zsduser = zsduser;
    }
}
