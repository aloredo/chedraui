package netasystems.net.pantallaschedraui.DTOS.z_confirm_price;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class TableOfZpreciosPendtes implements KvmSerializable {

    /**
     * Declaración de lista
     */

    private List<ZpreciosPendtes> item;

    /**
     * Declaración de constructor
     * @param item
     */

    public TableOfZpreciosPendtes(List<ZpreciosPendtes> item) {
        this.item = item;
    }

    /**
     * Getters y Setters
     * @return
     */

    public List<ZpreciosPendtes> getItem() {
        return item;
    }

    public void setItem(List<ZpreciosPendtes> item) {
        this.item = item;
    }

    /**
     * Declaración de clase
     */

    private Zreturndata zreturndata = new Zreturndata();

    @Override
    public Object getProperty(int index) {
        switch (index){
            case 0:
                return zreturndata;
                default:
                    break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index){
            case 0:
                this.zreturndata = (Zreturndata) value;
                break;
            default:
                break;
        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        switch (index){
            case 0:
                info.type = Zreturndata.class;
                info.name = "item";
                break;
            default:
                break;
        }

    }
}
