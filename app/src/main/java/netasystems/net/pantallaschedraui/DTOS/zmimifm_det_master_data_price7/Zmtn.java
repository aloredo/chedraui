package netasystems.net.pantallaschedraui.DTOS.zmimifm_det_master_data_price7;

/**
 * @author Neta Systems / Fuentes Loredo Jose Alberto
 */

public class Zmtn {

    /**
     * Declaración de listas y variables
     */

    private String Zmueble;
    private String Ztramo;
    private String Znivel;

    /**
     * Declaración de constructor
     * @param zmueble
     * @param ztramo
     * @param znivel
     */

    public Zmtn(String zmueble, String ztramo, String znivel) {
        Zmueble = zmueble;
        Ztramo = ztramo;
        Znivel = znivel;
    }

    /**
     * Getters y Setters
     * @return
     */

    public String getZmueble() {
        return Zmueble;
    }

    public void setZmueble(String zmueble) {
        Zmueble = zmueble;
    }

    public String getZtramo() {
        return Ztramo;
    }

    public void setZtramo(String ztramo) {
        Ztramo = ztramo;
    }

    public String getZnivel() {
        return Znivel;
    }

    public void setZnivel(String znivel) {
        Znivel = znivel;
    }
}
