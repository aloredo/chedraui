package netasystems.net.pantallaschedraui.DTOS.z_get_pending_price;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZpreciosPendtes implements KvmSerializable {

    /**
     * Declaración de variables
     */

    private String Mandt;
    private String Werks;
    private String Matnr;
    private String Vrkme;
    private String Ersda;   //Date
    private String Zctrl;
    private double Kbetr;   //curr11.2
    private String Konwa;   //cuky5
    private String Datab;   //Date
    private String Datbi;   //Date
    private double Kbetr2;  //curr11.2
    private String Konwa2;  //cuky5
    private String Mwskz;
    private double Ziva;    //curr10.4
    private double Zieps;   //curr10.4;
    private String Akart;
    private String Zregla;
    private String Matkl;
    private String Ean11;
    private String Zproc;
    private String ZesqM;
    private String Zind;
    private double ZprecAn; //curr11.2
    private String Zestat;
    private String Erzet;
    private double ZprecCon;    //curr11.2;
    private String ZfecCon;    //Date
    private String ZhorConf;    //time
    private String Usnam;
    private double ZhybPvr;    //curr11.2
    private double ZhybPvp;    //curr11.2
    private String Zcodigosat;
    private String Zumsat;
    private String ZhybPvpcond;

    public ZpreciosPendtes (){
        this.Mandt = "";
        this.Werks = "";
        this.Matnr = "";
        this.Vrkme = "";
        this.Ersda = "";
        this.Zctrl = "";
        this.Kbetr = Double.parseDouble("");
        this.Konwa = "";
        this.Datab = "";
        this.Datbi = "";
        this.Kbetr2 = Double.parseDouble("");
        this.Konwa2 = "";
        this.Mwskz = "";
        this.Ziva = Double.parseDouble("");
        this.Zieps = Double.parseDouble("");
        this.Akart = "";
        this.Zregla = "";
        this.Matkl = "";
        this.Ean11 = "";
        this.Zproc = "";
        this.ZesqM = "";
        this.Zind = "";
        this.ZprecAn = Double.parseDouble("");
        this.Zestat = "";
        this.Erzet = "";
        this.ZprecCon = Double.parseDouble("");
        this.ZfecCon = "";
        this.ZhorConf = "";
        this.Usnam = "";
        this.ZhybPvr = Double.parseDouble("");
        this.ZhybPvp = Double.parseDouble("");
        this.Zcodigosat = "";
        this.Zumsat = "";
        this.ZhybPvpcond = "";
    }

    /**
     * Declaración de constructor
     * @param mandt
     * @param werks
     * @param matnr
     * @param vrkme
     * @param ersda
     * @param zctrl
     * @param kbetr
     * @param konwa
     * @param datab
     * @param datbi
     * @param kbetr2
     * @param konwa2
     * @param mwskz
     * @param ziva
     * @param zieps
     * @param akart
     * @param zregla
     * @param matkl
     * @param ean11
     * @param zproc
     * @param zesqM
     * @param zind
     * @param zprecAn
     * @param zestat
     * @param erzet
     * @param zprecCon
     * @param zfecCon
     * @param zhorConf
     * @param usnam
     * @param zhybPvr
     * @param zhybPvp
     * @param zcodigosat
     * @param zumsat
     * @param zhybPvpcond
     * @param zhybPvpfini
     * @param zhybPvpffin
     */

    public ZpreciosPendtes(String mandt, String werks, String matnr, String vrkme, String ersda, String zctrl, double kbetr, String konwa, String datab, String datbi, double kbetr2, String konwa2, String mwskz, double ziva, double zieps, String akart, String zregla, String matkl, String ean11, String zproc, String zesqM, String zind, double zprecAn, String zestat, String erzet, double zprecCon, String zfecCon, String zhorConf, String usnam, double zhybPvr, double zhybPvp, String zcodigosat, String zumsat, String zhybPvpcond, String zhybPvpfini, String zhybPvpffin) {
        Mandt = mandt;
        Werks = werks;
        Matnr = matnr;
        Vrkme = vrkme;
        Ersda = ersda;
        Zctrl = zctrl;
        Kbetr = kbetr;
        Konwa = konwa;
        Datab = datab;
        Datbi = datbi;
        Kbetr2 = kbetr2;
        Konwa2 = konwa2;
        Mwskz = mwskz;
        Ziva = ziva;
        Zieps = zieps;
        Akart = akart;
        Zregla = zregla;
        Matkl = matkl;
        Ean11 = ean11;
        Zproc = zproc;
        ZesqM = zesqM;
        Zind = zind;
        ZprecAn = zprecAn;
        Zestat = zestat;
        Erzet = erzet;
        ZprecCon = zprecCon;
        ZfecCon = zfecCon;
        ZhorConf = zhorConf;
        Usnam = usnam;
        ZhybPvr = zhybPvr;
        ZhybPvp = zhybPvp;
        Zcodigosat = zcodigosat;
        Zumsat = zumsat;
        ZhybPvpcond = zhybPvpcond;
        ZhybPvpfini = zhybPvpfini;
        ZhybPvpffin = zhybPvpffin;
    }

    private String ZhybPvpfini;    //Date

    /**
     * Getters y Setters
     * @return
     */

    public String getMandt() {
        return Mandt;
    }

    public void setMandt(String mandt) {
        Mandt = mandt;
    }

    public String getWerks() {
        return Werks;
    }

    public void setWerks(String werks) {
        Werks = werks;
    }

    public String getMatnr() {
        return Matnr;
    }

    public void setMatnr(String matnr) {
        Matnr = matnr;
    }

    public String getVrkme() {
        return Vrkme;
    }

    public void setVrkme(String vrkme) {
        Vrkme = vrkme;
    }

    public String getErsda() {
        return Ersda;
    }

    public void setErsda(String ersda) {
        Ersda = ersda;
    }

    public String getZctrl() {
        return Zctrl;
    }

    public void setZctrl(String zctrl) {
        Zctrl = zctrl;
    }

    public double getKbetr() {
        return Kbetr;
    }

    public void setKbetr(double kbetr) {
        Kbetr = kbetr;
    }

    public String getKonwa() {
        return Konwa;
    }

    public void setKonwa(String konwa) {
        Konwa = konwa;
    }

    public String getDatab() {
        return Datab;
    }

    public void setDatab(String datab) {
        Datab = datab;
    }

    public String getDatbi() {
        return Datbi;
    }

    public void setDatbi(String datbi) {
        Datbi = datbi;
    }

    public double getKbetr2() {
        return Kbetr2;
    }

    public void setKbetr2(double kbetr2) {
        Kbetr2 = kbetr2;
    }

    public String getKonwa2() {
        return Konwa2;
    }

    public void setKonwa2(String konwa2) {
        Konwa2 = konwa2;
    }

    public String getMwskz() {
        return Mwskz;
    }

    public void setMwskz(String mwskz) {
        Mwskz = mwskz;
    }

    public double getZiva() {
        return Ziva;
    }

    public void setZiva(double ziva) {
        Ziva = ziva;
    }

    public double getZieps() {
        return Zieps;
    }

    public void setZieps(double zieps) {
        Zieps = zieps;
    }

    public String getAkart() {
        return Akart;
    }

    public void setAkart(String akart) {
        Akart = akart;
    }

    public String getZregla() {
        return Zregla;
    }

    public void setZregla(String zregla) {
        Zregla = zregla;
    }

    public String getMatkl() {
        return Matkl;
    }

    public void setMatkl(String matkl) {
        Matkl = matkl;
    }

    public String getEan11() {
        return Ean11;
    }

    public void setEan11(String ean11) {
        Ean11 = ean11;
    }

    public String getZproc() {
        return Zproc;
    }

    public void setZproc(String zproc) {
        Zproc = zproc;
    }

    public String getZesqM() {
        return ZesqM;
    }

    public void setZesqM(String zesqM) {
        ZesqM = zesqM;
    }

    public String getZind() {
        return Zind;
    }

    public void setZind(String zind) {
        Zind = zind;
    }

    public double getZprecAn() {
        return ZprecAn;
    }

    public void setZprecAn(double zprecAn) {
        ZprecAn = zprecAn;
    }

    public String getZestat() {
        return Zestat;
    }

    public void setZestat(String zestat) {
        Zestat = zestat;
    }

    public String getErzet() {
        return Erzet;
    }

    public void setErzet(String erzet) {
        Erzet = erzet;
    }

    public double getZprecCon() {
        return ZprecCon;
    }

    public void setZprecCon(double zprecCon) {
        ZprecCon = zprecCon;
    }

    public String getZfecCon() {
        return ZfecCon;
    }

    public void setZfecCon(String zfecCon) {
        ZfecCon = zfecCon;
    }

    public String getZhorConf() {
        return ZhorConf;
    }

    public void setZhorConf(String zhorConf) {
        ZhorConf = zhorConf;
    }

    public String getUsnam() {
        return Usnam;
    }

    public void setUsnam(String usnam) {
        Usnam = usnam;
    }

    public double getZhybPvr() {
        return ZhybPvr;
    }

    public void setZhybPvr(double zhybPvr) {
        ZhybPvr = zhybPvr;
    }

    public double getZhybPvp() {
        return ZhybPvp;
    }

    public void setZhybPvp(double zhybPvp) {
        ZhybPvp = zhybPvp;
    }

    public String getZcodigosat() {
        return Zcodigosat;
    }

    public void setZcodigosat(String zcodigosat) {
        Zcodigosat = zcodigosat;
    }

    public String getZumsat() {
        return Zumsat;
    }

    public void setZumsat(String zumsat) {
        Zumsat = zumsat;
    }

    public String getZhybPvpcond() {
        return ZhybPvpcond;
    }

    public void setZhybPvpcond(String zhybPvpcond) {
        ZhybPvpcond = zhybPvpcond;
    }

    public String getZhybPvpfini() {
        return ZhybPvpfini;
    }

    public void setZhybPvpfini(String zhybPvpfini) {
        ZhybPvpfini = zhybPvpfini;
    }

    public String getZhybPvpffin() {
        return ZhybPvpffin;
    }

    public void setZhybPvpffin(String zhybPvpffin) {
        ZhybPvpffin = zhybPvpffin;
    }

    private String ZhybPvpffin;    //Date

    @Override
    public Object getProperty(int index) {
        switch (index) {
            case 0:
                return this.Mandt;
            case 1:
                return this.Werks;
            case 2:
                return this.Matnr;
            case 3:
                return this.Vrkme;
            case 4:
                return this.Ersda;
            case 5:
                return this.Zctrl;
            case 6:
                return this.Kbetr;
            case 7:
                return this.Konwa;
            case 8:
                return this.Datab;
            case 9:
                return this.Datbi;
            case 10:
                return this.Kbetr2;
            case 11:
                return this.Konwa2;
            case 12:
                return this.Mwskz;
            case 13:
                return this.Ziva;
            case 14:
                return this.Zieps;
            case 15:
                return this.Akart;
            case 16:
                return this.Zregla;
            case 17:
                return this.Matkl;
            case 18:
                return this.Ean11;
            case 19:
                return this.Zproc;
            case 20:
                return this.ZesqM;
            case 21:
                return this.Zind;
            case 22:
                return this.ZprecAn;
            case 23:
                return this.Zestat;
            case 24:
                return this.Erzet;
            case 25:
                return this.ZprecCon;
            case 26:
                return this.ZfecCon;
            case 27:
                return this.ZhorConf;
            case 28:
                return this.ZhybPvr;
            case 29:
                return this.ZhybPvp;
            case 30:
                return this.Zcodigosat;
            case 31:
                return this.Zumsat;
            case 32:
                return this.ZhybPvpcond;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index) {
            case 0:
                this.Mandt = value.toString();
            case 1:
                this.Werks = value.toString();
            case 2:
                this.Matnr = value.toString();
            case 3:
                this.Vrkme = value.toString();
            case 4:
                this.Ersda = value.toString();
            case 5:
                this.Zctrl = value.toString();
            case 6:
                this.Kbetr = Double.parseDouble(value.toString());
            case 7:
                this.Konwa = value.toString();
            case 8:
                this.Datab = value.toString();
            case 9:
                this.Datbi = value.toString();
            case 10:
                this.Kbetr2 = Double.parseDouble(value.toString());
            case 11:
                this.Konwa2 = value.toString();
            case 12:
                this.Mwskz = value.toString();
            case 13:
                this.Ziva = Double.parseDouble(value.toString());
            case 14:
                this.Zieps = Double.parseDouble(value.toString());
            case 15:
                this.Akart = value.toString();
            case 16:
                this.Zregla = value.toString();
            case 17:
                this.Matkl = value.toString();
            case 18:
                this.Ean11 = value.toString();
            case 19:
                this.Zproc = value.toString();
            case 20:
                this.ZesqM = value.toString();
            case 21:
                this.Zind = value.toString();
            case 22:
                this.ZprecAn = Double.parseDouble(value.toString());
            case 23:
                this.Zestat = value.toString();
            case 24:
                this.Erzet = value.toString();
            case 25:
                this.ZprecCon = Double.parseDouble(value.toString());
            case 26:
                this.ZfecCon = value.toString();
            case 27:
                this.ZhorConf = value.toString();
            case 28:
                this.Usnam = value.toString();
            case 29:
                this.ZhybPvr = Double.parseDouble(value.toString());
            case 30:
                this.ZhybPvp = Double.parseDouble(value.toString());
            case 31:
                this.Zcodigosat = value.toString();
            case 32:
                this.Zumsat = value.toString();
            case 33:
                this.ZhybPvpcond = value.toString();
            default:
                break;
        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable hashtable, PropertyInfo info) {
        switch (index) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Mandt";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Werks";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Matnr";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Vrkme";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Ersda";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zctrl";
                break;
            case 6:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Kbetr";
                break;
            case 7:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Konwa";
                break;
            case 8:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Datab";
                break;
            case 9:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Datbi";
                break;
            case 10:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Kbetr2";
                break;
            case 11:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Konwa2";
                break;
            case 12:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Mwskz";
                break;
            case 13:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Ziva";
                break;
            case 14:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zieps";
                break;
            case 15:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Akart";
                break;
            case 16:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Ean11";
                break;
            case 17:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zproc";
                break;
            case 18:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZesqM";
                break;
            case 19:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zind";
                break;
            case 20:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZprecAn";
                break;
            case 21:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zestat";
                break;
            case 22:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Erzet";
                break;
            case 23:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZprecCon";
                break;
            case 24:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZfecCon";
                break;
            case 25:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZhorConf";
                break;
            case 26:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Usnam";
                break;
            case 27:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZhybPvr";
                break;
            case 28:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZhybPvp";
                break;
            case 29:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zcodigosat";
                break;
            case 30:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zumsat";
                break;
            case 31:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "ZhybPvpcond";
                break;
            default:
                break;
        }

    }
}
