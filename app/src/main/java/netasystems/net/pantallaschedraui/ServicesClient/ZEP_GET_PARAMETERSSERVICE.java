package netasystems.net.pantallaschedraui.ServicesClient;

import android.os.AsyncTask;
import android.util.Log;

import org.kobjects.base64.Base64;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import netasystems.net.pantallaschedraui.DTOS.zep_get_parameters.TableOfZtmprParam;
import netasystems.net.pantallaschedraui.DTOS.zep_get_parameters.ZtmprParam;

public class ZEP_GET_PARAMETERSSERVICE {


    //Declaración de Web Services
    private final String nameSpaceSoap = "http://schemas.xmlsoap.org/soap/envelope/";

    private final String nameSpaceSap = "urn:sap-com:document:sap:soap:functions:mc-style";

    private final String methodSap = "ZepGetParameters";

    private final String soapActionSap = "urn:sap-com:document:sap:soap:functions:mc-style/ZepGetParameters";

    private final String urlSap = "http://chderp00.gcch.com:8010/sap/bc/srt/rfc/sap/zep_get_parameters/300/zep_get_parametersservice/zep_get_parametersservice";
    //private final String urlSap = "http://chqecc01.gcch.com:8010/sap/bc/srt/wsdl/bndg_532992F94B860660E1008000AC1CFA8E/wsdl11/allinone/ws_policy/document?sap-client=300"

    boolean error;


    //Metodo en Segundo Plano
    private class TareaWSConsulta extends AsyncTask<String, String, String> {

        //Se ejecuta al Inicio
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //CargarDatos("Inicio de carga");
        }

        @Override
        protected String doInBackground(String... strings) {
            //CargarDatos();
            consulta();
            return null;
        }

        //Despues de la ejecucion
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //CargarDatos(s);
        }


        //
        @Override
        protected void onProgressUpdate(String... values) {
            //super.onProgressUpdate(values);
            //CargarDatos(values[0]);
        }
    }


    public void consulta() {

        SoapObject soapObject = new SoapObject(nameSpaceSap,methodSap);

        PropertyInfo propertyInfoZobj1 = new PropertyInfo();
        PropertyInfo propertyInfoZprog = new PropertyInfo();


        propertyInfoZobj1.setName("Zobj1");
        propertyInfoZobj1.setValue("VERSION");
        propertyInfoZobj1.setType(String.class);
        soapObject.addProperty(propertyInfoZobj1);


        propertyInfoZprog.setName("Zprog");
        propertyInfoZprog.setValue("AUDITORIAPRECIOS");
        propertyInfoZprog.setType(String.class);
        soapObject.addProperty(propertyInfoZprog);


        TableOfZtmprParam tableOfZtmprParam = new TableOfZtmprParam();
        soapObject.addProperty("TyTParametros", tableOfZtmprParam);

        ZtmprParam tableOfZtmprParam2 = new ZtmprParam();
        soapObject.addProperty("item", tableOfZtmprParam2);


        SoapSerializationEnvelope envelopeSap = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelopeSap.env = nameSpaceSoap;
        envelopeSap.dotNet = false;
        envelopeSap.implicitTypes = true;

        envelopeSap.addMapping(nameSpaceSap,"TyTParametros", new TableOfZtmprParam().getClass());
        envelopeSap.addMapping(nameSpaceSap, "item", new ZtmprParam().getClass());

        envelopeSap.setOutputSoapObject(soapObject);

        HttpTransportSE httpTransportSE = new HttpTransportSE(urlSap,3000);
        httpTransportSE.debug = true;


        //List<HeaderProperty> c = new ArrayList<HeaderProperty>();
        //c.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode("USER:PASS".getBytes())));
        //c.add(new HeaderProperty("Connection", "Keep-Alive"));
        //httpTransportSE.debug = true;

        //c.add(new HeaderProperty("Authorization","Basic dmNhcm1vbmE6QmFkc2Vhc3Nvbi4yMDE4"));
        //c.add(new HeaderProperty("Username","vcarmona"));
        //c.add(new HeaderProperty("Password","pale1481"));




        //List<HeaderProperty> c = new ArrayList<HeaderProperty>();
        //c.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode("vcarmona:pale1481".getBytes())));

        // c.add(new HeaderProperty("Username",org.kobjects.base64.Base64.encode("vcarmona".getBytes())));
        //c.add(new HeaderProperty("Password",org.kobjects.base64.Base64.encode("pale1481".getBytes())));

        //c.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode("vcarmona pale1481".getBytes())));

        //String test = org.kobjects.base64.Base64.encode("vcarmona pale1481".getBytes());

        //List<HeaderProperty> c = new ArrayList<HeaderProperty>();

        //String test = org.kobjects.base64.Base64.encode("vcarmona:pale1481".getBytes());
        //System.out.println("------Me canso ganso< "+test+" >");

        //c.add(new HeaderProperty("Authorization","Basic " + test + ""));
        //c.add(new HeaderProperty("Authorization", "Basic dmNhcm1vbmE6cGFsZTE0ODE="));
        //String test = org.kobjects.base64.Base64.encode("vcarmona pale1481".getBytes());


        //c.add(new HeaderProperty("Authorization","Basic dmNhcm1vbmE6cGFsZTE0ODE="));


        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic :" + Base64.encode("vcarmona:pale1481".getBytes())));


            httpTransportSE.call(soapActionSap, envelopeSap, headerList);

            SoapObject response = (SoapObject) envelopeSap.getResponse();

            Log.d("dump Request: ",httpTransportSE.requestDump);
            Log.d("dump Respondes: ",httpTransportSE.responseDump);


            //SoapObject response = (SoapObject) envelopeSap.bodyIn;

        } catch (IOException ioe) {
            Log.d("IOException", ioe.toString());
            //Toast.makeText(getApplicationContext(),"No hay conexión hacia el servidor", Toast.LENGTH_LONG).show();
            error = true;
        } catch (XmlPullParserException xppe) {
            Log.d("XmlPullParserException",xppe.toString());
        } catch (Exception ex) {
            Log.d("Exception",ex.toString());
            //ex.printStackTrace();
        }
    }







}
